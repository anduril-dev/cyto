# Cyto: computational cytometry meets workflow engines

Cyto is a docker-based application to run Anduril analysis pipelines on high dimensional cytometry data.

See the wiki page for all details: [User Guide](https://bitbucket.org/anduril-dev/cyto/wiki/Home)

Developed at the Research Program for Systems Oncology, University of Helsinki (Finland).