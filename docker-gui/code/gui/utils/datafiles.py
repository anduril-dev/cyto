
from utils.config import *
from utils.runner import BackgroundProcess, is_running, set_umask
from werkzeug.utils import secure_filename
import csv
import datetime
import json
import multiprocessing
import os
import shutil
import subprocess
import sys
import time
import zipfile


TEMPLATE ="""package setup
//---------------------------------------------------------//
// File automatically written by the graphical             //
// user interface before launching odyssey-pipeline.scala  //
//---------------------------------------------------------//
package object settings {{
	val options = Map[String,String] (
		"in" -> "{in}",
    "clinical_in" -> "{clinical_in}",
		"all_channels" -> "{all_channels_str}",
		"channels_type" -> "{channels_type_str}",
		"channels_state" -> "{channels_state_str}",
		"group_column" -> "{group_column}",
		"app_dir" -> "{app_dir}",
		"meta_file" -> "{meta_file}",
		"meta_dir" -> "{meta_dir}",
		"tsne" -> "{tsne}",
		"perplexity" -> "{perplexity}",
		"theta" -> "{theta}",
	  "umap" -> "{umap}",
	  "pca" -> "{pca}",
		"umap_nearest" -> "{umap_nearest}",
		"umap_min_dist" -> "{umap_min_dist}",
		"cluster_method" -> "{cluster_method}",
		"k_clusters" -> "{k_clusters}",
		"k_nearest" -> "{k_nearest}",
		"transformation" -> "{transformation}",
		"normalization" -> "{normalization}",
		"scale" -> "{scale}",
		"downsample" -> "{downsample}",
		"sampling_method" -> "{sampling_method}",
		"sample_size" -> "{sample_size}",
		"downsample_dim" -> "{downsample_dim}",
		"sample_size_dim" -> "{sample_size_dim}",
    "settings_name" -> "{settings_name}",
    "max_threads" -> "{max_threads}"
	)
}}"""


def is_annotations(project, config):
  fname = os.path.join('/projects',project,'validated_annotations.csv')
  if not os.path.exists(fname):
    return "No"
  if config['annotation_validation_message'] == '':
    return "No"
  return config['annotation_validation_message']


def is_data_read(project, config):
  if config['data_validation_message'].startswith("error"):
    return config['data_validation_message']
  if len(list_data(project)) == 0:
    return "not done"
  fname = os.path.join('/projects',project,'channels.json')
  if os.path.exists(fname):
    size = os.stat(fname).st_size
    if size == 0:
      return "being read"
  if is_running('validate-input.py'):
    return "being read"
  if config['data_files_hash'] != data_files_hash(project):
    return "needs refresh"
  return config['data_validation_message']
  # ~ return "done"


def is_project(project):
  return os.path.exists(os.path.join('/projects',project))


def list_data(project):
  try:
    return os.listdir(os.path.join('/projects',project,'data'))
  except:
    return []


def data_files_hash(project):
  return ",".join(sorted(list_data(project)))


def get_formatted_date(tstamp):
  try:
    tstamp = int(tstamp)
  except TypeError as e:
    tstamp = None

  if tstamp == None:
    tstamp = time.time()

  date_time = datetime.datetime.fromtimestamp(tstamp)
  return date_time.strftime("%Y%d%m_%H%M%S")


def get_data_channels(project):
  channels = {'channels':[], 'ncells': 0, 'message': ""}
  try:
    channels_file = os.path.join('/projects', project, 'channels.json')
    with open(channels_file, 'rt') as fp:
      channels = json.load(fp)
    channels['channels'] = [ch.strip() for ch in sorted(channels['channels']) if len(ch) > 0]
  except Exception as e:
    print(e)

  return channels


def get_csv_file_name(project, active, tstamp):
  d = get_formatted_date(tstamp)
  return "Cyto-%s-%s-%s-results.csv"%( project, active, d )


def get_settings_file_name(project, tstamp):
  d = get_formatted_date(tstamp)
  return "Cyto-%s-%s-settings.json"%( project, d )


def get_zip_file_name(project, active, tstamp):
  d = get_formatted_date(tstamp)
  return "Cyto-%s-%s-%s-result-viewer.zip"%( project, active, d )




def count_data_read(project):
  return len(os.listdir(os.path.join('/projects', project,'input')))


def read_data_channels(project):
  data_folder = os.path.join('/projects', project,'data')
  input_folder = os.path.join('/projects', project,'input')
  channels_file = os.path.join('/projects', project, 'channels.json')

  # empty input folder
  [os.unlink(os.path.join(input_folder, the_file)) for the_file in os.listdir(input_folder)]

  def ch_command(data_folder, input_folder, channels_file):
    print("Reading channels...")
    rc = 0
    try:
      subprocess.check_call(
        [
          '/code/scripts/validate-input.py',
          data_folder,
          input_folder,
          channels_file
        ],
        preexec_fn = set_umask,
      )
    except subprocess.CalledProcessError as e:
      rc = 1

    config = read_config(project)
    if rc == 0:
      config['data_files_hash'] = data_files_hash(project)
      channel_data = get_data_channels(project)
      config['available_channels'] = channel_data['channels']
      config['ncells'] = channel_data['ncells']
      config['data_validation_message'] = channel_data['message']
      for setting in config['settings']:
        config['settings'][setting]['channels_type'] = [x for x in config['settings'][setting]['channels_type'] if x in config['available_channels']]
        config['settings'][setting]['channels_state'] = [x for x in config['settings'][setting]['channels_state'] if x in config['available_channels']]
    else:
      # process finished with error
      # empty input folder
      [os.unlink(os.path.join(input_folder, the_file)) for the_file in os.listdir(input_folder)]
      config['data_validation_message'] = 'error: Process stopped'
      try:
        os.remove(
          os.path.join('/projects', project, 'channels.json')
        )
      except FileNotFoundError as e:
        pass
    write_config(config, project)

    print("Reading channels finished")
    sys.stdout.flush()

  # ~ ch_command(data_folder, input_folder, channels_file)

  processor = BackgroundProcess(
    ch_command,
    (
      data_folder,
      input_folder,
      channels_file
    )
  )
  processor.start()
  time.sleep(1)


def read_annotations(project):
  data_folder = os.path.join('/projects', project,'input')
  input_file = os.path.join('/projects', project,'annotations.csv')
  output_file = os.path.join('/projects', project, 'validated_annotations.csv')
  message_file = os.path.join('/projects', project, 'channels.json')
  # output file gets emptied
  with open(output_file, 'wt') as fp:
    fp.close()

  def annotation_command(data_folder, input_file, output_file, message_file):
    print("Reading annotations...")
    subprocess.call(
        [
          '/code/scripts/validate-annot.py',
          input_file,
          data_folder,
          output_file,
          message_file
        ],
        preexec_fn=set_umask
    )
    print("Reading annotations finished")

  annotation_command(data_folder, input_file, output_file, message_file)
  time.sleep(0.5)
  try:
    with open(message_file, 'rt') as fp:
      messages = json.load(fp)
      return messages['annotation_message']
  except Exception as e:
    print(e)
  return ''


def is_anduril_ever_run(project, active):
  path = os.path.join(
      '/projects',
      project,
      'result-' + active
    )
  return os.path.exists(path)


def is_app_created(config, active):
  state = "no"
  app_path = os.path.join(
      '/projects',
      config['project'],
      'result-' + active,
      'reportWebsite'
    )
  if os.path.exists(
    app_path
  ):
    state = "created"
    if is_running(app_path):
      state = "running"
  return state


def list_apps_created(config):
  completed = []
  for setting in config['settings']:
    if is_app_created(config, active = setting) != "no":
      completed.append(setting)
  return completed


def create_scala_file(config, project, active):
  config['settings'][active]['channels_type_str'] = ','.join(sorted(config['settings'][active]['channels_type']))
  config['settings'][active]['channels_state_str'] = ','.join(sorted(config['settings'][active]['channels_state']))
  config['settings'][active]['all_channels_str'] = ','.join(
    sorted(list(set(config['settings'][active]['channels_state'] + config['settings'][active]['channels_type'])))
  )
  config['settings'][active]['settings_name'] = active
  config['settings'][active]['max_threads'] = max(1, multiprocessing.cpu_count() - 1)
  config['settings'][active]['clinical_in'] = config['clinical_in']

  with open(os.path.join('/projects',project,'settings.scala'),'wt') as myfile:
    myfile.write(TEMPLATE.format(**config['settings'][active]))

  shutil.copyfile(
    '/code/cyto/cyto-pipeline.scala',
    os.path.join('/projects', project, 'cyto-pipeline.scala')
  )


def tail_log(project, active, config):
  # TODO, get active config, and its exec folder
  p = subprocess.Popen([
    'tail',
    '-n','40',
    os.path.join('/projects', project,'result-' + active, '_log')
  ],
    stdin = subprocess.PIPE,
    stdout = subprocess.PIPE,
    stderr = subprocess.PIPE
  )
  output, err = p.communicate()
  return output.decode('utf-8')

def read_full_log(project, active, config):
    # TODO, get active config, and its exec folder
  return open(
    os.path.join('/projects', project, 'result-' + active, '_log'),
    'rt'
  ).read()

def get_project_list():
    projects = []
    for p in os.listdir("/projects/"):
      if os.path.exists(os.path.join('/projects',p,'config.json')):
        config = read_config(p)
        projects.append({
          'name': p,
          'data_files': len(list_data(p)),
          'modification_date': os.path.getmtime(os.path.join('/projects',p,'config.json')),
          'modification_date_str': file_date_formatted(os.path.join('/projects',p,'config.json')),
          'no_of_settings': len(config['settings']),
          'no_of_completed': len(list_apps_created(config)),
        })
    projects.sort(key = lambda p: p['modification_date'], reverse = True)

    return projects

def get_project_result(project, active):
  return os.path.join('/projects', project, 'result-' + active, 'clusteredData', 'out.csv')


def annotation_file_save(project, file):

    filename = os.path.join(
        '/projects/',
        project,
        'annotations.csv'
    )
    print("Saving " + filename)
    file.save(filename)
    os.chmod(filename, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)

    config = read_config(project)
    config['clinical_in'] = 'validated_annotations.csv'
    write_config(config, project)
    return read_annotations(project)

def data_file_save(project, file):

    nice_filename = secure_filename(
        file.filename
    )
    filename = os.path.join(
        '/projects/',
        project,
        'data',
        nice_filename
    )
    print("Saving " + filename)
    file.save(filename)
    os.chmod(filename, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)
    config = read_config(project)
    config['data_validation_message'] = "needs refresh"
    write_config(config, project)
    # annotations wont match after new data uploaded
    annotation_files_delete(project)


def data_files_delete(project):
    for filename in list_data(project):
      os.remove(
        os.path.join('/projects', project, 'data', filename)
      )
    input_folder = os.path.join('/projects', project,'input')
    [os.remove(os.path.join(input_folder, the_file)) for the_file in os.listdir(input_folder)]

    try:
      os.remove(
        os.path.join('/projects', project, 'channels.json')
      )
    except FileNotFoundError as e:
      pass
    # TODO: Delete execution folders for each setting
    config = read_config(project)
    config['clinical_in'] = ''
    config['data_validation_message'] = ''
    write_config(config, project)
    annotation_files_delete(project)

def annotation_files_delete(project):
    try:
      os.remove(
        os.path.join('/projects', project, 'annotations.csv')
      )
    except FileNotFoundError as e:
      pass
    try:
      os.remove(
        os.path.join('/projects', project, 'validated_annotations.csv')
      )
    except FileNotFoundError as e:
      pass
    # TODO: Delete execution folders for each setting
    config = read_config(project)
    config['annotation_validation_message'] = ''
    write_config(config, project)


def delete_execution_folder(project, active):
    folder = os.path.join('/projects', project, 'result-' + active)
    shutil.rmtree(folder, ignore_errors = True)

def project_delete_full(project):

    subprocess.call(
        [
          'rm',
          '-rf',
          os.path.join('/projects', project)
        ],
    )


def file_date_formatted(filename):
    return datetime.datetime.utcfromtimestamp(
        os.path.getmtime(filename)
    ).strftime("%y-%m-%d\xa0%H:%M")

def zip_app(project, active):
    data_path = os.path.join(
      '/projects',
      project,
      'result-' + active,
      'reportWebsite'
    )
    app_path = '/code/scripts/dash'

    zip_path = os.path.join(
      '/tmp',
      "download.zip"
    )
    zf = zipfile.ZipFile(zip_path, 'w', zipfile.ZIP_DEFLATED)
    zip_add_folder(zf, app_path, "/%s-%s-result-viewer/"%( project, active ))
    zip_add_folder(zf, data_path, "/%s-%s-result-viewer/"%( project, active ))

    zf.close()
    return zip_path


def zip_add_folder(zf, folder, zip_root):

    if os.path.isfile(folder):
      zf.write(
        folder,
        arcname = os.path.join(zip_root, os.path.basename(folder))
      )
      return

    folder += "/"
    for root, dirs, files in os.walk(folder):
      for file in files:
        fp = os.path.join(folder, root, file)
        zf.write(
          fp,
          arcname = os.path.join(zip_root, fp.replace(folder, "", 1))
        )


def dprint(s):
  sys.stderr.write(str(s) + "\n")
  sys.stderr.flush()
