from multiprocessing import Process
from utils.config import *
import os
import subprocess
import sys
import time

class Runner:
    def __init__(self):
        self.config = None
        self.p = None
        self.active = None

    def is_alive(self):
        return is_running('java')

    def start(self):
        self.stop()
        self.p = Process(target=self.call, args=())
        self.p.daemon = True
        self.p.start()
        # just to let process start..
        time.sleep(0.5)

    def call(self):
        self.config = read_config(self.config['project'])
        self.config['settings'][self.active]['anduril_message'] = ""
        write_config(self.config, self.config['project'])
        subprocess.call(
            [
                'rm',
                '-f',
                os.path.join(
                  'result-' + self.active,
                  '_log'
                )
            ],
            cwd = os.path.join('/projects',self.config['project'])
        )
        # Stop dash app and remove it. Anduril must reproduce it.
        subprocess.call([
          'pkill',
          '-f', 'cyto_index.py'
        ])
        subprocess.call(
          [
              'rm',
              '-rf',
              os.path.join(
                'result-' + self.active,
                'reportWebsite'
              )
          ],
          cwd = os.path.join('/projects', self.config['project'])
        )
        subprocess.check_call(
          [
              '/usr/bin/time',
              '/anduril/bin/anduril',
              'run',
              '-d', 'result-' + self.active,
              'cyto-pipeline.scala'
          ],
          cwd = os.path.join('/projects',self.config['project']),
          preexec_fn=set_umask
        )
        self.config = read_config(self.config['project'])
        if self.check_run():
          self.config['settings'][self.active]['config_stale'] = False
          print("Running finished")
        else:
          self.config['settings'][self.active]['anduril_message'] = "error: Anduril failed, view the log"
          print("Running finished with error")
        write_config(self.config, self.config['project'])

        if self.check_run():
          self.run_dash()

    def check_run(self):
        try:
          with open(os.path.join('/projects', self.config['project'], 'result-' + self.active, '_log'), 'rt') as fp:
            contents = fp.read()
        except FileNotFoundError as e:
          print(e)
          return False
        if contents.find('[INFO <run-workflow>] Nothing to execute') > 0:
          return True
        if contents.find('[INFO <run-workflow>] Done. No errors occurred') > 0:
          return True

        return False


    def run_dash(self):

        subprocess.call([
          'pkill',
          '-f', 'cyto_index.py'
        ])
        time.sleep(1)

        subprocess.Popen(
            [
                'python3',
                '/code/scripts/dash/cyto_index.py',
                '--location', os.path.join(
                  '/projects',
                  self.config['project'],
                  'result-' + self.active,
                  'reportWebsite'
                )
            ],
            cwd = os.path.join('/projects',self.config['project'])
        )


    def stop(self):
        subprocess.call(
            [
                'pkill',
                '-x',
                'java',
            ],
        )
        subprocess.call(
            [
                'pkill',
                '-x',
                'R',
            ],
        )
        subprocess.call(
            [
                'pkill',
                '-f',
                'validate-input.py',
            ],
        )

    def set_config(self, config, active):
        self.config = config
        self.active = active


class BackgroundProcess:
    def __init__(self, call, args=()):
        self.args = args
        self.call = call
        self.p = None

    def start(self):
        self.p = Process(target=self.call, args=self.args)
        self.p.daemon = True
        self.p.start()

def is_running(program):
    p = subprocess.Popen(
      [
        'pgrep',
        '-fa',
        program,
      ],
      stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    std_out, std_err = p.communicate()

    # ~ sys.stderr.write(program)
    # ~ sys.stderr.write(str(p.returncode))
    # ~ sys.stderr.write(str(std_out))
    # ~ sys.stderr.write(str(std_err))

    # ~ sys.stderr.flush()

    return p.returncode == 0

def set_umask():
    os.umask(000)


def exit_program():

    def exit_all():
      time.sleep(2)
      subprocess.call(
        [
            'pkill',
            '-f',
            'java',
        ],
      )
      subprocess.call(
        [
            'pkill',
            '-f',
            'python',
        ],
      )

    processor = BackgroundProcess(exit_all)
    processor.start()
