import os
import json
import stat

def empty_config(project):
  options = {
    'project': project,
    'available_channels': [],
    'last_active': 'default',
    'ncells': 0,
    'data_validation_message': "",
    'annotation_validation_message': "",
    'settings_upload_message': (None, 0),
    'data_files_hash': -1,
    'clinical_in': '',
    'settings': {
      'default': {
        'anduril_message': '',
        'config_stale': False,
        'in': 'input',
        'clinical_in': '',
        'channels_type': '',
        'channels_state': '',
        'group_column': None,
        'meta_file': '',
        'meta_dir': '',
        'app_dir': '',
        'tsne': False,
        'umap': True,
        'pca': False,
        'perplexity': 20,
        'theta': 0.3,
        'umap_nearest': 30,
        'umap_min_dist': 0.1,
        'cluster_method': 'FlowSOM',
        'k_clusters': 16,
        'k_nearest': 30,
        'transformation': 'log1p',
        'normalization': 'z-score',
        'scale': None, # Does not seem to ever be used
        'downsample': True,
        'downsample_dim': False,
        'sampling_method': 'random',
        'sample_size': 10000,
        'sample_size_dim': 10000,
      }
    }
  }
  return options


def write_config(config, project):
  os.makedirs(os.path.join('/projects', project), exist_ok = True)
  os.makedirs(os.path.join('/projects', project, 'data'), exist_ok = True)
  os.makedirs(os.path.join('/projects', project, 'input'), exist_ok = True)
  os.chmod(os.path.join('/projects', project), stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)
  os.chmod(os.path.join('/projects', project, 'data'), stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)
  os.chmod(os.path.join('/projects', project, 'input'), stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)
  with open(os.path.join('/projects', project, 'config.json'), 'wt') as fp:
    json.dump(
      config,
      fp,
      sort_keys = True,
      indent = 4
    )
    fp.flush()
    fp.close()
  os.chmod(os.path.join('/projects', project, 'config.json'), stat.S_IWUSR | stat.S_IRUSR | stat.S_IWGRP | stat.S_IRGRP | stat.S_IWOTH | stat.S_IROTH)


def read_config(project):
  with open(os.path.join('/projects',project,'config.json'), 'rt') as fp:
    config = json.load(fp)
  return config


def settings_file_save(project, file):
  try:
    # There's a maximum size. we're not reading just anything.
    new_settings = json.loads(file.read(8192*4))
    empty = empty_config(project)
    old_config = read_config(project)
    # first initialize with default values
    # update with settings from uploaded file
    for setting in new_settings:
      old_config['settings'][setting] = empty['settings']['default'].copy()
      old_config['settings'][setting].update(new_settings[setting])
    write_config(old_config, project)
    return "Settings imported"
  except Exception as e:
    print(e)
    return "Import failed (%s)"%( e, )

