import os
import subprocess

env = os.environ.copy()

# ~ subprocess.call(
    # ~ [
        # ~ 'python3',
        # ~ '/code/gui/app.py',
    # ~ ],
    # ~ env = env
# ~ )

subprocess.call(
    [
        'gunicorn',
        '-b','0.0.0.0:9999',
        '-w','4',
        'app:app'
    ],
    env = env
)
