
function changesDone(e) {
  setTimeout(function(){
    document.getElementById('save_settings_button').classList.add("glow");
    validateForm();
    /*document.getElementById('undo').style.display = "block";*/
  }, 100);
}

function setChanges() {

  var elements = document.querySelectorAll("select");
  for (var i = 0; i < elements.length; i++) {
    if ( elements[i].id == 'select_active' ) { continue; }
    if ( elements[i].id.startsWith('upload') ) { continue; }
    elements[i].onchange = changesDone;
  }
  var elements = document.querySelectorAll("input");
  for (var i = 0; i < elements.length; i++) {
    if ( elements[i].type == 'submit' ) { continue; }
    if ( elements[i].id.startsWith('upload') ) { continue; }
    if ( elements[i].id == 'mock_new_active' ) { continue; }
    elements[i].onchange = changesDone;
    elements[i].onkeyup = changesDone;
  }
}

function validateForm() {
  var downsample = document.getElementsByName('downsample');
  for (var i = 0; i < downsample.length; i++) {
    if (downsample[i].checked) {
      if (downsample[i].value == 'true') {
        enable_name('sample_size');
      } else {
        disable_name('sample_size');
      }
    }
  }

  var clustering = document.getElementsByName('cluster_method');
  show_id('clusters_for_None');
  hide_id('clusters_for_Phenograph');
  hide_id('clusters_for_k-means');
  for (var i = 0; i < clustering.length; i++) {
      if (clustering[i].value == 'Phenograph') {
        show_id('clusters_for_Phenograph');
        hide_id('clusters_for_None');
      }
      if (clustering[i].value == 'k-means' || clustering[i].value == 'FlowSOM') {
        show_id('clusters_for_k-means');
        hide_id('clusters_for_None');
      }
  }

  // Dimension reduction
  var downsample = document.getElementsByName('downsample_dim');
  for (var i = 0; i < downsample.length; i++) {
    if (downsample[i].checked) {
      if (downsample[i].value == 'true') {
        enable_name('sample_size_dim');
      } else {
        disable_name('sample_size_dim');
      }
    }
  }

  var checkbox = document.getElementsByName('tsne');
  for (var i = 0; i < checkbox.length; i++) {
      if (checkbox[i].checked) {
        enable_name('perplexity');
        enable_name('theta');
      } else {
        disable_name('perplexity');
        disable_name('theta');
      }
  }

  var checkbox = document.getElementsByName('umap');
  for (var i = 0; i < checkbox.length; i++) {
      if (checkbox[i].checked) {
        enable_name('umap_min_dist');
        enable_name('umap_nearest');
      } else {
        disable_name('umap_min_dist');
        disable_name('umap_nearest');
      }
  }

}


function copy_new_active() {
  document.getElementById('new_active').value = document.getElementById('mock_new_active').value;
}

function poll_anduril() {
  var project = document.getElementById('project_var').getAttribute('name');
  var active = document.getElementById('active_var').getAttribute('name');
  fetch(document.location.protocol + '//' + document.location.host + '/state_poll/' + project + '/' + active)
  .then(function(response) {
    return response.json();
  })
  .then(function(data) {
    if (data.anduril_ever_run) {
      anduril_has_run();
    } else {
      anduril_has_never_run();
    }

    if (document.getElementById('data_read') != null && document.getElementById('data_read').getAttribute('value') == "being read") {
      if (data.data_read == "done") {
        go_to_view(location);
      }
      if (data.data_read.startsWith("error")) {
        go_to_view(location);
      }
    }
    if (data.running || data.data_read == "being read") {
      anduril_running();
      disable_form("rundisable");
    } else {
      anduril_not_running();
    }
    if (document.getElementById('anduril_run_button') != null ) {
      if (!data.running && data.config_stale == "yes" && data.data_read == "done") {
        document.getElementById('anduril_run_button').classList.add("glow");
      } else {
        document.getElementById('anduril_run_button').classList.remove("glow");
      }
    }
    if (!data.running && data.data_read == "done") {
      enable_form("rundisable");
      enable_form("validatedisable");
    }
    if (data.data_read != "done") {
      //~ disable_form("rundisable");
      disable_form("validatedisable");
    }
    if (data.data_read.startsWith("error")) {
      //~ disable_form("rundisable");
      disable_form("validatedisable");
    }

    if (data.app_created == "running") {
      enable_results();
    } else {
      disable_results();
    }
    if (data.app_created == "no") {
      disable_downloads();
    } else {
      enable_downloads();
    }
    if (data.anduril_message.startsWith("error")) {
      set_anduril_message(data.anduril_message);
    } else {
      unset_anduril_message();
    }
    if (document.getElementById('validation_count') != null ) {
      document.getElementById('validation_count').innerHTML = data.data_validated;
    }

  })
  .catch(function(error) {
    console.log(error);
  } );
  if (document.getElementById('log_pre')!= null ) {
    poll_log();
  }
}

function anduril_running() {
  //~ document.getElementById('icon_play').classList.add('mock');
  //~ document.getElementById('icon_cpu').classList.remove('mock');
  if (document.getElementById('anduril_run_button') != null) {
    document.getElementById('anduril_run_button').classList.add('mock');
  }
  document.getElementById('anduril_stop_button').classList.add('running');
  document.getElementById('anduril_stop_button').classList.remove('mock');
  /*
  document.getElementById('anduril_indicator').innerHTML = '&#10005;';
  document.getElementById('anduril_indicator').classList.add('rotate');
  */
}
function anduril_not_running() {
  //~ document.getElementById('icon_play').classList.remove('mock');
  //~ document.getElementById('icon_cpu').classList.add('mock');
  if (document.getElementById('anduril_run_button') != null) {
    document.getElementById('anduril_run_button').classList.remove('mock');
  }
  document.getElementById('anduril_stop_button').classList.remove('running');
  document.getElementById('anduril_stop_button').classList.add('mock');
  /*
  document.getElementById('anduril_indicator').innerHTML = '&vrtri;';
  document.getElementById('anduril_indicator').classList.remove('rotate');
  */
}
function anduril_has_run() {
  enable_id('anduril_log_button');
  if ( document.getElementById('anduril_start_text') == null ) { return; }
  document.getElementById('anduril_start_text').innerHTML = 'Update&nbsp;analysis';
}
function anduril_has_never_run() {
  disable_id('anduril_log_button');
  if ( document.getElementById('anduril_start_text') == null ) { return; }
  document.getElementById('anduril_start_text').innerHTML = 'Start&nbsp;analysis';
}

function set_anduril_message(msg){
  var el = document.getElementById('anduril_message');
  if (el == null) { return }
  el.innerHTML = msg;
  el.classList.add('flash');
  var a_copy = el.cloneNode(true);
  el.parentNode.replaceChild(a_copy, el);
}

function unset_anduril_message(){
  if (document.getElementById('anduril_message') == null) { return };
  document.getElementById('anduril_message').innerHTML = "";
}



function poll_system() {
  fetch(document.location.protocol + '//' + document.location.host + '/')
  .then(function(response) {
    document.body.classList.remove('disabled');
    document.getElementById('shutdown_indicator').innerHTML = "";
  })
  .catch(function(error) {
    document.body.classList.add('disabled');
    document.getElementById('shutdown_indicator').innerHTML = "Program does not respond";
  });
}


function poll_log() {
  var project = document.getElementById('project_var').getAttribute('name');
  var active = document.getElementById('active_var').getAttribute('name');

  fetch(document.location.protocol + '//' + document.location.host + '/full_log/' + project + '/' + active + '/log.txt')
  .then(function(data) {
    return data.text();
  })
  .then(function(text) {
    var el = document.getElementById('log_pre');
    if (el != null) {
      // if no change, return
      if (el.innerText === text) { return }
      el.innerText = text;
      // if anduril is running, scroll to bottom
      if (! document.getElementById('anduril_stop_button').classList.contains('mock')) {
        el.scrollTop = el.scrollHeight;
      }
    }
  })
  .catch(function(error) {
    console.log(error);
  } );
}

function hide_id(which) {
  if (document.getElementById(which) == null) { return; }
  document.getElementById(which).classList.add('mock');
}

function show_id(which) {
  if (document.getElementById(which) == null) { return; }
  document.getElementById(which).classList.remove('mock');
}

function disable_id(which) {
  if (document.getElementById(which) == null) { return; }
  document.getElementById(which).classList.add('disabled');
}

function enable_id(which) {
  if (document.getElementById(which) == null) { return; }
  document.getElementById(which).classList.remove('disabled');
}

function disable_name(which) {
  var elements = document.getElementsByName(which);
  for (var i = 0; i < elements.length; i++) {
    elements[i].classList.add('disabled');
  }
}

function enable_name(which) {
  var elements = document.getElementsByName(which);
  for (var i = 0; i < elements.length; i++) {
    elements[i].classList.remove('disabled');
  }
}

function disable_form(which, allow_upload = false) {
  /*var elements = document.querySelectorAll("form");
  for (var i = 0; i < elements.length; i++) {
    if ( elements[i].id.startsWith('upload') ) { continue; }
    elements[i].classList.add('disabled');
  }*/
  var elements = document.getElementsByClassName(which);
  for (var i = 0; i < elements.length; i++) {
    if (allow_upload) {
      if ( elements[i].id.startsWith('upload') ) { continue; }
    }
    elements[i].classList.add('disabled');
  }
}

function enable_form(which) {
  /*var elements = document.querySelectorAll("form");
  for (var i = 0; i < elements.length; i++) {
    elements[i].classList.remove('disabled');
  }*/
  var elements = document.getElementsByClassName(which);
  for (var i = 0; i < elements.length; i++) {
    elements[i].classList.remove('disabled');
  }
}

function disable_results() {
  if (document.getElementById("results_link") == null) { return; }
  document.getElementById("results_link").classList.add('disabled');
}

function enable_results() {
  if (document.getElementById("results_link") == null) { return; }
  document.getElementById("results_link").classList.remove('disabled');
}

function disable_downloads() {
  if (document.getElementById("download_csv") == null) { return; }
  document.getElementById("download_csv").classList.add('disabled');
  document.getElementById("download_app").classList.add('disabled');
}

function enable_downloads() {
  if (document.getElementById("download_csv") == null) { return; }
  document.getElementById("download_csv").classList.remove('disabled');
  document.getElementById("download_app").classList.remove('disabled');
}


function launch_pollers() {
  setInterval(function() {
    poll_system();
  }, 5000);
  if (document.getElementById('project_var') == null) {
    return
  }
  poll_anduril();
  setInterval(function() {
    poll_anduril();
  }, 5000);
  if (document.getElementById('save_settings') != null) {
    validateForm();
    bindKeys();
  }
}

function UploadFile(file, file_no, files_total) {
    if (uploadTurn != file_no) {
        // Wait for our turn to upload. check every 0.5s
        setTimeout(
            function() {
                UploadFile(file,file_no,files_total);
            },
            500
        );
        return
    }
    var file_counter = "(" + (file_no + 1) + "/" + files_total + ") ";
    var xhr = new XMLHttpRequest();
    if (xhr.upload) {
        var o = document.getElementById("upload_progress");
        xhr.upload.addEventListener(
            "progress",
            function(e) {
                var pc = parseInt((e.loaded / e.total * 100));
                o.className = "";
                o.innerHTML = "Uploading: " + file_counter + pc + "%";
                if (pc == 100) {
                    o.innerHTML = "Finishing up " + file_counter + "wait ...";
                }
                // upload works, hide button
                document.getElementById("upload_button").classList.add('disabled');
            },
            false
        );

        // file received/failed
        xhr.onreadystatechange = function(e) {
            if (xhr.readyState == 4) {
                o.className = (xhr.status == 200 ? "success" : "failure");
                if (xhr.status == 200) {
                    if (file_no + 1 == files_total) {
                        location = document.getElementById('prepare_url').getAttribute('value');
                    }
                } else {
                    o.innerHTML = xhr.response;
                }
                uploadTurn += 1;
            }
        };

        // start upload
        xhr.open("POST", document.getElementById("upload_form").action, true);
        xhr.setRequestHeader("X-FILENAME", file.name);
        var formData = new FormData();
        formData.append('file', file);
        xhr.send(formData);

    }
}

// file selection
function FileSelectHandler(e) {

    // fetch FileList object
    var files = e.target.files || e.dataTransfer.files;
    uploadTurn = 0;
    // process all File objects
    for (var i = 0, f; f = files[i]; i++) {
        UploadFile(f,i,files.length);
    }
}
// Variable to stop parallel uploads
var uploadTurn = -1;

///// Annotation upload
function AnnotationFileSelectHandler(e) {

    // fetch FileList object
    var file = e.target.files || e.dataTransfer.files;
    if (file.length == 0) {
      return
    }
    var xhr = new XMLHttpRequest();
    if (xhr.upload) {
        // file received/failed
        xhr.onreadystatechange = function(e) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    location = location;
                } else {
                    alert( xhr.response );
                }
            }
        };

        // start upload
        xhr.open("POST", document.getElementById("upload_annotations_form").action, true);
        xhr.setRequestHeader("X-FILENAME", file[0].name);
        var formData = new FormData();
        formData.append('file_annotation', file[0]);
        xhr.send(formData);

    }
}


///// Settings upload
function SettingsFileSelectHandler(e) {

    // fetch FileList object
    var file = e.target.files || e.dataTransfer.files;
    if (file.length == 0) {
      return
    }
    var xhr = new XMLHttpRequest();
    if (xhr.upload) {
        // file received/failed
        xhr.onreadystatechange = function(e) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    setTimeout(function() {
                      location = location;
                    }, 1000);
                } else {
                    alert( xhr.response );
                }
            }
        };

        // start upload
        disable_form("rundisable");
        disable_form("validatedisable");
        xhr.open("POST", document.getElementById("upload_settings_form").action, true);
        xhr.setRequestHeader("X-FILENAME", file[0].name);
        var formData = new FormData();
        formData.append('file_settings', file[0]);
        xhr.send(formData);
    }
}

///// dash APP

function launch_app() {
 setTimeout(function(){
    var port = document.getElementById('port_var').getAttribute('value');
    document.location = document.location.protocol + '//' + document.location.host.split(":")[0] + ":" + port;
  }, 1000);
}


function go_to_view(url) {
  setTimeout(function(){
    document.location = url;
  }, 1000);
}

//// key binding

function bindKeys() {
  document.onkeydown = function(event) {
    if (event.ctrlKey || event.metaKey) {
        switch (String.fromCharCode(event.which).toLowerCase()) {
        case 's':
            event.preventDefault();
            document.getElementById('save_settings').click();
            break;
        case 'x':
            event.preventDefault();
            document.getElementById('anduril_run_button').click();
            break;
        }
    }
  }
}
