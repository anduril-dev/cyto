# -*- coding: utf-8 -*-

import os, sys, time, stat
import re
import json
from datetime import datetime
from flask import Flask, render_template, jsonify, current_app, Response, \
    redirect, url_for, request, g, session, send_file, send_from_directory, \
    abort, flash
from werkzeug.utils import secure_filename

from utils.config import *
from utils.datafiles import *
from utils.runner import *

app = Flask(__name__)
app.config.from_object(__name__)
app.secret_key = b'NVTVg#oVhkB2YtxzgLx4'
app.config['version'] = "2020 version 0.1"

ERRORS = {
  'FOLDER ERROR':'Input folder is empty, does not contain CSV or FCS files, or contains both. Make sure the input folder consists of one file type',
  'DIFFCHANNELS':'Input files contain different channel names',
  'INPUT CONVERSION':'Make sure your input is numeric.',
  'META':'Clinical data csv needs to have a specified column named either "file", "filename", "samplename" or "sample" that includes all the files used in the analysis.',
  'ANDURILPATH':'Anduril is not installed, cannot run the analysis.',
}
MIN_CHANNELS = 3


@app.before_request
def before_request():
    g.version = app.config['version']
    pass

@app.route("/")
def index():
    projects = get_project_list()

    return render_template("index.html", entries=projects)


@app.route('/delete_data/<project>', methods=['GET',])
def delete_data(project):
    config = data_files_delete(project)
    return redirect(url_for('view', project = project))


@app.route('/delete_project/<project>', methods=['GET',])
def delete_project(project):
    project_delete_full(project)
    return redirect(url_for('index'))


@app.route('/download_csv/<tstamp>/<project>-<active>-results.csv', methods=['GET'])
@app.route('/download_csv/<project>-<active>-results.csv', methods=['GET'])
def download_csv(project, active, tstamp = None):
    csv_path = get_project_result(project, active)
    if not os.path.exists(csv_path):
      return "File not found", 404
    return send_file(
        csv_path,
        as_attachment = True,
        attachment_filename = get_csv_file_name(project, active, tstamp)
    )


@app.route('/download_app/<tstamp>/<project>-<active>-result-viewer.zip', methods=['GET'])
@app.route('/download_app/<project>-<active>-result-viewer.zip', methods=['GET'])
def download_app(project, active, tstamp = None):
    zip_path = zip_app(project, active)
    return send_file(
        zip_path,
        as_attachment = True,
        attachment_filename = get_zip_file_name(project, active, tstamp)
    )


@app.route('/download_settings/<project>-<tstamp>-settings.json', methods=['GET'])
@app.route('/download_settings/<project>-settings.json', methods=['GET'])
def download_settings(project, tstamp = None):
    config = read_config(project)
    fname = get_settings_file_name(project, tstamp)
    return Response(
      json.dumps(config['settings'], indent = 2),
      mimetype = 'application/json',
      headers = {'Content-Disposition':'attachment;filename=%s'%( fname, )}
    )


@app.route('/new', methods=['POST'])
def new():
    project = request.form['project'].strip()
    project = re.sub('[^0-9a-zA-Z]+', '_', project)
    if is_project(project):
      flash("Project already exists")
      return redirect(url_for('index'))
    config = empty_config(project)
    write_config(config, project)
    return redirect(url_for('view', project = project, active = 'default'))


@app.route('/view/<project>', methods=['GET','POST'])
@app.route('/view/<project>/<active>', methods=['GET','POST'])
def view(project, active = None):
    if active == None:
      return redirect(url_for('view', project = project, active = 'default'))

    config = read_config(project)
    if is_data_read(project, config) == "needs refresh":
      read_data_channels(project)
      # reread hashes
      config = read_config(project)
      # ~ redirect(url_for('read_data', project = project))
    if not active in config['settings']:
      return "No such settings '%s'"%( active, ), 400

    toggles = {
      'running': app.config['runner'].is_alive(),
      'channels_read': is_data_read(project, config),
      'annotations': is_annotations(project, config),
      'data_validated': count_data_read(project)
    }
    if toggles['channels_read'].startswith('error'):
      flash("Data " + toggles['channels_read'])
    if config['annotation_validation_message'] != "done" and config['annotation_validation_message'] != "" and config['annotation_validation_message'] != None :
      flash("Annotation error: " + config['annotation_validation_message'])
    if config['settings_upload_message'][0] != None:
      # Its a trick. show the error for short time..
      if time.time() - config['settings_upload_message'][1] < 10:
        flash(config['settings_upload_message'][0])
      if time.time() - config['settings_upload_message'][1] > 10:
        config['settings_upload_message'] = (None, 0)
        write_config(config, project)

    data_files = len(list_data(project))
    new_active = None
    if request.method == 'POST':
        # Delete settings
        try:
          delete_this = request.form['delete_active']
          if delete_this == 'delete_active':
            if active == 'default':
              flash("Can not delete default settings")
            else:
              delete_execution_folder(project, active)
              del config['settings'][active]
              write_config(config, project)
            return redirect(url_for('view', project = project, active = 'default'))
        except Exception as e:
          pass

        # Create new settings
        try:
          if request.form['submit'] == "New active":
            new_active = request.form['new_active'].strip()
            new_active = re.sub('[^0-9a-zA-Z]+', '_', new_active)

            if new_active == '':
              raise Exception("Empty new active")
            config['settings'][new_active] = config['settings'][active].copy()
            write_config(config, project)
            active = new_active
          # ~ return redirect(url_for('view', project = project))
        except Exception as e:
          new_active = None
          pass

        # Change active settings
        try:
          active = request.form['active']
          write_config(config, project)
          return redirect(url_for('view', project = project, active = active))
        except Exception as e:
          pass


        # Modify settings
        old_config = read_config(project)
        # panel 1
        try:
          channels_type = request.form.getlist('channels_type')
          config['settings'][active]['channels_type'] = channels_type
        except Exception as e:
          flash("Could not read type markers")

        if len(channels_type) < MIN_CHANNELS:
          flash("Select at least {:} cell type channels".format(MIN_CHANNELS))

        try:
          channels_state = request.form.getlist('channels_state')
          config['settings'][active]['channels_state'] = channels_state
        except Exception as e:
          flash("Could not read state markers")

        # commented out in the view.html
        # ~ config['settings'][active]['group_column'] = request.form['group_column']

        # panel 2

        try:
          config['settings'][active]['downsample'] = request.form['downsample'] == "true"
        except Exception as e:
          config['settings'][active]['downsample'] = False

        try:
          config['settings'][active]['sample_size'] = max(10, min(
            config['ncells'],
            int(request.form['sample_size'].replace(",",""))
          ))
        except Exception as e:
          flash("Could not read Sample Size: not integer?")

        config['settings'][active]['sampling_method'] = request.form['sampling_method']
        config['settings'][active]['transformation'] = request.form['transformation']
        config['settings'][active]['normalization'] = request.form['normalization']

        # panel 3
        config['settings'][active]['cluster_method'] = request.form['cluster_method']
        try:
          config['settings'][active]['k_clusters'] = max(1, min(
            600,
            int(request.form['k_clusters'])
          ))
        except Exception as e:
          flash("Could not read Number of clusters: not integer?")
        try:
          config['settings'][active]['k_nearest'] = max(1, min(
            600,
            int(request.form['k_nearest'])
          ))
        except Exception as e:
          flash("Could not read Number of k-Nearest: not integer?")

        try:
          config['settings'][active]['downsample_dim'] = request.form['downsample_dim'] == "true"
        except Exception as e:
          config['settings'][active]['downsample_dim'] = False

        try:
          if config['settings'][active]['downsample']:
            config['settings'][active]['sample_size_dim'] = max(10, min(
              config['settings'][active]['sample_size'], # Max is the previous sampling size
              int(request.form['sample_size_dim'])
            ))
          else:
            config['settings'][active]['sample_size_dim'] = max(10, min(
              config['ncells'], # Max is the the total number of cells
              int(request.form['sample_size_dim'])
            ))
        except Exception as e:
          flash("Could not read Sample Size for Data Embedding: not integer?")


        try:
          config['settings'][active]['tsne'] = request.form['tsne'] == "true"
        except Exception as e:
          config['settings'][active]['tsne'] = False
        try:
          config['settings'][active]['perplexity'] = max(1, min(
            1000,
            int(request.form['perplexity'])
          ))
        except Exception as e:
          flash("Could not read Perplexity: not integer?")
        try:
          config['settings'][active]['theta'] = max(0, min(
            1,
            float(request.form['theta'])
          ))
        except Exception as e:
          flash("Could not read theta: not a number?")


        try:
          config['settings'][active]['umap'] = request.form['umap'] == "true"
        except Exception as e:
          config['settings'][active]['umap'] = False
        try:
          config['settings'][active]['umap_nearest'] = max(1, min(
            1000,
            int(request.form['umap_nearest'])
          ))
        except Exception as e:
          flash("Could not read UMAP Nearest Neighbors size: not integer?")
        try:
          config['settings'][active]['umap_min_dist'] = max(0, min(
            0.99,
            float(request.form['umap_min_dist'])
          ))
        except Exception as e:
          flash("Could not read UMAP Minimum Distance size: not a number?")


        try:
          config['settings'][active]['pca'] = request.form['pca'] == "true"
        except Exception as e:
          config['settings'][active]['pca'] = False


        ## non changable configs:
        config['settings'][active]['app_dir'] = os.path.join(
          '/projects',
          config['project'],
          'result-' + active,
          'dash_app',
          'out'
        )
        if not old_config['settings'][active] == config['settings'][active]:
          config['settings'][active]['config_stale'] = True
        write_config(config, project)
        flash("Data saved")

    if new_active != None:
      return redirect(url_for('view', project = project, active = new_active))

    return render_template(
      'view.html',
      active = active,
      config = config,
      toggles = toggles,
      data_files = data_files,
      available_channels = config['available_channels'],
      tstamp = int(time.time())
    )


@app.route('/read_data/<project>/<active>', methods=['GET','POST'])
def read_data(project, active):
    return render_template('preparing.html', project = project, active = active)
    # ~ read_data_channels(project)
    # ~ flash("Channels read!")
    # ~ return redirect(url_for('view', project = project))


@app.route('/launch_results/<project>', methods=['GET','POST'])
def launch_results(project):
    config = read_config(project)
    # if created, run and return to view.   if running, render results
    try:
      port = int(os.getenv('APP_PORT',''))
    except Exception as e:
      print(e)
      print("Using default port")
      sys.stdout.flush()
      port = 8050
    return render_template('results.html', config = config, port = port)


@app.route('/log/<project>/<active>', methods=['GET','POST'])
def log(project, active):
    config = read_config(project)
    try:
      log = tail_log(project, active, config)
    except:
      log = ""
    return render_template('log.html', config = config, log = log, active = active)


@app.route('/full_log/<project>/<active>/log.txt', methods=['GET','POST'])
def full_log(project, active):
    config = read_config(project)
    try:
      log = read_full_log(project, active, config)
    except:
      log = ""
    return Response(log, mimetype='text/plain')


@app.route('/run/<project>/<active>', methods=['GET','POST'])
def run(project, active):
    config = read_config(project)
    create_scala_file(config, project, active)
    # ~ try:
    app.config['runner'].set_config(read_config(project), active)
    app.config['runner'].stop()
    app.config['runner'].start()
    flash("Running process")
    # ~ except:
        # ~ return "Running failed\n", 400

    return redirect(url_for('view', project = project, active = active))


@app.route('/state_poll/<project>/<active>', methods=['GET'])
def state_poll(project, active):
    config = read_config(project)
    is_read = is_data_read(project, config)
    app_created = is_app_created(config, active)
    try:
      is_stale = "yes" if config['settings'][active]['config_stale'] else "no"
    except Exception as e:
      is_stale = "no"
    return jsonify({
      'running': app.config['runner'].is_alive(),
      'anduril_message': config['settings'][active]['anduril_message'],
      'anduril_ever_run': is_anduril_ever_run(project, active),
      'data_read': is_read,
      'app_created': app_created,
      'data_validated': count_data_read(project),
      'config_stale': is_stale
    })


@app.route('/stop/<project>/<active>', methods=['GET','POST'])
def stop(project, active):
    try:
        app.config['runner'].set_config(read_config(project), active)
        app.config['runner'].stop()
        flash("Stopping process")
    except:
        return "Stopping failed\n", 400

    return redirect(url_for('view', project = project, active = active))


@app.route('/upload/<project>/<active>', methods=['POST'])
def upload(project, active):
    if request.method == 'POST':
        file = request.files['file']
        if file:
            data_file_save(project, file)

    # ~ return '', 200
    return redirect(url_for('read_data', project = project, active = active))


@app.route('/upload_annotations/<project>/<active>', methods=['POST'])
def upload_annotations(project, active):
    if request.method == 'POST':
        file = request.files['file_annotation']
        if file:
            message = annotation_file_save(project, file)
            print(message)
            sys.stdout.flush()
            config = read_config(project)
            config['annotation_validation_message'] = message
            write_config(config, project)
            return redirect(url_for('read_data', project = project, active = active))
    return redirect(url_for('view', project = project, active = active))


@app.route('/upload_settings/<project>', methods=['POST'])
def upload_settings(project):
    if request.method == 'POST':
        file = request.files['file_settings']
        if file:
            message = settings_file_save(project, file)
            print(message)
            flash(message)
            sys.stdout.flush()
            config = read_config(project)
            config['settings_upload_message'] = (message, time.time())
            write_config(config, project)
    return redirect(url_for('view', project = project))


@app.route('/exit', methods=['GET','POST'])
def exit():
    flash("Shutting down...")
    exit_program()

    return redirect(url_for('index'))


app.config['runner'] = Runner()

if __name__ == "__main__":
  app.run(debug=True)


