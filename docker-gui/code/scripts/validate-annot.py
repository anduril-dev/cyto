#!/usr/bin/env python3

import argparse
import csv
import os
import sys
import string
import json

def get_options():
    parser = argparse.ArgumentParser(
        description = 'Validate clinical data'
    )
    parser.add_argument(
        'input_file',
        help = "Input data for validation",
    )
    parser.add_argument(
        'data_folder',
        help = "Folder for input data",
    )
    parser.add_argument(
        'output_file',
        help = "Validated data file",
    )
    parser.add_argument(
        'message_file',
        help = "File to write errors as json",
    )

    return parser.parse_args()

VALID_SUFFIXES = ['.csv', '.fcs']
VALID_CHARS = "_.%s%s" % (string.ascii_letters, string.digits)
VALID_HEADERS = ["sample", "file", "filename", "samplename"]
MAX_ANNOT_VARIABLES = 10
MAX_UNIQUE_VALUES = 10

def validate_annotation_file(input_file, data_folder, output_file):
    """ Check that "file*" column is found and that the filenames match """
    print('Converting clinical data csv to Anduril friendly format')

    dialect = ''
    with open(input_file, 'rt') as csvfile:
        dialect = csv.Sniffer().sniff(csvfile.readline())
        reader = csv.reader(open(input_file,'rt'), dialect=dialect)
        metadata = [row for row in reader]
    headers = metadata[0]
    data = metadata[1:]
    names = []
    fnames = []
    valid_meta = []
    sample_idx = 0
    # Check that one of the allowed headers is found in the clinical data headers
    for i,value in enumerate(headers):
      if value.lower() in VALID_HEADERS:
        valid_meta = metadata
        sample_idx = i
        # switch the join column name to "sample" for the pipeline
        valid_meta[0][sample_idx] = "sample"
        names = list(zip(*data))[sample_idx]
        break
    if len(valid_meta) == 0:
        return (
          1,
          'Clinical data csv needs to have a specified column that includes all the samples uploaded. Valid column names are: {:}'.format(', '.join(map(str, VALID_HEADERS)))
        )
    
    # Remove suffixes from the filenames to compare the valid_meta file names to the the actual ones
    fnames = os.listdir(data_folder)
    for i,fname in enumerate(fnames):
        for suf in VALID_SUFFIXES:
            if fname.endswith(suf):
                fname=fname.replace(suf, "")
                fnames[i]=fname

    names = list(names)
    for i,name in enumerate(names):
        for suf in VALID_SUFFIXES:
            if name.endswith(suf):
                name=name.replace(suf, "")
        names[i]= 'id'+name if name.isdigit() else name
    
    # Check that the clinical data and actual filenames match
    # check that all the DATA files have a line in metadata. not necessarily the other way around

    if set(fnames) <= set(names):
        print('Clinical data matches the filenames.')
        for row in valid_meta:
            if row[sample_idx] != "sample":
                new_name = row[sample_idx].rsplit('.', 1)[0]
                if new_name in fnames:
                    row[sample_idx] = new_name
        valid_meta = [row for row in valid_meta if ( ( row[sample_idx].rsplit('.', 1)[0] in fnames) or (row[sample_idx].rsplit('.', 1)[0] == "sample") ) ]
    else:
        print('Missing sample annotations for: {:}'.format(set(fnames) - set(names)))
        return (1,'Clinical data csv doesnt match the files used in the analysis.')

    # Check annotation variables against MAX_ANNOT_VARIABLES and MAX_UNIQUE_VALUES
    cols = list(zip(*valid_meta[1:]))[sample_idx+1:]
    if len(cols) > MAX_ANNOT_VARIABLES:
        return (1,'Clinical data contains too many annotation variables')
    if any( [len(set(var)) > MAX_UNIQUE_VALUES for var in cols]):
        return (1,'Too many categories in at least one annotation variable')

    # Replace any special chars with "_" so that join will not fail in anduril
    for i, row in enumerate(valid_meta[1:]):
        name = row[sample_idx]
        name = ''.join([e if e in VALID_CHARS else "_" for e in name])
        row[sample_idx] = name

    with open(output_file,'wt') as csvfile:
        writer=csv.writer(csvfile, dialect=csv.excel_tab)
        writer.writerows(valid_meta)

    return(0,'done')

def update_values(new_values, file_path):
  try:
    with open(file_path, 'rt') as fp:
      old_values = json.load(fp)
  except Exception as e:
    old_values = {}

  old_values.update(new_values)
  with open(file_path, 'wt') as fp:
    json.dump(
      old_values,
      fp,
      sort_keys = True,
      indent = 4
    )


if __name__ == "__main__":
  opts = get_options()
  ec, message = validate_annotation_file(
    opts.input_file,
    opts.data_folder,
    opts.output_file
  )
  print(message)
  update_values({"annotation_message": message}, opts.message_file)
  sys.exit(ec)
