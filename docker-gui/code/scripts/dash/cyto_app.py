import dash
import argparse
import feather
import pickle
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate
from app_helpers import *

################################################################################
################################################################################
# Initialize the app and load static variables to global scope
# load data from pipeline to the global scope for the app
# these variables remain static and do not change ever

parser = argparse.ArgumentParser()
parser.add_argument("--location", help="specify the location of the result folder")
parser.add_argument("--port", help="specify the local port number for the dash app", default=8050, type=int)
args = parser.parse_args()

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])
server = app.server
app.config['suppress_callback_exceptions']=True
app.scripts.config.serve_locally=True

report_dir = args.location
port = args.port

file1 = report_dir+"/folder1/sample_medians.feather"
file2 = report_dir+"/folder1/sample_means.feather"
file3 = report_dir+"/folder1/cell_counts.feather"
file4 = report_dir+"/folder1/cluster_medians.feather"
file5 = report_dir+"/folder1/cluster_means.feather"
file6 = report_dir+"/folder1/cluster_proportions_col.feather"
file7 = report_dir+"/folder1/cluster_proportions_row.feather"
file8 = report_dir+"/folder1/2d_embedding.feather"
file9 = report_dir+"/folder1/pipeline_options.feather"
file10 = report_dir+"/folder1/sample_mds.feather"
file11 = report_dir+"/folder1/sample_NRS.feather"
file12 = report_dir+"/folder1/simpson_indexes.feather"
file13 = report_dir+"/folder2/boxplots.pickle"
file14 = report_dir+"/folder2/minimum_spanning_trees.pickle"
file15 = report_dir+"/folder1/raw_data_full.feather"
file16 = report_dir+"/folder1/trans_data_full.feather"
file17 = report_dir+"/folder2/correlations.pickle"
file18 = report_dir+"/folder2/markers.pickle"
file19 = report_dir+"/folder1/cluster_sample_counts.feather"

sample_medians = feather.read_dataframe(file1)
sample_means = feather.read_dataframe(file2)
sample_counts = feather.read_dataframe(file3)
cluster_medians = feather.read_dataframe(file4)
cluster_means = feather.read_dataframe(file5)
cluster_freq_col = feather.read_dataframe(file6)
cluster_freq_row = feather.read_dataframe(file7)
twoD_embedding = feather.read_dataframe(file8)
pipeline_options = feather.read_dataframe(file9)
sample_mds = feather.read_dataframe(file10)
sampleNRS_scores = feather.read_dataframe(file11)
simpson_indexes = feather.read_dataframe(file12)
raw_df = feather.read_dataframe(file15)
trans_df = feather.read_dataframe(file16)
cluster_sample_counts = feather.read_dataframe(file19)
boxplots_dict = pickle.load(open(file13, "rb"), encoding='latin1')
mst_dict = pickle.load(open(file14, "rb"), encoding='latin1')
corr_dict = pickle.load(open(file17, "rb"), encoding='latin1')
markers_dict = pickle.load(open(file18, "rb"), encoding='latin1')

# get the right data out of the frames created by the anduril pipeline.
total_cells = int(sample_counts.Count.sum())
samples = list(sample_counts['sample'])
markers = markers_dict['all_channels']
type_markers = markers_dict['channels_type']
state_markers = markers_dict['channels_state']
labels = list(map(str, list(range(1, len(cluster_medians)+1))))
clusters = ['cluster {}'.format(i) for i in range(1,len(cluster_medians)+1)]
cluster_cell_counts = list(cluster_medians['Count'])
cluster_freq = list(cluster_medians['prop'])
sample_cell_counts = list(sample_counts['Count'])
sample_freq = list(sample_medians['prop'])

heat_sample_median_z = sample_medians[markers]
heat_sample_mean_z = sample_means[markers]
heat_clust_median_z = cluster_medians[markers]
heat_clust_mean_z = cluster_means[markers]

scores = sampleNRS_scores[sampleNRS_scores.columns[sampleNRS_scores.columns.isin(markers)]]
meta_data = sampleNRS_scores[sampleNRS_scores.columns[~sampleNRS_scores.columns.isin(markers)]]

mst_full = mst_dict.pop('full data', None)
nodes, edges = create_mst_grid(mst_full.drop(columns=['prop', 'cellcounts']))

# create color options for different callbacks
colors_meta = {col:create_colors(meta_data[col]) for col in meta_data}
colors_meta['label'] = create_colors(list(range(1,len(clusters)+1)))
colors_markers = create_colors(markers)
colors_samples = colors_meta['sample']

# Get the users embedding methods
embed_methods = pipeline_options['value'][0].split(",")

# Dropdown options
drop_opts1 = [dict(label='Cluster Label', value='label')]
for col in meta_data:
    drop_opts1.append(dict(label=col.capitalize(), value=col))

drop_opts2 = [dict(label=marker, value=marker) for marker in markers]
drop_opts3 = drop_opts1 + drop_opts2
drop_opts4 = [dict(label=col.capitalize(), value=col) for col in meta_data]
drop_opts5 = [dict(label=cluster, value=cluster) for cluster in clusters]
drop_opts6 = [dict(label=sample, value=sample) for sample in samples]
drop_opts8 = [dict(label=val.strip(), value=val.strip()) for val in embed_methods]
drop_opts9 = [dict(label='Cluster frequency', value='relative'), dict(label='Total cell count', value='total')]

if len(meta_data.columns) > 1:
    drop_opts10=[dict(label='None', value='None')]
    drop_opts10 += [dict(label=col.capitalize(), value=col) for col in meta_data if col != 'sample']
else:
    drop_opts10=[dict(label='None', value='None')]

drop_opts11 = [
    dict(label='None', value='None'),
    dict(label='Min-max', value='min-max'),
    dict(label='Z-score', value='z-score'),
]

drop_opts12 = [
    dict(label='Median', value='median'),
    dict(label='Mean', value='mean')
]

drop_opts13 = [
    dict(label='Marker heatmap', value='m-hmap'),
    dict(label='Sample heatmap', value='s-hmap')
]

drop_opts14 = [
    dict(label='Raw counts', value='raw-counts'),
    dict(label='Row frequencies', value='row-freq'),
    dict(label='Column frequencies', value='col-freq')
]

if state_markers != ['']:
    drop_opts15 = [
        dict(label='Type', value='Type'),
        dict(label='State', value='State'),
        dict(label='All', value='All')
    ]
else:
    drop_opts15 = [
        dict(label='Type', value='Type')
    ]

if len(meta_data.columns) > 1:
    drop_opts16=[dict(label='Average linkage', value='average')]
    drop_opts16 += [dict(label=col.capitalize(), value=col) for col in meta_data if col != 'sample']
else:
    drop_opts16=[dict(label='Average linkage', value='average')]
