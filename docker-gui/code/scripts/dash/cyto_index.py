from dash.dependencies import Input, Output, State
from cyto_app import *
from apps import sample_set_app, dim_reduction_app, clusters_app, pop_trees_app


pages_hrefs = ["/", "/sample-set-summary", "/dim-reduction", "/clustering", "/population-trees"]

# General layout for all the pages
app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    navbar(),
    html.Div(id='page-content')
])

tabtitle1 = html.Label('Dataset overview')
options1 = dash_table_wrap('table-options', df=pipeline_options)
dropdown1 = dash_dropdown('dropdown-metadata', 'sample', drop_opts4)
barplot1 = dash_graph('count-plot', height='60vh', loading=True, loading_num=20)
mdsplot1 = dash_graph('mds-plot', height='60vh', loading=True, loading_num=21)

subrow = (
    column('barplot-card', 6, barplot1),
    column('mds-card', 6, mdsplot1)
)

row_args = (
    card('options-card', 3, tabtitle1, options1),
    card('summary-card', 9, dropdown1, row(*subrow))
)

index_layout = html.Div([
    row(*row_args)
])

################################################################################
################################################################################
########################## MAIN RENDERER CALLBACKS #############################
################################################################################
################################################################################

@app.callback(Output('page-content', 'children'),
              [Input('url', 'pathname')])
def display_page(pathname):
    if pathname == '/':
        return index_layout
    elif pathname == '/sample-set-summary':
        return sample_set_app.layout
    elif pathname == '/dim-reduction':
        return dim_reduction_app.layout
    elif pathname == '/clustering':
        return clusters_app.layout
    elif pathname == '/population-trees':
        return pop_trees_app.layout
    else:
        return '404'


@app.callback(
    Output("navbar-collapse", "is_open"),
    [Input("navbar-toggler", "n_clicks")],
    [State("navbar-collapse", "is_open")])
def toggle_navbar_collapse(n, is_open):
    if n:
        return not is_open
    return is_open


@app.callback([Output("page-{}-link".format(i), "active") for i in range(1, 6)],
              [Input("url", "pathname")])
def toggle_active_links(pathname):
    if pathname == "/":
        # Treat page 1 as the homepage / index
        return True, False, False, False, False
    return [pathname == page for page in pages_hrefs]

################################################################################
################################################################################
########################## OVERVIEW PAGE CALLBACKS #############################
################################################################################
################################################################################
@app.callback(Output('mds-plot', 'figure'),
              [Input('dropdown-metadata', 'value')])
def update_mds_plot(metaValue):
    """callback for mds plot"""
    grouped_data = sample_mds.groupby([metaValue])
    return mdsplot(
        grouped_data=grouped_data,
        colors=colors_meta[metaValue],
        samples=samples,
        markersize=20,
        opacity=0.8,
        title='Sample MDS'
    )


@app.callback(Output('count-plot', 'figure'),
              [Input('dropdown-metadata', 'value')])
def update_count_plot(metaValue):
    """callback for countplot"""
    counts = sample_counts.sort_values(by=['Count'], ascending=False)
    total = counts.sum(numeric_only=True).values
    grouped_data = counts.groupby([metaValue], sort=False)
    return countplot_index(
        grouped_data,
        total_count=total,
        colors=colors_meta[metaValue],
        title="Cell Counts (Total Cells = {})".format(total_cells)
    )

if __name__ == '__main__':
    app.run_server(debug=True, threaded=True, host='0.0.0.0', port=port)
