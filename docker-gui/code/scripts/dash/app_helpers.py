import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import dash_daq as daq
import dash_table
import scipy
import os
import json
import numpy as np
import pandas as pd
import random
import networkx as nx
import plotly_express as px
import plotly.graph_objs as go
import plotly.figure_factory as ff
from plotly import subplots
from scipy.spatial.distance import squareform, pdist
from scipy.cluster.hierarchy import linkage, leaves_list
from networkx.drawing.nx_agraph import graphviz_layout
from textwrap import dedent as d

################################################################################
################################################################################
########################## APP HELPER FUNCTIONS ################################
################################################################################
################################################################################

rand_int = lambda: random.randint(0,255)
def create_colors(colorlist):
    """Create hex-codes for colors"""
    colors = []
    colorlist = pd.unique(colorlist)
    for i in range(len(colorlist)):
        colors.append('#{:02x}{:02x}{:02x}'.format(rand_int(), rand_int(), rand_int()))
    return dict(zip(colorlist, colors))


def create_colorscale(centers, value=None):
    "Create maximum and minimum values for colorscale of the MST and 2d-embedding plots and heatmaps"
    if value:
        cmin = np.min(centers[value])
        cmax = np.max(centers[value])
        return (cmin, cmax)
    else:
        zmin = np.min(np.min(centers))
        zmax = np.max(np.max(centers))
        return (zmin, zmax)


def hclust(df):
    """return a hierarchically clustered df, only rows get clustrered"""
    dist = pdist(df, metric='euclidean')
    link = linkage(dist, metric='euclidean', method='complete')
    return leaves_list(link)


def scale(type, df):
    """Scale columns of a dataframe"""
    if type == 'min-max':
        df_scaled = (df-df.min())/(df.max()-df.min())
    elif type == 'z-score':
        df_scaled = (df-df.mean())/df.std()
    else:
        df_scaled = df

    return df_scaled


################################################################################
################################################################################
################## FUNCTION WRAPPERS FOR LAYOUT MANAGEMENT #####################
################################################################################
################################################################################
def navbar():
    """Create the app navbar with dash bootstrap navbar"""
    pages = dbc.Row(
        [
            dbc.Nav(
                [
                    dbc.NavItem(dbc.NavLink("Overview", href="/", id='page-1-link', className='px-2'), className='px-4'),
                    dbc.NavItem(dbc.NavLink("Sample set summary", href="/sample-set-summary", id='page-2-link', className='px-2'), className='px-4'),
                    dbc.NavItem(dbc.NavLink("Dimensionality reduction", href="/dim-reduction", id='page-3-link', className='px-2'), className='px-4'),
                    dbc.NavItem(dbc.NavLink("Unsupervised clustering", href="/clustering", id='page-4-link', className='px-2'), className='px-4'),
                    dbc.NavItem(dbc.NavLink("Population trees", href="/population-trees", id='page-5-link',className='px-2'), className='px-4')
                ],
                pills=True,
                navbar=True
            )
        ],
        className="ml-auto flex-nowrap mt-3 mt-md-0 px-5",
        align="center",
        justify='around'
    )

    return dbc.Navbar([
            html.A(
                # Use row and col to control vertical alignment of logo / brand
                dbc.Row(
                    [
                        dbc.Col(html.Img(src="./assets/cyto-icon.png", height="80px", className='ml-3')),
                    ],
                    align="center",
                    no_gutters=True
                ),
                href="/",
            ),
            dbc.NavbarToggler(id="navbar-toggler"),
            dbc.Collapse(pages, id="navbar-collapse", navbar=True)
        ],
        color="orange",
        dark=True,
        className='mx-auto navbar-nav navbar-expand-lgpx-12'
    )


def nav_menu():
    """Create the app nav-tab with dash Tabs object"""
    return html.Div([
        dcc.Tabs(id="tabs", value='tab-1', children=[
            dcc.Tab(label='Overview', value='tab-1'),
            dcc.Tab(label='Sample set summary', value='tab-2'),
            dcc.Tab(label='Dimensionality reduction', value='tab-3'),
            dcc.Tab(label='Unsupervised clustering', value='tab-4'),
            dcc.Tab(label='Populations trees', value='tab-5'),
        ], colors={"border": "#d1e3ff", "primary": "#1a7ef0", "background": "#d9d9d9"}
        ),
        html.Div(id='tabs-content')
    ])


def app_layout():
    """Create the parent <div> of the app where all of the contents are rendered"""
    return html.Div([
        dcc.Location(id='url', refresh=False),
        navbar(),
        html.Div(id='page-content'),
        nav_menu()
    ])


def row(*args):
    """Bootstrap row dash wrapper"""
    return html.Div([
        html.Div(args, className='row')
    ], style={'padding-top': 5, 'padding-left': 15, 'padding-right': 15})


def card_column(*args) :
    """Bootstrap card column (3-column grid) wrapper"""
    return html.Div(args, className='card-columns pt-2')


def card(id, grid_num=12, comp1=None, comp2=None, comp3=None, comp4=None, comp5=None,
         comp6=None, comp7=None, comp8=None, comp9=None, comp10=None, comp11=None, comp12=None, comp13=None):
    """Bootstrap card dash wrapper"""
    return html.Div([
                html.Div([
                    html.Div([
                        comp1,
                        comp2,
                        comp3,
                        comp4,
                        comp5,
                        comp6,
                        comp7,
                        comp8,
                        comp9,
                        comp10,
                        comp11,
                        comp12,
                        comp13
                    ], className='p-1 card-body', id=id)
                ], className='card shadow p-1')
            ], className='pl-1 pr-1 col-sm-{} col-md-{} col-lg-{}'.format(grid_num, grid_num, grid_num))


def column(id, grid_num=12, comp1=None, comp2=None, comp3=None, comp4=None, comp5=None,comp6=None,
         comp7=None, comp8=None, comp9=None, comp10=None, comp11=None, comp12=None, comp13=None):
        """Simple bootstrap column function wrapper"""
        return html.Div([
                    html.Div([
                        comp1,
                        comp2,
                        comp3,
                        comp4,
                        comp5,
                        comp6,
                        comp7,
                        comp8,
                        comp9,
                        comp10,
                        comp11,
                        comp12,
                        comp13
                    ], className='p-1 text-center', id=id)
                ], className='p-1 col-sm-{} col-md-{} col-lg-{}'.format(grid_num, grid_num, grid_num))


def info_card(text1=None, text2=None, text3=None, text4=None,
              colwidth1=3, colwidth2=3, colwidth3=3, colwidth4=3,
              comp1=None, comp2=None, comp3=None, comp4=None):
    """A bootstrap card with title and text as content"""
    return dbc.Card([
        dbc.CardBody([
            dbc.Row([
                dbc.Col([
                    html.P(text1, className='mb-1'),
                    comp1
                ], width=colwidth1, className='px-1'),
                dbc.Col([
                    html.P(text2, className='mb-1'),
                    comp2
                ], width=colwidth2, className='px-1'),
                dbc.Col([
                    html.P(text3, className='mb-1'),
                    comp3
                ], width=colwidth3, className='px-1'),
                dbc.Col([
                    html.P(text4, className='mb-1'),
                    comp4
                ], width=colwidth4, className='px-1')
            ])
        ], className='pt-1 pb-1')
    ], className='text-center')
################################################################################
################################################################################
####################### FUNCTION WRAPPERS FOR WIDGETS ##########################
################################################################################
################################################################################

def accordion_item(*args, **kwargs):
    """Creates an item in the accordion"""
    return html.Div([
        dbc.Card(
        [
            html.Div([
                dbc.CardHeader(
                    html.H2(
                        dbc.Button(
                            "{}".format(kwargs['name']),
                            color="link",
                            id="group-{}-toggle".format(kwargs['i']),
                        )
                    )
                ),
                dbc.Collapse(
                    dbc.CardBody(
                        html.Div([
                            *args
                        ])
                    ),
                    id="collapse-{}".format(kwargs['i']),
                )
            ])
        ]
    )])


def create_accordion(*args, **kwargs):
    """Creates a collapsible bootstrap accordion"""
    return html.Div(
        html.Div([
            *args
        ],className="accordion")
    )


def dbc_dropdown(id, options=[{'label':'Dummy', 'value':'Dummy'}],
                multi=False, pd_top=2.5, pd_bottom=2.5):

    children = [dbc.DropdownMenuItem(d['value'], id="dropdown-{}-item".format(d['value'])) for d in options]
    dropdown = dbc.DropdownMenu(
        label="Central values",
        children=children
    )
    return dropdown


def dbc_buttongroup(options, size="md", vertical=False):
    """dash bootstrap components function wrapper for buttongroup"""
    # First button is set as active
    buttons=[]
    for i, d in enumerate(options):
        if i == 0:
            buttons.append(dbc.Button(d, id="button-{}".format(d), className="btn active", n_clicks=1))
        else:
            buttons.append(dbc.Button(d, id="button-{}".format(d), className="btn"))

    return html.Div([
        dbc.ButtonGroup(buttons, size=size, vertical=vertical)
    ])


def dash_pre(id, height=40):
    """html <pre> dash object wrapper"""
    return html.Div([
        html.Pre(
            id=id,
            style={
                'border':'thin lightgrey solid',
                'height':height,
                'padding-top': 15,
                'padding-left': 30,
                'padding-right': 30
            }
        )
    ], style={'padding-top':15})


def daq_toggle(id, value, label, margin_left=0, margin_right=0, disabled=False):
    """DAQ toggle switch wrappers"""
    return html.Div([
        daq.ToggleSwitch(
            id=id,
            size=30,
            value=value,
            theme=dict(primary=True),
            label=label,
            disabled=disabled,
            style={
                'padding':5,
                'margin-left':margin_left,
                'margin-right':margin_right
            }
        )
    ])

def dash_dropdown(id, value, options=[{'label':'Dummy', 'value':'Dummy'}],
                  multi=False, pd_top=2.5, pd_bottom=2.5, pd_right=30, pd_left=30):
    """Dash dropdown object wrapper"""
    return html.Div([
        dcc.Dropdown(
            id=id,
            options=options,
            value=value,
            clearable=False,
            multi=multi
        )],
        style= {
            'padding-top': pd_top,
            'padding-bottom':pd_bottom,
            'padding-left': pd_left,
            'padding-right': pd_right
        }
    )


def dash_radiobuttons(id, value, options=[{'label': 'Dummy', 'value': 'Dummy'}]):
    """Dash radiobuttons object wrapper"""
    return html.Div([
            dcc.RadioItems(
                id=id,
                options=options,
                value=value,
                labelStyle={'display': 'inline-block', 'padding-left':4, 'padding-right':4}
            )
        ], style={
            'padding-top':2.5,
            'padding-bottom':0,
            'padding-left':30,
            'padding-right':30
        }
    )


def dash_table_wrap(id, row_select=False, df=None):
    """Dash data table object wrapper"""
    if df is not None:
        return dash_table.DataTable(
            id=id,
            data=df.to_dict("rows"),
            columns=[{"name": i, "id": i} for i in df.columns],

            style_header={
                'backgroundColor': 'rgb(30, 30, 30)',
                'fontWeight': 'bold',
                'color': 'white',
                'textAlign': 'left'
            },
            style_cell={
                'color': 'rgb(70, 70, 70)',
                'textAlign': 'left',
                'padding':'8px'
            },

            fixed_rows={'headers': True, 'data': 0},

            style_table={
                'overflowY': 'scroll'
            }
        )
    else:
        return dash_table.DataTable(
            id=id,
            data=pd.DataFrame().to_dict('rows'),

            style_header={
                'fontWeight': 'bold',
                'textAlign': 'left',
                'padding':'8px'
            },
            style_cell={
                'color': 'rgb(70, 70, 70)',
                'textAlign': 'left',
                'padding':'8px',
                'minWidth':'115px'
            },

            fixed_rows={'headers': True, 'data': 0},

            style_table={
                'overflowY': 'scroll',
                'overflowX': 'scroll'
            }
        )

################################################################################
################################################################################
######## FUNCTION WRAPPERS FOR DASH TABLE OBJS AND PLOTLY GRAPH OBJS ###########
################################################################################
################################################################################

def dash_table_paginate(id, height, width, row_select=False, data=[], columns=[]):
    """Dash data table object wrapper"""
    return dash_table.DataTable(
        id=id,
        data=data,
        columns=columns,

        style_header={
            'backgroundColor': 'rgb(30, 30, 30)',
            'fontWeight': 'bold',
            'color': 'white',
            'textAlign': 'left'
        },
        style_cell={
            'color': 'rgb(70, 70, 70)',
            'textAlign': 'left',
            'minWidth': '140px', 'maxWidth': '280px',
            'padding':'8px'
        },

        fixed_rows={'headers': True, 'data': 0},

        style_table={
            'overflowY':'scroll',
            'overflowX':'scroll'
        },

        page_current=0,
        page_size=8,
        page_action='native'
    )


def dash_graph(id, figure=None, height=None, scrollZoom=False, loading=False, loading_num=0):
    """Dash graph object wrapper"""
    if figure != None:
        return dcc.Graph(
            id=id,
            figure=figure,
            style={"height": height},
            config={'displaylogo':False, 'scrollZoom':scrollZoom, 'autosizable':True}
        )
    elif loading and loading_num != 0:
        return dcc.Loading(
            id="loading-{}".format(loading_num),
            children=[
                dcc.Graph(
                    id=id,
                    figure=go.Figure(
                        layout=go.Layout(
                            plot_bgcolor='rgb(255,255,255)',
                            yaxis=dict(
                                mirror= False,
                                showgrid=False,
                                showline=False,
                                zeroline=False,
                                showticklabels=False,
                                ticks=""
                            ),
                            xaxis=dict(
                                mirror= False,
                                showgrid=False,
                                showline=False,
                                zeroline=False,
                                showticklabels=False,
                                ticks=""
                            )
                        )
                    ),
                    config={
                        'displaylogo':False,
                        'scrollZoom':scrollZoom,
                        'autosizable':True,
                    },
                    style={"height": height},
                )
            ],
            type="circle",
            style={
                'height':height,
                'display':'flex',
                'align-items':'center'
            }
        )
    else:
        return dcc.Graph(
            id=id,
            figure=go.Figure(
                layout=go.Layout(
                    plot_bgcolor='rgb(255,255,255)',
                    yaxis=dict(
                        mirror= False,
                        showgrid=False,
                        showline=False,
                        zeroline=False,
                        showticklabels=False,
                        ticks=""
                    ),
                    xaxis=dict(
                        mirror= False,
                        showgrid=False,
                        showline=False,
                        zeroline=False,
                        showticklabels=False,
                        ticks=""
                    )
                )
            ),
            config={
                'displaylogo':False,
                'scrollZoom':scrollZoom,
                'autosizable':True,
                'displayModeBar': False
            },
            style={"height": height},
        )


def dash_markdown(text):
    """Dash markdown object wrapper"""
    return html.Div([
        dcc.Markdown(d("""{}""".format(text)))
    ], style={'padding-top': 10, 'padding-left': 15, 'padding-right': 15})


def dash_store(id, storage_type='memory'):
    return dcc.Store(id, storage_type)

################################################################################
################################################################################
################ FUNCTION WRAPPERS FOR PLOTLY PLOTS ############################
################################################################################
################################################################################

def heatmap_annotations(heatmap, fontsize, yref='y1', xref='x1'):
    """Create annotations for the plotly heatmap figure object"""
    annotations = []
    for i in range(len(heatmap['y'])):
        for j in range(len(heatmap['x'])):
            annotations.append(
                dict(
                    text=str(heatmap['z'][i][j]),
                    x=heatmap['x'][j],
                    y=heatmap['y'][i],
                    xref=xref,
                    yref=yref,
                    font=dict(color='#FFFFFF', size=fontsize),
                    showarrow=False
                )
            )
    return annotations


def create_hmap_hover_txt(xaxis, yaxis, cell_counts, proportions, y_flag=False, x_flag=False):
    """Create hovering texts for the plotly heatmap figure object"""
    hovertext_heatmap = []
    for i, rowname in enumerate(yaxis):
        hovertext_heatmap.append(list())
        for j, colname in enumerate(xaxis):
            if x_flag:
                t = d("""antibody: {}<br>sample: {}\
                     <br>sample cell count: {}\
                     <br>sample frequency: {}%""").format(rowname, colname, cell_counts[j], proportions[j])
                hovertext_heatmap[-1].append(t)
            elif y_flag:
                t = d("""\
                     marker: {}<br>cluster: {}\
                     <br>cluster cell count: {}\
                     <br>cluster frequency: {}%""").format(colname, rowname, cell_counts[i], proportions[i])
                hovertext_heatmap[-1].append(t)
            else:
                t = d("""\
                      cluster: {}<br>sample: {}\
                      <br>sample total cell count: {}\
                      <br>sample frequency: {}%""").format(rowname, colname, cell_counts[j], proportions[j])
                hovertext_heatmap[-1].append(t)
    return hovertext_heatmap


def cluster_sample_map(cluster_summaries, sample_cluster_counts, markers, type_markers,
                       meta_data, colors_meta, state_markers, clusters, cluster_cell_counts,
                       sample_counts, ordering_y, ordering_samples, normalize, cluster_freqs,
                       sample_freqs, fsize_x=13, fsize_y=13, fsize_a=8):
    """
    Returns a plotly figure that contains heatmap of clusters vs markers with marker type annotation and
    a sample frequency vs clusters heatmap. heatmaps are aligned by the shared cluster-axis and they share
    a dendrogram.
    """

    sample_cell_counts = list(sample_counts['Count'])
    samples = list(sample_counts['sample'])

    #  Create domains for the xaxis
    xdomain = .85 # dendrogram takes 5% of the xaxis and barplot another 10%
    x_type_markers = len(type_markers)/(len(type_markers)+len(state_markers)+len(samples))*xdomain
    x_state_markers = len(state_markers)/(len(type_markers)+len(state_markers)+len(samples))*xdomain
    x_samples = len(samples)/(len(type_markers)+len(state_markers)+len(samples))*xdomain
    domains = [x_samples, x_state_markers, x_type_markers]

    # If domain is too little add some lentgh to it
    if state_markers != ['']:
        if any(x < 0.1  for x in domains):
            domains=[x+0.05 if x==x_state_markers else x-0.05 for x in domains]
            x_samples = domains[0]
            x_state_markers = domains[1]
            x_type_markers = domains[2]

    # Initialize the figure with dendrogram
    # Also hierarchically cluster the y-axis (clusters)
    annotations = []
    if ordering_y == 'clustersimilarity':
        X = cluster_summaries[type_markers]
    elif ordering_y == 'clusterbias':
        X = sample_cluster_counts.transpose()

    fig = ff.create_dendrogram(X, orientation='right', labels=clusters)

    # Get the new order of clusters and their indexes to reorder the dataframes
    clusters_ordered = fig['layout']['yaxis']['ticktext']
    dendro_leaves_side_idx = [clusters.index(label) for label in clusters_ordered]

    # Set the position of the dendrogram in the figure grid
    for i in range(len(fig['data'])):
        fig['data'][i]['xaxis'] = 'x5'
        fig['data'][i]['yaxis'] = 'y2'

    # If there are no state markers selected then use only the type markers
    # Transpose bc the markers are initially columns.
    if state_markers != ['']:
        dendro_state_markers = cluster_summaries[state_markers].transpose()
        dendro_type_markers = cluster_summaries[type_markers].transpose()

        # Create top Dendrogram for type marker hmap
        dendro_up = ff.create_dendrogram(dendro_type_markers,orientation='bottom')
        dendro_leaves_top_idx = list(map(int, dendro_up['layout']['xaxis']['ticktext']))

        for i in range(len(dendro_up['data'])):
            dendro_up['data'][i]['yaxis'] = 'y5'
            dendro_up['data'][i]['xaxis'] = 'x3'

        # Add Side Dendrogram Data to Figure
        for data in dendro_up['data']:
            fig.add_trace(data)

        # Cluster both separately marker sets and reorder the df's
        dist1 = pdist(dendro_state_markers, metric='euclidean')
        link1 = linkage(dist1, metric='euclidean', method='complete')
        heat_data_state = dendro_state_markers.transpose().iloc[dendro_leaves_side_idx, leaves_list(link1)]
        heat_data_type = dendro_type_markers.transpose().iloc[dendro_leaves_side_idx, dendro_leaves_top_idx]

        zmin, zmax = None, None
        if normalize == 'min-max':
            df = (cluster_summaries-cluster_summaries.min())/(cluster_summaries.max()-cluster_summaries.min())
            zmin, zmax = create_colorscale(df)
            heat_data_type = (heat_data_type-heat_data_type.min())/(heat_data_type.max()-heat_data_type.min())
            heat_data_state = (heat_data_state-heat_data_state.min())/(heat_data_state.max()-heat_data_state.min())
        elif normalize == 'z-score':
            df = (cluster_summaries-cluster_summaries.mean())/cluster_summaries.std()
            zmin, zmax = create_colorscale(df)
            heat_data_type = (heat_data_type-heat_data_type.mean())/heat_data_type.std()
            heat_data_state = (heat_data_state-heat_data_state.mean())/heat_data_state.std()

        type_markers_ordered = list(heat_data_type.columns)
        state_markers_ordered = list(heat_data_state.columns)

        # Create hover data for cluster/marker heatmap
        cluster_counts = [cluster_cell_counts[idx] for idx in dendro_leaves_side_idx]
        cluster_proportions = [cluster_freqs[idx] for idx in dendro_leaves_side_idx]
        hovertext_type = create_hmap_hover_txt(type_markers_ordered, clusters_ordered, cluster_counts, cluster_proportions, y_flag=True)
        hovertext_state = create_hmap_hover_txt(state_markers_ordered, clusters_ordered, cluster_counts, cluster_proportions, y_flag=True)

        # Create the cluster/type marker heatmap
        dec = 1 if len(type_markers_ordered) >= 60 else 2
        heatmap1 = [go.Heatmap(
            z = heat_data_state.round(decimals=dec),
            x = state_markers_ordered,
            y = clusters_ordered,
            text= hovertext_state,
            zmin=zmin,
            zmax=zmax,
            hoverinfo='text+z',
            colorscale = 'Viridis',
            showscale=False
        )]

        # y-axis tick-values need to be the same as in the dendrogram
        heatmap1[0]['y'] = fig['layout']['yaxis']['tickvals']

        # Add heatmap data to figure
        for data in heatmap1:
            data['xaxis'] = 'x4'
            data['yaxis'] = 'y2'
            fig.add_trace(data)

        # Create the cluster/state marker heatmap
        dec = 1 if len(state_markers_ordered) >= 60 else 2
        heatmap2 = [go.Heatmap(
            z = heat_data_type.round(decimals=dec),
            x = type_markers_ordered,
            y = clusters_ordered,
            text= hovertext_type,
            zmin=zmin,
            zmax=zmax,
            hoverinfo='text+z',
            colorscale = 'Viridis',
            showscale=False
        )]
        # y-axis tick-values need to be the same as in the dendrogram
        heatmap2[0]['y'] = fig['layout']['yaxis']['tickvals']
        heatmap2[0]['x'] = dendro_up['layout']['xaxis']['tickvals']

        # Add heatmap data to figure
        for data in heatmap2:
            data['xaxis'] = 'x3'
            data['yaxis'] = 'y2'
            fig.add_trace(data)

        fig.update_layout(xaxis4={'domain':[0.05, x_state_markers],
                                  'mirror': False,
                                  'showgrid': False,
                                  'showline': False,
                                  'zeroline': False,
                                  'showticklabels': True,
                                  'side':'bottom',
                                  'ticktext':state_markers_ordered,
                                  'tickangle':60,
                                  'ticks':""})

        fig.update_layout(xaxis3={'domain':[x_state_markers+0.005, x_state_markers+x_type_markers],
                                  'mirror': False,
                                  'showgrid': False,
                                  'showline': False,
                                  'zeroline': False,
                                  'showticklabels': True,
                                  'side':'bottom',
                                  'tickvals':dendro_up['layout']['xaxis']['tickvals'],
                                  'ticktext':type_markers_ordered,
                                  'tickangle':60,
                                  'ticks':""})

        annotations1 = heatmap_annotations(heatmap1[0], fsize_a, yref='y2', xref='x4')
        annotations2 = heatmap_annotations(heatmap2[0], fsize_a, yref='y2', xref='x3')
        annotations = annotations1+annotations2

        # Create annotatons bars
        for ix, marker in enumerate(type_markers_ordered):
            regex = '^(?!.*{}).*$'.format(marker)
            marker_data = pd.Series(type_markers_ordered)
            marker_data = marker_data.replace(regex, 'NaN', regex=True)
            marker_data = marker_data.replace(marker, 1)
            colorscale = [[0, '#3a34eb'], [1, '#3a34eb']]
            ann_heatmap = [go.Heatmap(
                        z=[marker_data],
                        name=marker,
                        hoverinfo='text',
                        colorscale=colorscale,
                        showscale=False,
                        xgap=1,
                        ygap=1
                    )]
            ann_heatmap[0]['x'] = dendro_up['layout']['xaxis']['tickvals']

            for data in ann_heatmap:
                data['xaxis'] = 'x3'
                data['yaxis'] = 'y3'
                texts = [[]]
                for marker in type_markers_ordered:
                    t = ''
                    t += 'marker: {}<br>cell type marker'.format(marker)
                    texts[0].append(t)
                data['text'] = texts
                fig.add_trace(data)


        for ix, marker in enumerate(state_markers_ordered):
            regex = '^(?!.*{}).*$'.format(marker)
            marker_data = pd.Series(state_markers_ordered)
            marker_data = marker_data.replace(regex, 'NaN', regex=True)
            marker_data = marker_data.replace(marker, 1)
            colorscale = [[0, '#ebe71e'], [1, '#ebe71e']]
            ann_heatmap = [go.Heatmap(
                        z=[marker_data],
                        name=marker,
                        hoverinfo='text',
                        colorscale=colorscale,
                        showscale=False,
                        xgap=1,
                        ygap=1
                    )]
            for data in ann_heatmap:
                data['xaxis'] = 'x4'
                data['yaxis'] = 'y3'
                texts = [[]]
                for marker in state_markers_ordered:
                    t = ''
                    t += 'marker: {}<br>cell state marker'.format(marker)
                    texts[0].append(t)
                data['text'] = texts
                fig.add_trace(data)
    else:
        # Build the new top dendrogram
        dendro_type_markers = cluster_summaries[type_markers].transpose()
        dendro_top = ff.create_dendrogram(dendro_type_markers, orientation='bottom')
        dendro_leaves_top_idx = list(map(int, dendro_top['layout']['xaxis']['ticktext']))

        for i in range(len(dendro_top['data'])):
            dendro_top['data'][i]['yaxis'] = 'y5'
            dendro_top['data'][i]['xaxis'] = 'x4'

        # Add Side Dendrogram Data to Figure
        for data in dendro_top['data']:
            fig.add_trace(data)

        heat_data_clusters = dendro_type_markers.transpose().iloc[dendro_leaves_side_idx, dendro_leaves_top_idx]

        if normalize == 'min-max':
            heat_data_clusters = (heat_data_clusters-heat_data_clusters.min())/(heat_data_clusters.max()-heat_data_clusters.min())
        elif normalize == 'z-score':
            heat_data_clusters = (heat_data_clusters-heat_data_clusters.mean())/heat_data_clusters.std()

        # reorder markers based on the clustering results above
        markers_ordered = list(heat_data_clusters.columns)

        # Create hover data for cluster/marker heatmap
        cluster_counts = [cluster_cell_counts[idx] for idx in dendro_leaves_side_idx]
        cluster_proportions = [cluster_freqs[idx] for idx in dendro_leaves_side_idx]
        hovertext = create_hmap_hover_txt(markers_ordered, clusters_ordered, cluster_counts, cluster_proportions, y_flag=True)

        # Create the cluster/marker heatmap
        dec = 1 if len(markers_ordered) >= 60 else 2
        heatmap = [go.Heatmap(
            z = heat_data_clusters.round(decimals=dec),
            x = markers_ordered,
            y = clusters_ordered,
            text= hovertext,
            hoverinfo='text+z',
            colorscale = 'Viridis',
            showscale=False
        )]

        # y-axis tick-values need to be the same as in the dendrogram
        heatmap[0]['y'] = fig['layout']['yaxis']['tickvals']
        heatmap[0]['x'] = dendro_top['layout']['xaxis']['tickvals']

        # Add heatmap data to figure
        for data in heatmap:
            data['xaxis'] = 'x4'
            data['yaxis'] = 'y2'
            fig.add_trace(data)

        for ix, marker in enumerate(markers_ordered):
            regex = '^(?!.*{}).*$'.format(marker)
            marker_data = pd.Series(markers_ordered)
            marker_data = marker_data.replace(regex, 'NaN', regex=True)
            marker_data = marker_data.replace(marker, 1)
            colorscale = [[0, '#3a34eb'], [1, '#3a34eb']]
            ann_heatmap = [go.Heatmap(
                        z=[marker_data],
                        name=marker,
                        hoverinfo='text',
                        colorscale=colorscale,
                        showscale=False,
                        xgap=1,
                        ygap=1
                    )]
            ann_heatmap[0]['x'] = dendro_top['layout']['xaxis']['tickvals']

            for data in ann_heatmap:
                data['xaxis'] = 'x4'
                data['yaxis'] = 'y3'
                texts = [[]]
                for marker in markers_ordered:
                    t = ''
                    t += 'marker: {}<br>cell type marker'.format(marker)
                    texts[0].append(t)
                data['text'] = texts
                fig.add_trace(data)



        fig.update_layout(xaxis4={'domain':[0.05, x_type_markers+x_state_markers],
                                  'mirror': False,
                                  'showgrid': False,
                                  'showline': False,
                                  'zeroline': False,
                                  'showticklabels': True,
                                  'side':'bottom',
                                  'tickvals':dendro_top['layout']['xaxis']['tickvals'],
                                  'ticktext':markers_ordered,
                                  'tickangle':60,
                                  'ticks':""})

        annotations = heatmap_annotations(heatmap[0], fsize_a, yref='y2', xref='x4')

    # Reorder the sample_cluster_counts df and samples list
    if ordering_samples == 'None':
        dist4 = pdist(sample_cluster_counts, metric='euclidean')
        link4 = linkage(dist4, metric='euclidean', method='complete')
        heat_data_counts = sample_cluster_counts.transpose().iloc[dendro_leaves_side_idx, leaves_list(link4)]
        samples_ordered = [samples[i] for i in leaves_list(link4)]

        # Create hovertext for the sample/cluster heatmap
        sample_cell_counts = [sample_cell_counts[idx] for idx in leaves_list(link4)]
        sample_proportions = [sample_freqs[idx] for idx in leaves_list(link4)]
        hovertext = create_hmap_hover_txt(samples_ordered, clusters_ordered, sample_cell_counts, sample_proportions)
    else:
        sample_counts['sum'] = sample_counts.groupby([ordering_samples])['Count'].transform(sum)
        sample_counts = sample_counts.sort_values(['sum','Count'], ascending=[False, False])
        samples_ordered=sample_counts['sample']
        new_idxs = [samples.index(sample) for sample in samples_ordered]
        heat_data_counts = sample_cluster_counts.transpose().iloc[dendro_leaves_side_idx, new_idxs]

        # Create hovertext for the sample/cluster heatmap
        sample_cell_counts = [sample_cell_counts[idx] for idx in new_idxs]
        sample_proportions = [sample_freqs[idx] for idx in new_idxs]
        hovertext = create_hmap_hover_txt(samples_ordered, clusters_ordered, sample_cell_counts, sample_proportions)

    # Create the heatmap
    samplemap = [go.Heatmap(
        z = heat_data_counts,
        x = samples_ordered,
        y = clusters_ordered,
        text=hovertext,
        hoverinfo='text+z',
        colorscale = 'Portland',
        showscale=False
    )]

    samplemap[0]['y'] = fig['layout']['yaxis']['tickvals']

    for data in samplemap:
        data['xaxis'] = 'x2'
        data['yaxis'] = 'y2'
        fig.add_trace(data)

    # Set the y-domain for the annotation bar
    ncol = len(pd.DataFrame(meta_data).columns)
    yrange = 0.12
    slot_h = yrange/4
    y_domain = slot_h*ncol

    if ncol > 4:
        y_domain = 0.12

    # Samplemap sample annotations,
    # Hackish solution to create a categorical heatmap based on this pen:
    # https://codepen.io/etpinard/pen/GmbVZq?editors=0010
    meta_data = pd.merge(pd.DataFrame({'sample':samples_ordered}), meta_data)
    for ix, col in enumerate(meta_data.columns):
        if col != 'sample':
            uniques = pd.unique(meta_data[col])
            for i, uniq in enumerate(uniques):
                regex = '^(?!.*{}).*$'.format(uniq)
                data = meta_data.replace(regex, 'NaN', regex=True)
                data = data.replace(uniq, 1).transpose()
                data = data.drop(['sample'])
                color = colors_meta[col][uniq]
                colorscale = [[0, color], [1, color]]

                ann_heatmap = [go.Heatmap(
                    z = data,
                    name=uniq,
                    hoverinfo='text',
                    colorscale=colorscale,
                    showscale=False,
                    xgap=1,
                    ygap=1
                )]
                for data in ann_heatmap:
                    data['xaxis'] = 'x2'
                    data['yaxis'] = 'y4'
                    texts=[]
                    for column in meta_data.columns:
                        if column != 'sample':
                            t = []
                            for value in meta_data[column]:
                                text ='{}: {}'.format(column, value)
                                t.append(text)
                            texts.append(t)
                    data['text'] = texts
                    fig.add_trace(data)

    annotations2 = heatmap_annotations(samplemap[0], fsize_a, yref='y2', xref='x2')
    annotations = annotations + annotations2
    fig['layout']['annotations'] = annotations

    # Create a count plot on the side of the heatmap
    bar1 = [go.Bar(
        x = cluster_counts,
        y = clusters_ordered,
        orientation='h',
        hoverinfo='x',
        marker=dict(
            color='midnightblue',
        )
    )]

    bar1[0]['y'] = fig['layout']['yaxis']['tickvals']

    for data in bar1:
        data['xaxis'] = 'x6'
        data['yaxis'] = 'y2'
        fig.add_trace(data)

    # Edit the layout of the figure
    fig.update_layout(
        showlegend=False,
        hovermode='closest',
        margin=dict(l=5, r=5, t=30),
        plot_bgcolor='rgb(255,255,255)'
    )
    # Default yaxis
    fig.update_layout(yaxis2={'domain': [0, 0.88],
                              'mirror': False,
                              'showgrid': False,
                              'showline': False,
                              'zeroline': False,
                              'showticklabels':True,
                              'side':'right',
                              'tickvals':fig['layout']['yaxis']['tickvals'],
                              'ticktext':clusters_ordered,
                              'ticks':""})

    # marker annotations
    fig.update_layout(yaxis3={'domain': [0.88, 0.91],
                              'mirror': False,
                              'showgrid': False,
                              'showline': False,
                              'zeroline': False,
                              'showticklabels':False,
                              'ticks':""})

    # Sample annotations
    fig.update_layout(yaxis4={'domain': [0.88, 0.88+y_domain],
                              'mirror': False,
                              'showgrid': False,
                              'showline': False,
                              'zeroline': False,
                              'showticklabels':False,
                              'ticks':""})

    # Upper dendro
    fig.update_layout(yaxis5={'domain': [0.91, 1],
                              'mirror': False,
                              'showgrid': False,
                              'showline': False,
                              'zeroline': False,
                              'showticklabels':False,
                              'ticks':""})

    # side dendro
    fig.update_layout(xaxis5={'domain': [0, .05],
                              'mirror': False,
                              'showgrid': False,
                              'showline': False,
                              'zeroline': False,
                              'showticklabels': False,
                              'ticks': ""
                            })

    # Sample hmap
    fig.update_layout(xaxis2={'domain': [x_state_markers+x_type_markers+0.005, 0.89],
                              'mirror': False,
                              'showgrid': False,
                              'showline': False,
                              'zeroline': False,
                              'showticklabels': True,
                              'tickangle':60,
                              'ticks': ""
                            })

    # Side barplot
    fig.update_layout(xaxis6={'domain': [0.95, 1],
                              'mirror': False,
                              'showgrid': False,
                              'showline': False,
                              'zeroline': False,
                              'showticklabels': False,
                              'ticks': ""
                            })

    # Add dropdown
    fig.update_layout(
        updatemenus=[
            go.layout.Updatemenu(
                type = "buttons",
                direction = "left",
                buttons=list([
                    dict(
                        args=[
                            {"visible": True},
                            {"annotations": annotations},
                        ],
                        label="Annotations on",
                        method="update"
                    ),
                    dict(
                        args=[
                            {"visible": True},
                            {"annotations": []},
                        ],
                        label="Annotations off",
                        method="update"
                    )
                ]),
                showactive=True,
                xanchor="left",
                x=0.05,
                y=1.07,
                yanchor="top"
            ),
        ]
    )
    return fig


def heatmap_dendro(data, yaxis, xaxis, hover1, hover2, dec, fsize, fsize_x=10, fsize_y=10, title=None):
    """returns a heatmap with an upper dendrogram as plotly figure object"""
    dendro_data = data
    # Transpose heatmap data to have antibodies as rows
    heat_data = data.transpose().values

    # Create upper dendrogram
    dendro = ff.create_dendrogram(dendro_data, orientation='bottom', labels=xaxis)
    xaxis_ordered = dendro['layout']['xaxis']['ticktext']

    # Create side dendrogram
    dendro_side = ff.create_dendrogram(heat_data, orientation='right', labels=yaxis)
    markers_ordered = dendro_side['layout']['yaxis']['ticktext']

    # Sort heatmap data by dendogram results
    dendro_leaves_side_idx = [yaxis.index(label) for label in markers_ordered]
    dendro_leaves_up_idx = [xaxis.index(label) for label in xaxis_ordered]
    heat_data = heat_data[dendro_leaves_side_idx,:]
    heat_data = heat_data[:,dendro_leaves_up_idx]

    # Create hover data
    counts = [hover1[idx] for idx in dendro_leaves_up_idx]
    proportions = [hover2[idx] for idx in dendro_leaves_up_idx]
    hovertext = create_hmap_hover_txt(xaxis_ordered, markers_ordered, counts, proportions, x_flag=True)

    # Create heatmap
    heatmap = go.Heatmap(
        z=heat_data.round(decimals=dec),
        y=yaxis,
        x=xaxis_ordered,
        text=hovertext,
        hoverinfo='text+z',
        colorscale = 'Viridis',
        showscale=False
    )

    # switch heatmap axes to dendrogram tickvals to match the xaxis
    heatmap['x'] = dendro['layout']['xaxis']['tickvals']
    heatmap['y'] = dendro_side['layout']['yaxis']['tickvals']

    # Create an own yaxis for the dendrogram
    for i in range(len(dendro.data)):
        dendro.data[i].yaxis='y2'

    # Create the figure object
    figure = subplots.make_subplots(
        rows=3, cols=1,
        vertical_spacing=0.02,
        print_grid=False,
        shared_xaxes=True
    )

    for fig in dendro.data:
        figure.add_trace(fig)

    # Add heatmap to the figure
    figure.add_trace(heatmap)

    # Create annotations for the heatmap
    annotations = heatmap_annotations(heatmap, fsize)

    # Edit layout of the plot
    figure['layout'].update({#'height':height,
                             'showlegend':False,
                             'hovermode': 'closest'})

    figure['layout']['xaxis'].update({'mirror': False,
                                      'showgrid': False,
                                      'showline': False,
                                      'zeroline': False,
                                      'tickfont': {'size':fsize_x},
                                      'tickangle':50,
                                      'showticklabels': True,
                                      'ticks':""})

    figure['layout']['yaxis'].update({'domain':[0,.9],
                                      'mirror': False,
                                      'showgrid': False,
                                      'showline': False,
                                      'zeroline': False,
                                      'tickfont': {'size':fsize_y},
                                      'ticks':""})

    figure['layout']['yaxis2'].update({'domain':[0.9,1],
                                      'mirror': False,
                                      'showgrid': False,
                                      'showline': False,
                                      'zeroline': False,
                                      'showticklabels': False,
                                      'ticks':""})

    figure['layout']['annotations'] = annotations
    figure['layout']['yaxis']['ticktext'] = markers_ordered
    figure['layout']['yaxis']['tickvals'] = dendro_side['layout']['yaxis']['tickvals']
    figure['layout']['xaxis']['ticktext'] = xaxis_ordered
    figure['layout']['xaxis']['tickvals'] = dendro['layout']['xaxis']['tickvals']
    figure['layout']['title'] = title
    figure['layout']['plot_bgcolor']='rgb(255,255,255)'
    return figure


def countplot(counts, valueMeta, colors, orientation='v', fontsize=9, titlepos=0.44):
    """Returns a barplot created from the sample counts table created by the cyto pipeline"""
    counts['sum'] = counts.groupby([valueMeta])['count'].transform(sum)
    counts = counts.sort_values(['sum','count'], ascending=[False, False])
    total_count = counts['count'].sum()
    data = []
    colors_pie = []
    for val in counts[valueMeta]:
        colors_pie.append(colors[val])

    data.append(
        go.Pie(
            labels=counts[valueMeta],
            values=counts['count'],
            hole=.3,
            sort=False,
            hoverinfo='label+text+value+percent',
            textinfo='percent',
            showlegend=True,
            marker=dict(
                colors=colors_pie,
            ),
            domain=dict(
                x = [0.02, 0.28],
                y = [0, 0.96]
            )
        )
    )

    for index, row in counts.iterrows():
        data.append(go.Bar(
            x=[row['sample']],
            y=[row['count']],
            name=row[valueMeta],
            marker=dict(
                color=colors[row[valueMeta]],
                line=dict(
                    color='#000000',
                    width=0.5
            )),
            legendgroup=row[valueMeta],
            text=['{}<br>{}<br>{}%'.format(row['sample'], row['count'], np.round(row['count']/total_count*100, 2))],
            hoverinfo='text',
            xaxis='x1',
            yaxis='y1',
            showlegend=False,
        ))

    layout = go.Layout(
                plot_bgcolor='rgb(255,255,255)',
                title="Cells selected: {}".format(total_count),
                margin=dict(r=5, t=35),
                xaxis=dict(
                    linecolor='#bdbdbd',
                    domain=[0.43, 1]
                ),
                yaxis=dict(
                    domain=[0, 1],
                    showticklabels=True,
                    showgrid=False,
                    showline=True,
                    gridcolor='#bdbdbd',
                    linecolor='#bdbdbd',
                ),
            )
    fig = go.Figure(data, layout)

    return fig


def countplot_index(grouped_data, colors, total_count, orientation='v', fontsize=9, title=''):
    """Returns a barplot created from the sample counts table created by the cyto pipeline"""
    data = []
    for key, df in grouped_data:
        for index, row in df.iterrows():
            data.append(go.Bar(
                x=[row['sample']],
                y=[row['Count']],
                name=key,
                marker=dict(
                    color=colors[key],
                    line=dict(
                        color='#000000',
                        width=0.5
                )),
                text=['{}<br>{}<br>{}%'.format(row['sample'], row['Count'], np.round(row['Count']/total_count*100, 2))],
                legendgroup=key,
                showlegend=False,
                hoverinfo='text'
            ))

    layout = go.Layout(
                margin=dict(l=35, r=35, t=35),
                plot_bgcolor='rgb(255,255,255)',
                yaxis=dict(
                    showline=True,
                    showgrid=False,
                    gridcolor='#bdbdbd',
                    linecolor='#bdbdbd'
                ),
                xaxis=dict(
                    linecolor='#bdbdbd',
                ),
                title=dict(
                    text=title,

                )
            )
    fig = go.Figure(data, layout)
    return fig


def px_boxplot(exprs_melt, x, y, colors, groupn, shapes=[]):
    """plotly express boxplot from melted dataframes from the pipeline"""
    if groupn == 'None':
        fig = px.box(exprs_melt, x=x, y=y, color=x)
        for data in fig['data']:
            data['boxpoints'] = False
            group = data['legendgroup'].split("=")[1]
            data['name'] = group
            data['legendgroup'] = None
            data['alignmentgroup'] = None
            data['offsetgroup'] = None
            color = data['marker']['color']
            data['line']['color'] = 'black'
            data['fillcolor'] = color
            data['x'] = None
            data['x0'] = None
            data['y0'] = None
            data['line']['width'] = 0.5
            data['whiskerwidth'] = 0.5

        fig['layout']['boxmode']=None
        fig['layout']['height']=None
        fig['layout']['margin']=None
        fig['layout']['xaxis']=None
        fig['layout']['yaxis']=None
    else:
        color=colors
        fig = px.box(exprs_melt, x=x, y=y,
                     color=groupn, hover_name=groupn)

        for data in fig['data']:
            group = data['legendgroup'].split("=")[1]
            data['boxpoints'] = False
            data['name'] = group
            data['legendgroup'] = group
            data['line']['color'] = 'black'
            data['fillcolor'] = colors[group]
            data['line']['width'] = 0.5
            data['whiskerwidth'] = 0.5

    fig.update_layout(
        margin=dict(l=20, r=5, t=35, b=35),
        showlegend=True,
        plot_bgcolor='rgb(255,255,255)',
        height=None,
        yaxis=dict(
            showline=True,
            showticklabels=True,
            gridcolor='#dbdbdb',
            linecolor='#dbdbdb',
            title=None
        ),
        xaxis=dict(
            showline=True,
            showticklabels=True,
            tickangle=60,
            gridcolor='#dbdbdb',
            linecolor='#dbdbdb',
            title=None
        ),
        updatemenus=[
            go.layout.Updatemenu(
                type = "buttons",
                direction = "left",
                buttons=list([
                    dict(
                        args=[
                            {"showlegend": True},
                        ],
                        label="show legend",
                        method="update"
                    ),
                    dict(
                        args=[
                            {"showlegend": False},
                        ],
                        label="hide legend",
                        method="update"
                    )
                ]),
                pad={"r": 10,},
                showactive=True,
                xanchor="left",
                y=1.2,
                yanchor="top"
            ),
        ],
        shapes=shapes
    )
    return fig


def boxplot(loop_data, data, showlegend=False, boxpoints=False,
            fontsize=10, colors=None, value_color=None, title=''):
    """Returns plotly boxplots from different boxplottable data created by the cyto pipeline"""
    if colors != None:
        colors = colors
    else:
        colors = ['rgb(0,0,0,0.1)']*len(loop_data)
    data_box = []
    for col in loop_data:

        if value_color != None:
            y = data[col][value_color]
        else:
            y = data[col]

        data_box.append(go.Box(
            y=y,
            name=col,
            showlegend=showlegend,
            line=dict(
                 width=0.5,
                 color=colors[col]
            ),
            whiskerwidth=0.5,
            boxpoints=boxpoints
        ))

    boxplotlayout = go.Layout(
        legend = dict(font=dict(size=fontsize), tracegroupgap=4),
        margin=dict(l=20, r=5, t=35),
        xaxis=dict(
            showgrid=True,
            showline=True,
            tickangle=50,
            tickfont=dict(size=fontsize),
            showticklabels=True,
            ticks='',
            gridcolor='#dbdbdb',
            linecolor='#dbdbdb'
        ),
        yaxis=dict(
            showline=True,
            showticklabels=True,
            ticks='',
            gridcolor='#dbdbdb',
            linecolor='#dbdbdb'
        ),
        plot_bgcolor='rgb(255,255,255)'
    )
    fig = go.Figure(data_box, boxplotlayout)

    fig.update_layout(
        updatemenus=[
            go.layout.Updatemenu(
                type = "buttons",
                direction = "left",
                buttons=list([
                    dict(
                        args=[
                            {"showlegend": True},
                        ],
                        label="show legend",
                        method="update"
                    ),
                    dict(
                        args=[
                            {"showlegend": False},
                        ],
                        label="hide legend",
                        method="update"
                    )
                ]),
                pad={"r": 10,},
                showactive=True,
                xanchor="left",
                y=1.2,
                yanchor="top"
            ),
        ]
    )

    return fig

def scatter(grouped_data, colors_meta, type='tSNE',  cmin=None, cmax=None,
            showlegend=False, colorscale=None, value_color=None, fontsize=12, height=None):
    """Returns (GL) a plotly scatter plot from tsne/umap data created by the cyto pipeline"""
    if colorscale != None:
        scatterdata = []
        for key in grouped_data.groups.keys():
            df = grouped_data.get_group(key)

            # Get the embedding type
            y, x = None, None
            if type == 'tSNE':
                x=df['tsne1']
                y=df['tsne2']
            elif type == 'UMAP':
                x=df['UMAP1']
                y=df['UMAP2']

            scatterdata.append(go.Scattergl(
                y = y,
                x = x,
                mode = 'markers',
                name = key,
                text = df[value_color],
                marker = dict(size=3,
                              cmin=cmin,
                              cmax=cmax,
                              color=df[value_color],
                              colorscale='Jet',
                              showscale=True,
                              colorbar=dict(
                                  title='Expression'
                              )
                        ),
                hoverinfo='text+name'
            ))
    else:
        scatterdata = []
        for key in grouped_data.groups.keys():
            df = grouped_data.get_group(key)
            text = df[value_color]

            # Get the embedding type
            y, x = None, None
            if type == 'tSNE':
                x=df['tsne1']
                y=df['tsne2']
            elif type == 'UMAP':
                x=df['UMAP1']
                y=df['UMAP2']

            scatterdata.append(go.Scattergl(
                y = y,
                x = x,
                mode = 'markers',
                name = key,
                text = text,
                marker = dict(size=4, color=colors_meta[value_color][key]),
                hoverinfo='all'
            ))

    scatterlayout = go.Layout(
        hovermode='closest',
        showlegend=showlegend,
        xaxis=dict(
            title='{}1'.format(type),
            gridwidth=2,
            showticklabels=True,
            linecolor='#bdbdbd',
            gridcolor='#f5f5f5',
            zeroline=False
        ),
        yaxis=dict(
            title='{}2'.format(type),
            gridwidth=2,
            showticklabels=True,
            linecolor='#bdbdbd',
            gridcolor='#f5f5f5',
            zeroline=False
        ),
        legend = dict(
            font=dict(size=fontsize),
            y=0.97
        ),
        uirevision=type,
        margin=dict(l=5, r=5, t=35),
        plot_bgcolor='rgb(255,255,255)'

    )
    fig = go.Figure(data=scatterdata, layout=scatterlayout)
    return fig


def mdsplot(grouped_data, colors, samples, markersize=6, opacity=1, title=None, fontsize=10):
    """Returns plotly scatterplot from the mds data created by the cyto pipeline"""
    scatterdata = []
    for key, frame in grouped_data:
        df = frame[['MDS1', 'MDS2']]
        for index, row in df.iterrows():
            scatterdata.append(go.Scatter(
                mode='markers',
                x=[row['MDS1']],
                y=[row['MDS2']],
                name=key,
                text=samples[index],
                marker=dict(
                    size=markersize,
                    color=colors[key],
                    line=dict(
                        color='#000000',
                        width=0.3
                    ),
                    opacity=opacity
                ),
                hoverinfo='text+name',
                legendgroup=key
            ))

    scatterlayout = go.Layout(
        title=title,
        showlegend=True,
        legend = dict(font=dict(size=fontsize), tracegroupgap=4),
        xaxis=dict(
            showgrid=True,
            zeroline=False,
            gridcolor='#bdbdbd',
            linecolor='#bdbdbd',
            gridwidth=1,
            dtick=0.2,
            ticks='',
            title='logFC MDS-1'
            ),
        yaxis=dict(
            showgrid=True,
            zeroline=False,
            gridcolor='#bdbdbd',
            linecolor='#bdbdbd',
            gridwidth=1,
            dtick=0.2,
            ticks='',
            title='logFC MDS-2'
        ),
        plot_bgcolor='rgb(255,255,255)'
    )
    return go.Figure(scatterdata, scatterlayout)


def nrsplot(grouped_data, data, colors, markers, samples, markersize=6, title=None, tickangle=0, fontsize=10):
    """"Returns plotly scatter+boxplot figure from non-redundancy-score data created by the cyto pipeline"""
    data_nrs = []
    for key, frame in grouped_data:
        df = frame[markers]
        for idx, row in df.iterrows():
            data_nrs.append(go.Scatter(
                x = list(data.columns),
                y = row,
                name=samples[idx],
                marker=dict(
                    size=6,
                    color=colors[key]
                ),
                mode='markers',
                showlegend=True,
                hoverinfo='all',
                legendgroup=key)
            )

    for col in data.columns:
        data_nrs.append(go.Box(
            y=data[col],
            name=col,
            line=dict(
                width=1.5,
                color='rgb(0,0,0, 0.1)'
            ),
            whiskerwidth=0.1,
            boxpoints=False,
            showlegend=False)
        )

    nrslayout = go.Layout(
        title=title,
        xaxis=dict(
            showgrid=True,
            showline=True,
            tickangle=tickangle,
            tickfont=dict(size=fontsize),
            showticklabels=True,
            ticks='outside',
            linecolor='#bdbdbd'
        ),
        yaxis=dict(
            showline=True,
            gridcolor='#bdbdbd',
            showticklabels=True,
            ticks='outside',
            linecolor='#bdbdbd'
        ),
        legend = dict(font=dict(size=fontsize), tracegroupgap=4),
        plot_bgcolor='rgb(255,255,255)'
    )
    return go.Figure(data_nrs, nrslayout)


def corrplot(data, fontsize):
    """Returns a correlation heatmap from the correlation data created by the cyto pipeline"""
    corr_data = data.values.round(decimals=1).tolist()
    heatmap = go.Heatmap(
        z = corr_data,
        y = data.columns,
        x = data.columns,
        colorscale = 'Portland',
        showscale=False
    )

    annotations = heatmap_annotations(heatmap, fontsize)
    figure = go.Figure()
    figure.add_trace(heatmap)
    figure['layout']['annotations'] = annotations
    figure['layout']['xaxis']['tickfont'] = dict(size=9)
    figure['layout']['yaxis']['tickfont'] = dict(size=9)
    figure['layout']['xaxis']['tickangle'] = 50
    figure['layout']['title'] = 'Correlation Matrix for Antibodies'
    figure['layout']['plot_bgcolor']='rgb(255,255,255)'

    return figure


def create_mst_grid(centers):
    """Creates the edge and node positions of the mst plots from the mst data created by the cyto pipeline"""

    # Create the minimum spanning tree data
    data_dist=pdist(centers)
    mst_data = squareform(data_dist)

    # Create a graph object from the data
    G = nx.from_numpy_matrix(mst_data)
    MST = nx.minimum_spanning_tree(G)

    # Create positions for the MST nodes
    pos = graphviz_layout(MST, prog='neato')

    # Create edges
    edge_label=list(MST.edges())
    Xedges=[]
    Yedges=[]
    for e in edge_label:
        Xedges.extend([pos[e[0]][0], pos[e[1]][0], None])# x coordinates of the nodes defining the edge e
        Yedges.extend([pos[e[0]][1], pos[e[1]][1], None])# y coordinates of the nodes defining the edge e
    edges = (Xedges, Yedges)

    # Create nodes
    node_label=list(MST.nodes())
    Xnodes=[pos[k][0] for k in node_label]# x-coordinates of nodes
    Ynodes=[pos[k][1] for k in node_label]# y-coordinates of nodes
    nodes = (Xnodes, Ynodes)

    return nodes, edges


def single_mst_trace(nodes, edges, df, cmin, cmax, value1, value2, showscale=False):
    """Return a single MST tree as a plotly trace that can be used as input for plotly figure object"""
    data = []
    tree = go.Scatter(x=edges[0],
                      y=edges[1],
                      mode='lines',
                      line=dict(color='#a3bad2', width=2),
                      hoverinfo ='none',
                      showlegend=False)

    data.append(tree)

    # Hover data, colors and cluster sizes for the data
    sizes=df['prop']
    cellcounts=df['cellcounts']
    labels=df.index
    centers = df.drop(columns=['prop', 'cellcounts'])
    exprs = centers[value1]
    text = []
    for count, exprs, label, prop in zip(cellcounts, exprs, labels, sizes):
        text.append('cluster {}<br>cell count: {}<br>frequency: {}%<br>median expression: {}'.format(label, count, round(prop,2), round(exprs, 3)))

    # Decide sizing option for the plot from user input
    if value2 == 'total':
        size = np.sqrt(cellcounts)/3+5
    else:
        size = np.sqrt(sizes)*10+5

    # Create Scatter plot from the data
    tracer_marker = go.Scatter(x=nodes[0],
                               y=nodes[1],
                               mode='markers',
                               marker = dict(
                                   size=size,
                                   line=dict(width=1),
                                   cmin=cmin,
                                   cmax=cmax,
                                   color=centers[value1],
                                   colorscale='Jet',
                                   colorbar=dict(
                                       title='Median expression'
                                   ),
                                   showscale=showscale
                               ),
                               hoverinfo='text',
                               text=text,
                               showlegend=False)

    data.append(tracer_marker)
    return data


def plot_single_mst(trace, df, value1, title=''):
    """Return a plotly figure of one MST tree"""

    # Hover data, colors and cluster sizes for the data
    cellcounts=df['cellcounts']
    axis_style = dict(
        title='',
        titlefont=dict(size=20),
        showgrid=False,
        zeroline=False,
        showline=False,
        ticks='',
        showticklabels=False
    )

    layout = go.Layout(
        title='{}, {}, total cell count: {}'.format(value1, title, np.sum(cellcounts)),
        autosize=False,
        showlegend=False,
        xaxis=axis_style,
        yaxis=axis_style,
        hovermode='closest',
        plot_bgcolor='rgb(255,255,255)'

    )
    return go.Figure(trace, layout)


def plot_multiple_mst(nodes, edges, frames_dict, cmin, cmax, valueMarker, valueSize, columns=3):
    """Return a plotly figure with multiple mst-plots as subplots arranged in a 3-column grid"""
    rows = int(np.ceil(len(frames_dict) / columns))
    keys = list(frames_dict.keys())
    num_plots = len(keys)
    counts = []
    mod, remainder = divmod(num_plots, 3)

    if mod == 0:
        columns = num_plots

    plot = subplots.make_subplots(
        rows=rows,
        cols=columns,
        subplot_titles=keys,
        horizontal_spacing=0.02,
        vertical_spacing=0.02,
        print_grid=False
    )

    # Loop the grid and create each subplot
    i = 0
    for row in range(1, rows+1):
        for column in range(1, columns+1):
            if i < num_plots:
                counts.append(np.sum(frames_dict[keys[i]]['cellcounts']))
                tracelist=single_mst_trace(nodes, edges, frames_dict[keys[i]], cmin, cmax, valueMarker, valueSize)

                # Add each trace from a list of traces to the subplot trace
                for trace in tracelist:
                    plot.append_trace(trace, row, column)
                i += 1
            else:
                break

    # Layout styles
    axis_style = dict(
        title='',
        titlefont=dict(size=20),
        showgrid=False,
        zeroline=False,
        showline=False,
        ticks='',
        showticklabels=False
    )

    # Adjust plot height based on how many plots in num_plots
    # TODO: TEST more
    height = 750
    if 3 < num_plots <= 6:
        height *= 1.5
    elif 6 < num_plots <= 9:
        height *= 2
    elif 9 < num_plots <= 12:
        height *= 2.5
    elif 12 < num_plots <= 15:
        height *= 3
    elif 15 < num_plots <= 18:
        height *= 3.5
    elif num_plots > 20:
        height *= 4

    plot.layout.update(dict(
        height=height,
        xaxis=axis_style,
        yaxis=axis_style,
        hovermode='closest',
        plot_bgcolor='rgb(255,255,255)'
    ))

    # Change axis styles for each subplot
    for i in range(2, num_plots+1):
        plot['layout']['yaxis{}'.format(i)].update(axis_style)
        plot['layout']['xaxis{}'.format(i)].update(axis_style)

    # Change title for each subplot
    for i, obj in enumerate(plot['layout']['annotations']):
        obj['text'] = '{}, cell count: {}'.format(obj['text'], counts[i])

    return plot


def plot_simpson(grouped, colors, fontsize, title):
    """Return a plotly boxplot figure of the simpson indexes created by the cyto pipeline"""
    data_box = []
    for i, df in grouped:
        data_box.append(go.Box(y=df['simpson'],
                               boxpoints='all',
                               name=i,
                               text=df['sample'],
                               jitter=0.5,
                               pointpos=0,
                               marker = dict(
                                    color=colors[i]
                               )))

    boxplotlayout = go.Layout(
        legend = dict(font=dict(size=fontsize), tracegroupgap=4),
        xaxis=dict(
            showgrid=True,
            showline=True,
            tickangle=50,
            tickfont=dict(size=fontsize),
            showticklabels=True,
            ticks='outside',
            linecolor='#bdbdbd'
        ),
        yaxis=dict(
            showline=True,
            showticklabels=True,
            ticks='outside',
            linecolor='#bdbdbd',
            gridcolor='#bdbdbd'
        ),
        title=title,
        plot_bgcolor='rgb(255,255,255)'
    )
    return go.Figure(data_box, boxplotlayout)


def ridgeplot_sample(grouped, colors, value, marker, height=None):
    """return a ridgeplot of the specific marker for every sample"""
    fig = go.Figure()
    if value is not 'sample':
        for key, frame in grouped:
            for sample, fr in frame.groupby(['sample']):
                fig.add_trace(
                    go.Violin(
                        x=fr[marker],
                        name=sample,
                        line=dict(
                            color=colors[key]
                        ),
                        legendgroup=key
                    )
                )
    else:
        for sample, frame in grouped:
            fig.add_trace(
                    go.Violin(
                        x=frame[marker],
                        name=sample,
                        line=dict(
                            color=colors[sample]
                        ),
                        legendgroup=sample
                    )
                )

    fig.update_traces(
        orientation='h',
        side='positive',
        width=5,
        points=False
    )

    fig.update_layout(
        xaxis_zeroline=True,
        yaxis=dict(gridcolor='#bdbdbd', linecolor='#bdbdbd'),
        xaxis=dict(linecolor='#bdbdbd'),
        # height=height,
        title=marker,
        plot_bgcolor='rgb(255,255,255)'
    )

    return fig


def cluster_boxplots(frames, counts,  valueMeta, colors=None, shapes=None):
    """Splittable boxplots of the each ckusters marker expressions"""
    total_count = counts['count'].sum()
    if valueMeta == 'None':
        fig = px.box(frames, x='marker', y='expression', color='marker')
        for data in fig['data']:
            data['boxpoints'] = False
            group = data['legendgroup'].split("=")[1]
            data['name'] = group
            data['legendgroup'] = None
            data['alignmentgroup'] = None
            data['offsetgroup'] = None
            data['x'] = None
            data['x0'] = None
            data['y0'] = None
            color = data['marker']['color']
            data['line']['color'] = 'black'
            data['fillcolor'] = color
            data['line']['width'] = 0.5
            data['whiskerwidth'] = 0.5

        fig['layout']['boxmode']=None
        fig['layout']['height']=None
        fig['layout']['margin']=None
        fig['layout']['xaxis']=None
        fig['layout']['yaxis']=None


        colors_pie = []
        for val in counts['sample']:
            colors_pie.append(colors[val])

        fig.add_trace(
            go.Pie(
                labels=counts['sample'],
                values=counts['count'],
                hole=.3,
                sort=False,
                hoverinfo='label+text+value+percent',
                textinfo='percent',
                showlegend=False,
                marker=dict(
                    colors=colors_pie,
                ),
                domain=dict(
                    x = [0, 0.2],
                    y = [0.55, 0.96]
                )
            )
        )

        for index, row in counts.iterrows():
            fig.add_trace(go.Bar(
                x=[row['sample']],
                y=[row['count']],
                name=row['sample'],
                marker=dict(
                    color=colors[row['sample']],
                    line=dict(
                        color='#000000',
                        width=0.5
                )),
                legendgroup=row['sample'],
                text=['{}<br>{}<br>{}%'.format(row['sample'], row['count'], np.round(row['count']/total_count*100, 1))],
                hoverinfo='text',
                xaxis='x2',
                yaxis='y2',
                showlegend=False,
            ))
    else:
        fig = px.box(frames, x='marker', y='expression', color='group')
        for i, data in enumerate(fig['data']):
            group = data['legendgroup'].split("=")[1]
            data['boxpoints'] = False
            data['name'] = group
            data['legendgroup'] = group
            data['line']['width'] = 0.5
            data['whiskerwidth'] = 0.5
            data['line']['color'] = 'black'
            data['fillcolor'] = colors[group]

        colors_pie = []
        for val in counts[valueMeta]:
            colors_pie.append(colors[val])

        fig.add_trace(
            go.Pie(
                labels=counts[valueMeta],
                values=counts['count'],
                hole=.3,
                sort=False,
                showlegend=False,
                hoverinfo='label+text+value+percent',
                textinfo='label+percent',
                domain=dict(
                    x = [0, 0.2],
                    y = [0.55, 0.96],
                ),
                marker=dict(
                    colors=colors_pie
                )
            )
        )

        fig['layout']['height']=None

        for index, row in counts.iterrows():
            fig.add_trace(go.Bar(
                x=[row['sample']],
                y=[row['count']],
                name=row[valueMeta],
                marker=dict(
                    color=colors[row[valueMeta]],
                    line=dict(
                        color='#000000',
                        width=0.5
                )),
                text=['{}<br>{}<br>{}%'.format(row['sample'], row['count'], np.round(row['count']/total_count*100, 2))],
                legendgroup=row[valueMeta],
                hoverinfo='text',
                xaxis='x2',
                yaxis='y2',
                showlegend=False,
            ))

    fig.update_layout(
        margin=dict(l=15, r=5, t=35),
        xaxis=dict(
            domain=[0.24, 1],
            showgrid=True,
            showline=True,
            tickangle=50,
            showticklabels=True,
            ticks='',
            gridcolor='#dbdbdb',
            linecolor='#dbdbdb',
            title=''
        ),
        xaxis2=dict(
            domain=[0, 0.2],
            showgrid=True,
            showline=True,
            tickangle=50,
            showticklabels=True,
            ticks='',
            gridcolor='#dbdbdb',
            linecolor='#dbdbdb'
        ),
        yaxis=dict(
            showline=True,
            showticklabels=True,
            ticks='',
            gridcolor='#dbdbdb',
            linecolor='#dbdbdb',
            title=''
        ),
        yaxis2=dict(
            domain=[0, 0.5],
            showline=True,
            showticklabels=True,
            autorange=True,
            ticks='',
            gridcolor='#dbdbdb',
            linecolor='#dbdbdb',
            title='',
            anchor="free",
            overlaying="y2",
            side="left",
        ),
        plot_bgcolor='rgb(255,255,255)',
        title='Total number of cells: {}'.format(total_count)
    )
    fig.update_layout(shapes = shapes)
    return fig

################################################################################
################################################################################
# OLDIES
# def kdeplot(kde, quantiles, markers, title=''):
#     """Returns plotly distplots from the kde tables created by the cyto pipeline"""
#     fig = subplots.make_subplots(
#         rows=len(markers),
#         cols=1,
#         shared_xaxes=True,
#         vertical_spacing=0.05,
#         subplot_titles=markers,
#         print_grid=False
#     )
#
#     for i, marker in enumerate(markers):
#         vals_min = quantiles[marker][0.00]
#         vals_max = quantiles[marker][1.00]
#         q1 = quantiles[marker][0.25]
#         q2 = quantiles[marker][0.50]
#         q3 = quantiles[marker][0.75]
#
#         distribution = go.Scatter(
#             y=kde[marker],
#             x=np.linspace(vals_min, vals_max, 100),
#             mode='lines',
#             name='',
#             fill='tonextx',
#             line=go.scatter.Line(width=1, color='#f54266')
#         )
#
#         qq = go.Scatter(
#             y=[0, 0],
#             x=[q1, q3],
#             text=['lower-quartile: ' + '{:0.2f}'.format(q1),
#                   'upper-quartile: ' + '{:0.2f}'.format(q3)],
#             mode='lines',
#             line=go.scatter.Line(
#                 width=4,
#                 color='rgb(0,0,0)'
#             ),
#             hoverinfo='text'
#         )
#
#         med = go.Scatter(
#             y=[0],
#             x=[q2],
#             text=['median: ' + '{:0.2f}'.format(q2)],
#             mode='markers',
#             marker=dict(symbol='square', color='rgb(255,255,255)'),
#             hoverinfo='text'
#         )
#         ff = [distribution, qq, med]
#
#         yaxis = go.layout.YAxis(
#             ticks='',
#             showline=False,
#             zeroline=True,
#             showgrid=False,
#             mirror=False,
#             showticklabels=False
#         )
#
#         for item in ff:
#             fig.append_trace(item, i+1, 1)
#
#     height = 1375
#     if len(markers) <= 2:
#         height = 450
#     elif 2 < len(markers) <= 4:
#         height = 750
#     elif 4 < len(markers) <= 8:
#         height = 1050
#
#     fig.layout.update(
#         showlegend=False,
#         height=height,
#         title=title,
#         # plot_bgcolor='rgb(255,255,255)'
#     )
#     return fig


# def double_heatmap_dendro(data1, data2, yaxis1, yaxis2, xaxis,
#                           hover1, hover2, hover3, hover4, height,
#                           ordering, fsize_x=10, fsize_y=10, title=None):
#     """returns two heatmaps with a shared upper dendrogram as plotly figure object"""
#
#     dendro_data = data1
#     # Transpose upper heatmap values to have antibodies as rows
#     heat_data1 = data1.transpose().values
#     # Heatmap data for the lower heatmap
#     heat_data2 = data2.values
#
#     # Choose dataset for dendrogram
#     if ordering == 'clustersimilarity':
#         X = dendro_data
#     elif ordering == 'clusterbias':
#         X = heat_data2.transpose()
#     # Create upper dendrogram
#     dendro = ff.create_dendrogram(X, orientation='bottom', labels=xaxis)
#     xaxis_ordered = dendro['layout']['xaxis']['ticktext']
#
#     # Create side dendrogram
#     dendro_side1 = ff.create_dendrogram(heat_data1, orientation='right', labels=yaxis1)
#     markers_ordered = dendro_side1['layout']['yaxis']['ticktext']
#     dendro_side2 = ff.create_dendrogram(heat_data2, orientation='right', labels=yaxis2)
#     samples_ordered = dendro_side2['layout']['yaxis']['ticktext']
#
#     # Sort heatmaps data by dendogram results
#     dendro_leaves_side1_idx = [yaxis1.index(label) for label in markers_ordered]
#     dendro_leaves_side2_idx = [yaxis2.index(sample) for sample in samples_ordered]
#     dendro_leaves_up_idx = [xaxis.index(label) for label in xaxis_ordered]
#     heat_data1 = heat_data1[dendro_leaves_side1_idx,:]
#     heat_data1 = heat_data1[:,dendro_leaves_up_idx]
#     heat_data2 = heat_data2[dendro_leaves_side2_idx,:]
#     heat_data2 = heat_data2[:,dendro_leaves_up_idx]
#
#     # Create hover data for heatmap1
#     counts = [hover1[idx] for idx in dendro_leaves_up_idx]
#     proportions = [hover2[idx] for idx in dendro_leaves_up_idx]
#     hovertext = create_hmap_hover_txt(xaxis_ordered, markers_ordered, counts, proportions, y_flag=True)
#
#     # Number of decimals in annotations based on number of clusters
#     dec = 1 if len(xaxis_ordered) >= 37 else 2
#
#     # Create 1st heatmap
#     heatmap1 = go.Heatmap(
#         z=heat_data1.round(decimals=dec),
#         y=markers_ordered,
#         x=xaxis_ordered,
#         text=hovertext,
#         hoverinfo='text+z',
#         colorscale = 'Viridis',
#         showscale=False
#     )
#
#     # switch heatmap axes to dendrogram tickvals to match the xaxis
#     heatmap1['x'] = dendro['layout']['xaxis']['tickvals']
#     heatmap1['y'] = dendro_side1['layout']['yaxis']['tickvals']
#
#     # Create hover data for heatmap2
#     counts = [hover3[idx] for idx in dendro_leaves_side2_idx]
#     proportions = [hover4[idx] for idx in dendro_leaves_side2_idx]
#     hovertext = create_hmap_hover_txt(xaxis_ordered, samples_ordered, counts, proportions)
#
#     # Create the second heatmap
#     heatmap2 = go.Heatmap(
#         z=heat_data2.round(decimals=1),
#         y=samples_ordered,
#         x=xaxis_ordered,
#         text=hovertext,
#         hoverinfo='text+z',
#         colorscale='Portland',
#         showscale=False
#     )
#
#     # switch heatmap axes to dendrogram tickvals to match the xaxis
#     heatmap2['x'] = dendro['layout']['xaxis']['tickvals']
#     heatmap2['y'] = dendro_side2['layout']['yaxis']['tickvals']
#
#     # Create an own yaxis for the dendrogram
#     for i in range(len(dendro.data)):
#         dendro.data[i].yaxis='y2'
#
#     # Create the figure object
#     figure = subplots.make_subplots(
#         rows=3, cols=1,
#         vertical_spacing=0.02,
#         print_grid=False,
#         shared_xaxes=True
#     )
#
#     # Add dendorgram traces to the figure
#     for fig in dendro.data:
#         figure.add_trace(fig, row=2, col=1)
#
#     # Add upper and lower heatmap to the figure
#     figure.add_trace(heatmap1, row=1, col=1)
#     figure.add_trace(heatmap2, row=3, col=1)
#
#     # Create annotations for the heatmaps if enough space
#     fsize = 9 if len(xaxis_ordered) < 50 else 6
#
#     if len(xaxis_ordered) <= 60:
#         annotations1 = heatmap_annotations(heatmap1, fsize)
#         annotations2 = heatmap_annotations(heatmap2, fsize, yref='y3')
#         annotations = annotations1 + annotations2
#         figure['layout']['annotations'] = annotations
#
#     # Edit the layout of the plot
#     figure['layout'].update({'height':height,
#                              'showlegend':False,
#                              'hovermode': 'closest'})
#
#     figure['layout']['xaxis'].update({'mirror': False,
#                                       'showgrid': False,
#                                       'showline': False,
#                                       'zeroline': False,
#                                       'tickangle':50,
#                                       # 'showticklabels': True,
#                                       'ticks':""})
#
#     figure['layout']['yaxis'].update({'domain':[0.26,.92],
#                                       'mirror': False,
#                                       'showgrid': False,
#                                       'showline': False,
#                                       'zeroline': False,
#                                       'tickfont':dict(size=10),
#                                       'ticks':""})
#
#     figure['layout']['yaxis2'].update({'domain':[0.92,1],
#                                        'mirror': False,
#                                        'showgrid': False,
#                                        'showline': False,
#                                        'zeroline': False,
#                                        'showticklabels': False,
#                                        'ticks':""})
#
#     figure['layout']['yaxis3'].update({'domain':[0,.25],
#                                        'mirror': False,
#                                        'showgrid': False,
#                                        'showline': False,
#                                        'zeroline': False,
#                                        'showticklabels': True,
#                                        'tickfont':dict(size=10),
#                                        'ticks':""})
#
#     figure['layout']['yaxis']['ticktext'] = markers_ordered
#     figure['layout']['yaxis']['tickvals'] = dendro_side1['layout']['yaxis']['tickvals']
#     figure['layout']['yaxis3']['ticktext'] = samples_ordered
#     figure['layout']['yaxis3']['tickvals'] = dendro_side2['layout']['yaxis']['tickvals']
#     figure['layout']['xaxis']['ticktext'] = xaxis_ordered
#     figure['layout']['xaxis']['tickvals'] = dendro['layout']['xaxis']['tickvals']
#     figure['layout']['title'] = title
#     figure['layout']['plot_bgcolor']='rgb(255,255,255)'
#     return figure
#
#
# def plot_clust_cond_boxplots(grouped, colors, clusters, height, columns=3):
#     """returns a boxplot of cluster frequencies by group"""
#     rows = int(np.ceil(len(clusters)/columns))
#     plot = subplots.make_subplots(
#         rows=rows,
#         cols=columns,
#         horizontal_spacing=0.05,
#         vertical_spacing=0.05,
#         subplot_titles=['cluster {}'.format(cluster) for cluster in clusters],
#         print_grid=False
#     )
#
#     loop_track = 0
#     num_plots = len(clusters)
#     for row in range(1, rows+1):
#         for col in range(1, columns+1):
#             loop_track += 1
#             showlegend=True
#             if loop_track > 1:
#                 showlegend=False
#             if loop_track <= num_plots:
#                 cluster = str(clusters.pop(0))
#                 for i, df in grouped:
#                     plot.append_trace(
#                         go.Box(
#                             y=df[cluster],
#                             boxpoints='all',
#                             name=i,
#                             text=df['sample'],
#                             jitter=0.5,
#                             pointpos=0,
#                             line=dict(
#                                 color=colors[i],
#                                 width=1
#                             ),
#                             legendgroup=i,
#                             showlegend=showlegend
#                         ),
#                         row, col
#                     )
#
#
#     plot.layout.update(dict(
#         height=height,
#         hovermode='closest',
#         plot_bgcolor='rgb(255,255,255)'
#     ))
#
#     return plot
#
#
# def plot_markers_boxplots(metadata, boxplots_dict, colors, height, fontsize, title, value):
#     """Returns plotly boxplots from the pickled boxplot data created by the cyto pipeline"""
#     data_box = []
#     for i, meta_df in metadata:
#         for sample in meta_df['sample']:
#             data_box.append(go.Box(y = boxplots_dict[sample][value],
#                                    name=sample,
#                                    line=dict(
#                                         color=colors[i],
#                                         width=0.5
#                                     ),
#                                     legendgroup=i
#                             ))
#
#     boxplotlayout = go.Layout(
#         height = height,
#         legend = dict(font=dict(size=fontsize), tracegroupgap=4),
#         xaxis=dict(
#             showgrid=True,
#             showline=True,
#             tickangle=50,
#             tickfont=dict(size=fontsize),
#             showticklabels=True,
#             ticks='outside'
#         ),
#         yaxis=dict(
#             showline=True,
#             showticklabels=True,
#             ticks='outside',
#         ),
#         title=title,
#         plot_bgcolor='rgb(255,255,255)'
#     )
#
#     return go.Figure(data_box, boxplotlayout)
