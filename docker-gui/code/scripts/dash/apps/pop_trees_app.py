from cyto_app import *

################################################################################
################################################################################
######################## POPULATION TREES PAGE LAYOUT ##########################
################################################################################
################################################################################
dropdown1 = dash_dropdown(
    'mst-dropdown-marker',
    markers[0],drop_opts2,
    pd_top=0, pd_bottom=0,
    pd_right=0, pd_left=0
)

dropdown2 = dash_dropdown(
    'mst-dropdown-size',
    'relative', drop_opts9,
    pd_top=0, pd_bottom=0,
    pd_right=0, pd_left=0
)

dropdown3 = dash_dropdown(
    'mst-dropdown-split',
    'sample', drop_opts4,
    pd_top=0, pd_bottom=0,
    pd_right=0, pd_left=0
)

infobox = info_card(
    text1='Select marker',
    text2='Cluster Sizing',
    text3='Split MST by',
    colwidth1=4,
    colwidth2=4,
    colwidth3=4,
    comp1=dropdown1,
    comp2=dropdown2,
    comp3=dropdown3,
)

mst_single = dash_graph('mst-full', height='83vh', loading=True, loading_num=8)
mst_multi = dash_graph('mst-rest', loading=True, loading_num=9)

col_args1 = (
    card('full-mst', 4, infobox, mst_single),
    card('rest-mst', 8, mst_multi)
)

layout = html.Div([
    row(*col_args1)
])

################################################################################
################################################################################
###################### POPULATION TREES PAGE CALLBACKS #########################
################################################################################
################################################################################
@app.callback(Output('mst-full', 'figure'),
              [Input('mst-dropdown-marker', 'value'),
               Input('mst-dropdown-size', 'value')])
def update_mst_single(valueMarker, valueSize):
    """callback for leftmost mst plot"""
    cmin, cmax = create_colorscale(mst_full, valueMarker)
    trace = single_mst_trace(
        nodes,
        edges,
        mst_full,
        cmin, cmax,
        valueMarker,
        valueSize,
        showscale=True
    )

    return plot_single_mst(
        trace,
        mst_full,
        valueMarker,
        title='Full Data'
    )


@app.callback(Output('mst-rest', 'figure'),
              [Input('mst-dropdown-marker', 'value'),
               Input('mst-dropdown-split', 'value'),
               Input('mst-dropdown-size', 'value')])
def update_mst_multiple(valueMarker, valueMeta, valueSize):
    """callback for multiple mst plot"""
    cmin, cmax = create_colorscale(mst_full, valueMarker)
    msts = mst_dict[valueMeta]
    return plot_multiple_mst(nodes, edges, msts, cmin, cmax, valueMarker, valueSize)
