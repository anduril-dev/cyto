from cyto_app import *

################################################################################
################################################################################
######################## SAMPLE SET SUMMARY PAGE LAYOUT ########################
################################################################################
################################################################################
# Define objects to render
toggle1 = daq_toggle('datatype-toggle', False, ['Processed', 'Unprocessed'])
toggle2 = daq_toggle('mean-median-toggle', False, label=['Median', 'Mean'])
dropdown1 = dash_dropdown('dropdown-metadata', 'sample', drop_opts4)
dropdown2 = dash_dropdown('dropdown-markers', markers[0], drop_opts2)
dropdown3 = dash_dropdown('dropdown-samples', samples[0], drop_opts6)
nrsplot1 = dash_graph('nrs-plot', height='45vh')
ridgeplot1 = dash_graph('ridge-plot', height='90vh', loading=True, loading_num=1)
heatmap1 = dash_graph('corr-plot', height='85vh', loading=True, loading_num=2)
heatmap2 = dash_graph('heatmap-plot', height='85vh', loading=True, loading_num=3)
boxplot1 = dash_graph('boxplot-simpson', height='49.4vh')

row_args1 = (
    card('corrplot-card', 12, dropdown3, heatmap1),
    card('heatmap-card', 12, toggle2, heatmap2)
)

row_args2 = (
    card('nrs-card', 12, dropdown1, nrsplot1, boxplot1),
    card('ridge-card', 12, toggle1, dropdown2, ridgeplot1)
)

layout = html.Div([
    card_column(*row_args1),
    card_column(*row_args2)
])

################################################################################
################################################################################
######################## SAMPLE SET SUMMARY PAGE CALLBACKS #####################
################################################################################
################################################################################
@app.callback(Output('ridge-plot', 'figure'),
              [Input('dropdown-markers', 'value'),
               Input('dropdown-metadata', 'value'),
               Input('datatype-toggle', 'value')])
def update_ridgeplot(value1, value2, toggle):
    data = trans_df if not toggle else raw_df
    grouped = data.groupby([value2])
    colors = colors_meta[value2]
    return ridgeplot_sample(grouped, colors, value2, value1)


@app.callback(Output('heatmap-plot', 'figure'),
              [Input('mean-median-toggle', 'value')])
def update_sample_hmap(value):
    """callback for sample vs marker heatmap"""
    data = heat_sample_median_z if not value else heat_sample_mean_z
    return heatmap_dendro(data=data,
                          yaxis=markers,
                          xaxis=samples,
                          hover1=sample_cell_counts,
                          hover2=sample_freq,
                          dec=2,
                          fsize=9,
                          fsize_y=9,
                          title='Samples vs. Markers')


@app.callback(Output('corr-plot', 'figure'),
              [Input('dropdown-samples', 'value')])
def update_corrplot(valueSample):
    """callback for sample vs marker heatmap"""
    data = corr_dict[valueSample]
    return corrplot(data=data,
                    fontsize=9)


@app.callback(Output('nrs-plot', 'figure'),
              [Input('dropdown-metadata', 'value')])
def update_nrs_plot(valueMeta):
    """ callback for nrs plot"""
    grouped_data = sampleNRS_scores.groupby([valueMeta])
    return nrsplot(grouped_data=grouped_data,
                   data=scores,
                   colors=colors_meta[valueMeta],
                   markers=scores.columns,
                   samples=samples,
                   title='Sample NRS Scores',
                   tickangle=50)


@app.callback(Output('boxplot-simpson', 'figure'),
              [Input('dropdown-metadata', 'value')])
def update_simpson(value):
    """callback for simpson diversity boxplots"""
    grouped = simpson_indexes.groupby([value])
    return plot_simpson(
        grouped,
        colors_meta[value],
        fontsize=10,
        title='Simpson diversity indexes per group'
    )
