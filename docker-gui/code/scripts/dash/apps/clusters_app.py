from cyto_app import *

################################################################################
################################################################################
############################# CLUSTERING PAGE LAYOUT ###########################
################################################################################
################################################################################
# Dropdown option values for storage1 callback
transform_opts = [k['value'] for k in drop_opts11]
sumstat_opts = [k['value'] for k in drop_opts12]
y_order_opts = [k['value'] for k in drop_opts13]
sample_hmap_data_opts = [k['value'] for k in drop_opts14]
sample_hmap_order_opts = [k['value'] for k in drop_opts16]

# Content
dropdown1 = dash_dropdown('box-dropdown1', 'cluster 1', drop_opts5)
dropdown2 = dash_dropdown('box-dropdown2', 'None', drop_opts10)
dropdown3 = dash_dropdown('hmap-dropdown-order', 'None', drop_opts10)
dropdown4 = dash_dropdown('hmap-dropdown-trans', 'None', drop_opts11)
dropdown5 = dash_dropdown('hmap-dropdown-sumstat', 'median', drop_opts12)
dropdown6 = dash_dropdown('hmap-dropdown-yclust', 'm-hmap', drop_opts13)
dropdown7 = dash_dropdown('hmap-dropdown-data', 'raw-counts', drop_opts14)
boxplot1 = dash_graph('boxplot', height='70vh', loading=True, loading_num=10)
heatmap1 = dash_graph('clust-sample-hmap', height='80vh', loading=True, loading_num=7)
storage1 = dash_store('heatmap-data-store', 'memory')

infobox1 = info_card(
    text1='Scale columns:',
    text2='Summary statistic:',
    text3='Sort clusters by:',
    colwidth1=4,
    colwidth2=4,
    colwidth3=4,
    comp1=dropdown4,
    comp2=dropdown5,
    comp3=dropdown6
)

infobox2 = info_card(
    text1='Sample heatmap values:',
    text2='Sort sample heatmap by:',
    colwidth1=6,
    colwidth2=6,
    comp1=dropdown7,
    comp2=dropdown3,
)

infobox3 = info_card(
    text1='Choose cluster:',
    text2='Split plots by:',
    colwidth1=6,
    colwidth2=6,
    comp1=dropdown1,
    comp2=dropdown2,
)

subrow = (
    column('hmap-dropdowns1', 7, infobox1),
    column('hmap-dropdowns2', 5, infobox2)
)

row_args = (
    card('cluster-heatmap', 12, row(*subrow), storage1, heatmap1),
)

row_args2 = (
    card('cluster-markers', 12, infobox3, boxplot1),
)

layout = html.Div([
    row(*row_args), row(*row_args2)
])


################################################################################
################################################################################
################## UNSUPERVISED CLUSTERING PAGE CALLBACKS ######################
################################################################################
################################################################################
@app.callback(Output('heatmap-data-store', 'data'),
              [Input('hmap-dropdown-order', 'value'),
               Input('hmap-dropdown-trans', 'value'),
               Input('hmap-dropdown-sumstat', 'value'),
               Input('hmap-dropdown-yclust', 'value'),
               Input('hmap-dropdown-data', 'value')],
              [State('heatmap-data-store', 'data')])
def update_clustermap_cache(valueOrder, valueTrans, valueSumstat, valueYclust, valueData, data):
    """Callback to store calculations of heatmap in cache"""
    if not dash.callback_context.triggered:
        raise PreventUpdate
    dc = dash.callback_context
    trigger = dc.triggered[0]['value']
    trigger_id = dc.triggered[0]['prop_id']

    print('trigger: ', trigger)
    print('trigger id: ', trigger_id)
    print(dc.inputs)
    print('\n\nbeginning:\n', dc.states)

    ############################################################################
    # Initialize dict to besaved to memory or if it is already populated use it
    data = data or {
        'z-range':{
            'median':{
                'z-score':{
                    'zmin':None,
                    'zmax':None
                },
                'min-max':{
                    'zmin':None,
                    'zmax':None
                },
                'raw':{
                    'zmin':None,
                    'zmax':None
                }
            },
            'mean':{
                'z-score':{
                    'zmin':None,
                    'zmax':None
                },
                'min-max':{
                    'zmin':None,
                    'zmax':None
                },
                'raw':{
                    'zmin':None,
                    'zmax':None
                }
            }
        },
        'marker-orders':{
            'state':{
                'median':{
                    'z-score':{
                        'x':None,
                        'y':None
                    },
                    'min-max':{
                        'x':None,
                        'y':None
                    },
                    'raw':{
                        'x':None,
                        'y':None
                    }
                },
                'mean':{
                    'z-score':{
                        'x':None,
                        'y':None
                    },
                    'min-max':{
                        'x':None,
                        'y':None
                    },
                    'raw':{
                        'x':None,
                        'y':None
                    }
                }
            },
            'type':{
                'median':{
                    'z-score':{
                        'x':None,
                        'y':None
                    },
                    'min-max':{
                        'x':None,
                        'y':None
                    },
                    'raw':{
                        'x':None,
                        'Y':None
                    }
                },
                'mean':{
                    'z-score':{
                        'x':None,
                        'y':None
                    },
                    'min-max':{
                        'x':None,
                        'y':None
                    },
                    'raw':{
                        'x':None,
                        'y':None
                    }
                }

            }
        },
        'sample-orders':{
            'raw-counts':{
                'x': None,
                'y': None
            },
            'col-freq':{
                'x': None,
                'y': None
            },
            'row-freq':{
                'x': None,
                'y': None
            },
            'orderings': {'{}'.format(col):None for col in meta_data.columns}
        },
        'active-data':{
            'y-order':None,
            'type-marker-order':None,
            'state-marker-order':None,
            'sample-order':None,
            'type-hmap-annotations':None,
            'state-hmap-annotations':None,
            'sample-hmap-annotations':None,
            'type-hmap-hovertext':None,
            'state-hmap-hovertext':None,
            'sample-hmap-hovertext':None,
        }
    }

    ############################################################################
    # Initialize data on first page load
    # if dc.states['heatmap-data-store.data'] == None and state_markers != ['']:
    #     df = scale(valueTrans, heat_clust_median_z)
    #     zmin, zmax = create_colorscale(df)
    #     data['zmin'] = zmin
    #     data['zmax'] = zmax
    #     data['cluster-order'] = hclust(df[type_markers])
    #     data['type-markers-order'] = hclust(df[type_markers].transpose())
    #     data['state-markers-order'] = hclust(df[state_markers].transpose())
    #
    # # If no state markers specified, initialize marker data to the storage here
    # elif dc.states['heatmap-data-store.data'] == None and state_markers == ['']:
    #     df = scale(valueTrans, heat_clust_median_z)
    #     zmin, zmax = create_colorscale(df)
    #     data['zmin'] = zmin
    #     data['zmax'] = zmax
    #     data['cluster-order'] = hclust(df[type_markers])
    #     data['type-markers-order'] = hclust(df[type_markers].transpose())
    #     data['state-markers-order'] = None
    #
    # #  Populate the storage with the sample orders
    # if dc.states['heatmap-data-store.data'] == None:
    #     data['sample-order'] = hclust(cluster_sample_counts[labels])


    # #  and update it-
    # data = data or {
    #     'marker-data':'median',
    #     'sample-data':'raw-counts',
    #     'cluster-order':None,
    #     'type-markers-order':None,
    #     'state-markers-order':None,
    #     'sample-order':None,
    #     'type-marker-annotations':None,
    #     'state-marker-annotations':None,
    #     'sample-annotations':None,
    #     'type-marker-hovertext':None,
    #     'state-marker-hovertext':None,
    #     'samplemap-hovertext':None,
    #     'zmin':None,
    #     'zmax':None
    # }
    #
    # ############################################################################
    # # Inititalize Default datas and orderings when page is loaded to memory
    #
    # # If there is nothing in storage, populate it with marker data.
    # # If there are state markers specified, do it here
    # if dc.states['heatmap-data-store.data'] == None and state_markers != ['']:
    #     df = scale(valueTrans, heat_clust_median_z)
    #     zmin, zmax = create_colorscale(df)
    #     data['zmin'] = zmin
    #     data['zmax'] = zmax
    #     data['cluster-order'] = hclust(df[type_markers])
    #     data['type-markers-order'] = hclust(df[type_markers].transpose())
    #     data['state-markers-order'] = hclust(df[state_markers].transpose())
    #
    # # If no state markers specified, initialize marker data to the storage here
    # elif dc.states['heatmap-data-store.data'] == None and state_markers == ['']:
    #     df = scale(valueTrans, heat_clust_median_z)
    #     zmin, zmax = create_colorscale(df)
    #     data['zmin'] = zmin
    #     data['zmax'] = zmax
    #     data['cluster-order'] = hclust(df[type_markers])
    #     data['type-markers-order'] = hclust(df[type_markers].transpose())
    #     data['state-markers-order'] = None
    #
    # #  Populate the storage with the sample orders
    # if dc.states['heatmap-data-store.data'] == None:
    #     data['sample-order'] = hclust(cluster_sample_counts[labels])
    #
    #
    # ###########################################################################
    # # Choose data for marker heatmap
    # if trigger in ['mean', 'median', 'z-score', 'min-max']:
    #     if trigger == 'mean':
    #         data['marker-data'] = 'mean'
    #         df_marker = scale(valueTrans, heat_clust_mean_z)
    #     else:
    #         data['marker-data'] = 'median'
    #         df_marker = scale(valueTrans, heat_clust_median_z)
    # else:
    #     if data['marker-data'] == 'median':
    #         df_marker = scale(valueTrans, heat_clust_median_z)
    #     elif data['marker-data'] == 'mean':
    #         df_marker = scale(valueTrans, heat_clust_mean_z)
    #
    # # Choose data for sample heatmap
    # if trigger in ['raw-counts', 'row-freq', 'col-freq']+list(meta_data.columns):
    #     if trigger == 'raw-counts':
    #         data['sample-data'] = 'raw-counts'
    #         df_sample = cluster_sample_counts
    #     elif trigger == 'col-freq':
    #         data['sample-data'] = 'col-freq'
    #         df_sample = cluster_freq_col
    #     elif trigger == 'row-freq':
    #         data['sample-data'] = 'row-freq'
    #         df_sample = cluster_freq_col
    # else:
    #     if data['sample-data'] == 'raw-counts':
    #         df_sample = cluster_sample_counts
    #     elif data['sample-data'] == 'col-freq':
    #         df_sample = cluster_freq_col
    #     elif data['sample-data'] == 'row-freq':
    #         df_sample = cluster_freq_row
    #
    # ############################################################################
    #
    # zmin, zmax = create_colorscale(df)
    # data['zmin'] = zmin
    # data['zmax'] = zmax
    # data['cluster-order'] = hclust(df[type_markers])
    # data['type-markers-order'] = hclust(df[type_markers].transpose())
    # data['state-markers-order'] = hclust(df[state_markers].transpose())

    ###########################################################################
    # Start going through triggerings in dropdowns and calculate stuff to memory
    print('\n\nend:\n', data)
    return data


@app.callback(Output('clust-sample-hmap', 'figure'),
              [Input('hmap-dropdown-order', 'value'),
               Input('hmap-dropdown-trans', 'value'),
               Input('hmap-dropdown-sumstat', 'value'),
               Input('hmap-dropdown-yclust', 'value'),
               Input('hmap-dropdown-data', 'value')])
def update_cluster_hmap(valueOrder, valueTrans, valueSumstat, valueYclust, valueData):
    """callback for cluster marker heatmap"""

    if valueSumstat == 'median':
        data1 = heat_clust_median_z
    elif valueSumstat == 'mean':
        data1 = heat_clust_mean_z

    if valueTrans == 'z-score':
        normalize='z-score'
    elif valueTrans == 'min-max':
        normalize='min-max'
    else:
        normalize='None'

    if valueYclust == 'm-hmap':
        ordering_y = 'clustersimilarity'
    else:
        ordering_y = 'clusterbias'

    if valueData == 'raw-counts':
        data2 = cluster_sample_counts[labels]
    elif valueData == 'col-freq':
        data2 = cluster_freq_col[labels]
    else:
        data2 = cluster_freq_row[labels]

    return cluster_sample_map(cluster_summaries=data1,
                              sample_cluster_counts=data2,
                              markers=markers,
                              type_markers=type_markers,
                              state_markers=state_markers,
                              meta_data=meta_data,
                              colors_meta=colors_meta,
                              clusters=clusters,
                              cluster_cell_counts=cluster_cell_counts,
                              cluster_freqs=cluster_freq,
                              sample_counts=sample_counts,
                              sample_freqs=sample_freq,
                              ordering_y=ordering_y,
                              ordering_samples=valueOrder,
                              normalize=normalize,
                              fsize_x=13,
                              fsize_y=13)


@app.callback(Output('boxplot', 'figure'),
              [Input('box-dropdown1', 'value'),
               Input('box-dropdown2', 'value')])
def update_boxplot(valueClust, valueMeta):
    """callback for marker boxplots"""

    if valueMeta == 'None':
        frames = boxplots_dict[valueClust]['full'].copy()
        counts = frames.pop('count')
        counts = counts.sort_values(['count'], ascending=False)
        frame = frames['data']

        if state_markers == ['']:
            t_ordered = frame[frame.columns[frame.columns.isin(type_markers)]].columns
            markers_ordered = list(t_ordered)
            shapes = [go.layout.Shape(
                type="rect",
                xref="x",
                yref="paper",
                x0=markers_ordered[0],
                y0=0,
                x1=markers_ordered[-1],
                y1=0.02,
                fillcolor='#3a34eb',
                opacity=0.5,
                layer="below",
                line_width=0,
            )]

        else:
            t_ordered = frame[frame.columns[frame.columns.isin(type_markers)]].columns
            s_ordered = frame[frame.columns[frame.columns.isin(state_markers)]].columns
            shapes = [
                go.layout.Shape(
                    type="rect",
                    xref="x",
                    yref="paper",
                    x0=s_ordered[0],
                    y0=0,
                    x1=s_ordered[-1],
                    y1=0.02,
                    fillcolor='#ebe71e',
                    opacity=0.5,
                    layer="below",
                    line_width=0,
                ),
                go.layout.Shape(
                    type="rect",
                    xref="x",
                    yref="paper",
                    x0=t_ordered[0],
                    y0=0,
                    x1=t_ordered[-1],
                    y1=0.02,
                    fillcolor='#3a34eb',
                    opacity=0.5,
                    layer="below",
                    line_width=0,
                )
            ]
            markers_ordered = list(s_ordered) + list(t_ordered)

        melt = pd.melt(
            frame,
            value_vars=markers_ordered,
            var_name='marker',
            value_name='expression'
        )
        colors = colors_meta['sample']
    else:
        frames = boxplots_dict[valueClust][valueMeta].copy()
        counts = frames.pop('count')
        counts['sum'] = counts.groupby([valueMeta])['count'].transform(sum)
        counts = counts.sort_values(['sum','count'], ascending=[False, False])
        concat = pd.concat(frames.values(), ignore_index=False)

        if state_markers == ['']:
            t_ordered = concat[concat.columns[concat.columns.isin(type_markers)]].columns
            markers_ordered = list(t_ordered)
            shapes = [go.layout.Shape(
                type="rect",
                xref="x",
                yref="paper",
                x0=markers_ordered[0],
                y0=0,
                x1=markers_ordered[-1],
                y1=0.02,
                fillcolor='#3a34eb',
                opacity=0.5,
                layer="below",
                line_width=0,
            )]
        else:
            t_ordered = concat[concat.columns[concat.columns.isin(type_markers)]].columns
            s_ordered = concat[concat.columns[concat.columns.isin(state_markers)]].columns
            shapes = [
                go.layout.Shape(
                    type="rect",
                    xref="x",
                    yref="paper",
                    x0=s_ordered[0],
                    y0=0,
                    x1=s_ordered[-1],
                    y1=0.02,
                    fillcolor='#ebe71e',
                    opacity=0.5,
                    layer="below",
                    line_width=0,
                ),
                go.layout.Shape(
                    type="rect",
                    xref="x",
                    yref="paper",
                    x0=t_ordered[0],
                    y0=0,
                    x1=t_ordered[-1],
                    y1=0.02,
                    fillcolor='#3a34eb',
                    opacity=0.5,
                    layer="below",
                    line_width=0,
                )
            ]
            markers_ordered = list(s_ordered) + list(t_ordered)

        melt = pd.melt(
            concat,
            value_vars=markers_ordered,
            id_vars=['group'],
            var_name='marker',
            value_name='expression'
        )
        colors = colors_meta[valueMeta]

    return cluster_boxplots(frames=melt,
                            counts=counts,
                            valueMeta=valueMeta,
                            colors=colors,
                            shapes=shapes)


##################################################
##################################################
##################################################
# Old accordion code
# cluster_hmap_text1 = """
# **Select** the antibody heatmaps to show either median, mean or standard deviation of the expression levels.
# The expression levels can also be z-scored or min-max-scaled.
# """
#
# cluster_hmap_text2 = """
# **Select** the clustering of the Y-axis. The default option is to cluster
# hierachically based on the type marker similarity. The other option is to cluster based on the data
# in the sample heatmap on the right. This is useful if you want to show the inter-heterogenity of the samples.
# """
#
# cluster_hmap_text3 = """
# **Select** what data to use here. Either raw counts of cells
# or the proportion of cells assigned to each cluster by sample (row frequencies)
# or the proportion of each cluster in samples (column frequencies).
# """
#
# cluster_hmap_text4 = """
# **Select** The ordering of the sample heatmap by sorting the heatmap x-axis
# based on a sample annotation. The default ('None') order is a hierarchical clustering
# of the sample heatmap x-axis that depends on the data it displays.
# """
#
# # Button group options
# group1 = ['median', 'mean', 'std']
# group2 = ['None', 'min-max', 'z-score']
# group3 = ['marker similarity', 'sample similarity']
# group4 = ['raw counts', 'row frequencies', 'column frequencies']
#
# markdown1 = dash_markdown(cluster_hmap_text1)
# markdown2 = dash_markdown(cluster_hmap_text2)
# markdown3 = dash_markdown(cluster_hmap_text3)
# markdown4 = dash_markdown(cluster_hmap_text4)
# btngroup1 = dbc_buttongroup(group1, size="md", vertical=True)
# btngroup2 = dbc_buttongroup(group2, size="md", vertical=True)
# btngroup3 = dbc_buttongroup(group3, size="md", vertical=True)
# btngroup4 = dbc_buttongroup(group4, size="md", vertical=True)
# storage1 = dash_store('click-data-store1', 'memory')
# storage2 = dash_store('click-data-store2', 'memory')
# storage3 = dash_store('click-data-store3', 'memory')
# storage4 = dash_store('click-data-store4', 'memory')
#
# subrow1 = (
#     markdown1,
#     column('btn-group-1', 6, btngroup1),
#     column('btn-group-2', 6, btngroup2)
# )
#
# subrow2 = (
#     markdown2,
#     column('btn-group-3', 12, btngroup3)
# )
#
# subrow3 = (
#     markdown3,
#     column('btn-group-4', 12, btngroup4)
# )
#
# accordion_items = (
#     accordion_item(storage1, storage2, row(*subrow1), i=1, name='Transformations'),
#     accordion_item(storage3, row(*subrow2), i=2, name='Y-axis ordering'),
#     accordion_item(storage4, row(*subrow3), i=3, name='Sample heatmap data'),
#     accordion_item(markdown4, dropdown3, i=4, name='Sample heatmap ordering')
# )
# accordion = create_accordion(*accordion_items)
#
# @app.callback(
#     [Output("collapse-{}".format(i), "is_open") for i in range(1, 5)],
#     [Input("group-{}-toggle".format(i), "n_clicks") for i in range(1, 5)],
#     [State("collapse-{}".format(i), "is_open") for i in range(1, 5)],
# )
# def toggle_accordion(n1, n2, n3, n4, is_open1, is_open2, is_open3, is_open4):
#     ctx = dash.callback_context
#
#     if not ctx.triggered:
#         return ""
#     else:
#         button_id = ctx.triggered[0]["prop_id"].split(".")[0]
#
#     if button_id == "group-1-toggle" and n1:
#         return not is_open1, False, False, False
#     elif button_id == "group-2-toggle" and n2:
#         return False, not is_open2, False, False
#     elif button_id == "group-3-toggle" and n3:
#         return False, False, not is_open3, False
#     elif button_id == "group-4-toggle" and n4:
#         return False, False, False, not is_open4
#     return False, False, False, False
#
#
#
# @app.callback(
#     [Output("button-{}".format(btn), "className") for btn in group1],
#     [Input("button-{}".format(btn), "n_clicks") for btn in group1])
# def set_active1(*args):
#     """Callback to set button as active after pressing it for one button group"""
#     ctx = dash.callback_context
#
#     if not ctx.triggered or not any(args):
#         states = ["btn" for _ in group1]
#         states[0] = "btn active"
#         return states
#     button_id = ctx.triggered[0]["prop_id"].split(".")[0]
#     return ["btn active" if button_id == "button-{}".format(btn) else "btn" for btn in group1]
#
#
# @app.callback(
#     [Output("button-{}".format(btn), "className") for btn in group2],
#     [Input("button-{}".format(btn), "n_clicks") for btn in group2])
# def set_active2(*args):
#     """Callback to set button as active after pressing it for another button group"""
#     ctx = dash.callback_context
#
#     if not ctx.triggered or not any(args):
#         states = ["btn" for _ in group2]
#         states[0] = "btn active"
#         return states
#
#     button_id = ctx.triggered[0]["prop_id"].split(".")[0]
#     return ["btn active" if button_id == "button-{}".format(btn) else "btn" for btn in group2]
#
#
# @app.callback(
#     [Output("button-{}".format(btn), "className") for btn in group3],
#     [Input("button-{}".format(btn), "n_clicks") for btn in group3])
# def set_active3(*args):
#     """Callback to set button as active after pressing it for another button group"""
#     ctx = dash.callback_context
#
#     if not ctx.triggered or not any(args):
#         states = ["btn" for _ in group3]
#         states[0] = "btn active"
#         return states
#
#     button_id = ctx.triggered[0]["prop_id"].split(".")[0]
#     return ["btn active" if button_id == "button-{}".format(btn) else "btn" for btn in group3]
#
#
# @app.callback(
#     [Output("button-{}".format(btn), "className") for btn in group4],
#     [Input("button-{}".format(btn), "n_clicks") for btn in group4])
# def set_active4(*args):
#     """Callback to set button as active after pressing it for another button group"""
#     ctx = dash.callback_context
#
#     if not ctx.triggered or not any(args):
#         states = ["btn" for _ in group4]
#         states[0] = "btn active"
#         return states
#
#     button_id = ctx.triggered[0]["prop_id"].split(".")[0]
#     return ["btn active" if button_id == "button-{}".format(btn) else "btn" for btn in group4]
#
#
# @app.callback(
#     Output('click-data-store1', 'data'),
#     [Input("button-{}".format(btn), "n_clicks") for btn in group1],
#     [State("button-{}".format(btn), "n_clicks_timestamp") for btn in group1])
# def on_button_click1(meanValue, medianValue, stdValue,
#                     meanTs, medianTs, stdTs):
#     """button click storage callback for central value buttongroup"""
#     if meanValue == 1 and medianValue is None and stdValue is None:
#         return {'active': 'button-median'}
#
#     ctx = dash.callback_context
#     ts = np.array(list(ctx.states.values()), dtype=np.float64)
#     latest_ts = np.nanmax(ts)
#     button_id = ""
#     for key in ctx.states.keys():
#         if ctx.states[key] == latest_ts:
#             button_id = key.split(".")[0]
#     return {'active': button_id}
#
#
# @app.callback(
#     Output('click-data-store2', 'data'),
#     [Input("button-{}".format(btn), "n_clicks") for btn in group2],
#     [State("button-{}".format(btn), "n_clicks_timestamp") for btn in group2])
# def on_button_click2(noneValue, minmaxValue, zscoreValue,
#                     meanTs, medianTs, stdTs):
#     """button click storage callback for normalization buttongroup"""
#     if noneValue == 1 and minmaxValue is None and zscoreValue is None:
#         return {'active': 'button-None'}
#
#     ctx = dash.callback_context
#     ts = np.array(list(ctx.states.values()), dtype=np.float64)
#     latest_ts = np.nanmax(ts)
#     button_id = ""
#     for key in ctx.states.keys():
#         if ctx.states[key] == latest_ts:
#             button_id = key.split(".")[0]
#     return {'active': button_id}
#
#
# @app.callback(
#     Output('click-data-store3', 'data'),
#     [Input("button-{}".format(btn), "n_clicks") for btn in group3],
#     [State("button-{}".format(btn), "n_clicks_timestamp") for btn in group3])
# def on_button_click3(clusterSimValue, sampleSimValue, clusterSimTs, sampleSimTs):
#     """button click storage callback for Y-axis ordering buttongroup"""
#     if clusterSimValue == 1 and sampleSimValue is None:
#         return {'active': 'button-marker similarity'}
#
#     ctx = dash.callback_context
#     ts = np.array(list(ctx.states.values()), dtype=np.float64)
#     latest_ts = np.nanmax(ts)
#     button_id = ""
#     for key in ctx.states.keys():
#         if ctx.states[key] == latest_ts:
#             button_id = key.split(".")[0]
#     return {'active': button_id}
#
#
# @app.callback(
#     Output('click-data-store4', 'data'),
#     [Input("button-{}".format(btn), "n_clicks") for btn in group4],
#     [State("button-{}".format(btn), "n_clicks_timestamp") for btn in group4])
# def on_button_click4(rawValue, sampFreqValue, clustFreqValue, rawTs, sampFreqTs, clustFreqTs):
#     """button click storage callback for sample heatmap data buttongroup"""
#     if rawValue == 1 and sampFreqValue is None and clustFreqValue is None:
#         return {'active': 'button-raw counts'}
#
#     ctx = dash.callback_context
#     ts = np.array(list(ctx.states.values()), dtype=np.float64)
#     latest_ts = np.nanmax(ts)
#     button_id = ""
#     for key in ctx.states.keys():
#         if ctx.states[key] == latest_ts:
#             button_id = key.split(".")[0]
#     return {'active': button_id}
