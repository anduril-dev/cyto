from cyto_app import *

################################################################################
################################################################################
#################### DIMENSIONALITY REDUCTION PAGE LAYOUT ######################
################################################################################
################################################################################
dropdown1 = dash_dropdown('scatter-color-dropdown', 'label', drop_opts3, pd_top=0, pd_bottom=0)
dropdown2 = dash_dropdown('scatter-method-dropdown', embed_methods[0], drop_opts8, pd_top=0, pd_bottom=0)
dropdown3 = dash_dropdown('metadata-dropdown-bar', 'sample', drop_opts4, pd_top=0, pd_bottom=0)
dropdown4 = dash_dropdown('metadata-dropdown-box', 'None', drop_opts10, pd_top=0, pd_bottom=0)
if state_markers != ['']:
    dropdown5 = dash_dropdown('marker-dropdown-box', 'Type', drop_opts15, pd_top=0, pd_bottom=0)
else:
    dropdown5 = dash_dropdown('marker-dropdown-box', 'Type', drop_opts15, pd_top=0, pd_bottom=0)
storage1 = dash_store('legend-data-store', 'memory')
scatter1 = dash_graph('scatter', height='82.9vh', scrollZoom=True, loading=True, loading_num=4)
boxplot1 = dash_graph('selection-boxplot', height='42.9vh', loading=True, loading_num=5)
barplot1 = dash_graph('selection-barplot', height='40vh', loading=True, loading_num=6)

infobox1 = info_card(
    text1='Select the coloring:',
    text2='Select the embedding method:',
    colwidth1=6,
    colwidth2=6,
    comp1=dropdown1,
    comp2=dropdown2
)

infobox2 = info_card(
    text1='Split the bar plot by',
    text2='Split the box plot by',
    text3='Markers',
    colwidth1=4,
    colwidth2=4,
    colwidth3=4,
    comp1=dropdown3,
    comp2=dropdown4,
    comp3=dropdown5
)

row_args = (
    card('scatter-card', 6, infobox1, scatter1),
    card('boxplot-card', 6, storage1, infobox2, boxplot1, barplot1)
)

layout = html.Div([
    row(*row_args)
])

################################################################################
################################################################################
############### DIMENSIONALITY REDUCTION PAGE CALLBACKS ########################
################################################################################
################################################################################
@app.callback(Output('scatter', 'figure'),
              [Input('scatter-color-dropdown', 'value'),
               Input('scatter-method-dropdown', 'value')])
def update_scatter(valueColor, valueMethod):
    """callback for dropdown to render a tsne scatterplot"""
    type=''
    if valueColor in markers:
        type = valueMethod
        grouped = twoD_embedding.groupby(['label'], sort=False)
        cmin, cmax = create_colorscale(twoD_embedding, valueColor)
        showlegend = False
        colorscale = 'Jet'
    else:
        type = valueMethod
        grouped = twoD_embedding.groupby([valueColor])
        cmin, cmax = None, None
        showlegend = True
        colorscale = None

    return scatter(grouped_data=grouped,
                   colors_meta=colors_meta,
                   type=type,
                   cmin=cmin,
                   cmax=cmax,
                   showlegend=showlegend,
                   colorscale=colorscale,
                   value_color=valueColor)


@app.callback(Output('selection-boxplot', 'figure'),
              [Input('legend-data-store', 'modified_timestamp'),
               Input('metadata-dropdown-box', 'value'),
               Input('marker-dropdown-box', 'value')],
              [State('legend-data-store', 'data')])
def boxplot_selected_data(ts, valueMeta, valueMarkerType, data):
    """callback for selected point boxplot"""
    plot_data = None
    if data is None:
        plot_data = twoD_embedding
        raise PreventUpdate
    else:
        if data['scatter-value'] in markers:
            plot_data = twoD_embedding
        else:
            plot_data = twoD_embedding.loc[twoD_embedding[data['scatter-value']].isin(data['visible'])]
    exprs = plot_data

    if data['selected-data'] is not None:
        if len(data['selected-data']['points']) > 0:
            df = pd.DataFrame(columns = ['x', 'y'])
            for point in data['selected-data']['points']:
                df.loc[len(df)] = point['x']
                df.loc[len(df)] = point['y']

        # get the intersecting points
        if data['toggle-value'] == 'tSNE':
            exprs=pd.merge(df, plot_data, how='inner', left_on=['x'], right_on=['tsne1'])
        elif data['toggle-value'] == 'UMAP':
            exprs=pd.merge(df, plot_data, how='inner', left_on=['x'], right_on=['UMAP1'])

    if valueMarkerType == 'Type':
        markers_to_use = type_markers
    elif valueMarkerType == 'State':
        markers_to_use = state_markers
    else:
        markers_to_use = markers

    # sort markers
    exprs_markers = exprs[exprs.columns[exprs.columns.isin(markers_to_use)]]
    exprs_markers = exprs_markers.reindex(exprs_markers.mean().sort_values().index, axis=1)
    markers_ordered = exprs_markers.columns

    if valueMarkerType == 'All':
        # group both marker types
        t_ordered = exprs_markers[exprs_markers.columns[exprs_markers.columns.isin(type_markers)]].columns
        s_ordered = exprs_markers[exprs_markers.columns[exprs_markers.columns.isin(state_markers)]].columns
        markers_ordered = list(s_ordered) + list(t_ordered)
        shapes = [
            go.layout.Shape(
                type="rect",
                xref="x",
                yref="paper",
                x0=s_ordered[0],
                y0=0,
                x1=s_ordered[-1],
                y1=0.02,
                fillcolor='#ebe71e',
                opacity=0.5,
                layer="below",
                line_width=0,
            ),
            go.layout.Shape(
                type="rect",
                xref="x",
                yref="paper",
                x0=t_ordered[0],
                y0=0,
                x1=t_ordered[-1],
                y1=0.02,
                fillcolor='#3a34eb',
                opacity=0.5,
                layer="below",
                line_width=0,
            )
        ]
    elif valueMarkerType == 'Type':
        shapes = [go.layout.Shape(
            type="rect",
            xref="x",
            yref="paper",
            x0=markers_ordered[0],
            y0=0,
            x1=markers_ordered[-1],
            y1=0.02,
            fillcolor='#3a34eb',
            opacity=0.5,
            layer="below",
            line_width=0,
        )]
    else:
        shapes = [go.layout.Shape(
            type="rect",
            xref="x",
            yref="paper",
            x0=markers_ordered[0],
            y0=0,
            x1=markers_ordered[-1],
            y1=0.02,
            fillcolor='#ebe71e',
            opacity=0.5,
            layer="below",
            line_width=0,
        )]

    # Melt dataframe
    meta_cols = list(meta_data.columns)
    exprs = pd.melt(
        exprs,
        id_vars=meta_cols,
        value_vars=markers_ordered,
        var_name='marker',
        value_name='expression'
    )

    # Render based on dropdown value
    if valueMeta == 'None':
        fig = px_boxplot(
            exprs,
            x='marker',
            y='expression',
            groupn=valueMeta,
            colors=colors_markers,
            shapes=shapes
        )
    else:
        fig = px_boxplot(
            exprs,
            x='marker',
            y='expression',
            groupn=valueMeta,
            colors=colors_meta[valueMeta],
            shapes=shapes
        )
    return fig


@app.callback(Output('selection-barplot', 'figure'),
              [Input('legend-data-store', 'modified_timestamp'),
               Input('metadata-dropdown-bar', 'value')],
              [State('legend-data-store', 'data')])
def barplot_selected_data(ts, valueMeta, data):
    """callback for selected point boxplot"""
    plot_data = None
    if data is None:
        plot_data = twoD_embedding
        raise PreventUpdate
    else:
        if data['scatter-value'] in markers:
            plot_data = twoD_embedding
        else:
            plot_data = twoD_embedding.loc[twoD_embedding[data['scatter-value']].isin(data['visible'])]
    exprs = plot_data

    # Find out the intersecting points from the original scatter df
    if data['selected-data'] is not None:
        if len(data['selected-data']['points']) > 0:
            df = pd.DataFrame(columns = ['x', 'y'])
            for point in data['selected-data']['points']:
                df.loc[len(df)] = point['x']
                df.loc[len(df)] = point['y']

        # Get the embedding type
        if data['toggle-value'] == 'tSNE':
            exprs=pd.merge(df, plot_data, how='inner', left_on=['x'], right_on=['tsne1'])
        elif data['toggle-value'] == 'UMAP':
            exprs=pd.merge(df, plot_data, how='inner', left_on=['x'], right_on=['UMAP1'])

    # Sort intersecting points
    exprs = pd.DataFrame(exprs['sample'].value_counts())
    exprs.reset_index(level=0, inplace=True)
    exprs.columns = ['sample', 'count']

    # Join metadata if it exists
    if len(meta_data.columns) > 1:
        exprs = pd.merge(exprs, meta_data, how='left', on='sample')

    return countplot(
        exprs,
        valueMeta,
        colors=colors_meta[valueMeta],
        titlepos=0.65
    )


@app.callback(Output('legend-data-store', 'data'),
              [Input('scatter', 'restyleData'),
               Input('scatter', 'selectedData'),
               Input('scatter', 'clickData'),
               Input('scatter-color-dropdown', 'value'),
               Input('scatter-method-dropdown', 'value')],
              [State('legend-data-store', 'data')])
def on_tab3event_clicks(restyleData, selectedData, valueClick, valueScatter, valueMethod, data):
    """
    Callback to save intermediate event data to a dash storage.
    """
    # Get the groups of the data
    if valueScatter not in markers:
        grouped_data = twoD_embedding.groupby([valueScatter])
    else:
        grouped_data = twoD_embedding.groupby(['label'])

    group_names = list(grouped_data.groups.keys())

    # Data to be stored between callbacks
    data = data or {'visible':group_names,
                    'non-visible':[],
                    'scatter-value':valueScatter,
                    'scatter-prev':valueScatter,
                    'toggle-value':valueMethod,
                    'toggle-prev':valueMethod,
                    'selected-data':selectedData,
                    'selected-prev':None,
                    'restyle-data':restyleData,
                    'restyle-prev':None,
                    'counter':[0, selectedData],
                    'counter-prev':[0, selectedData],
                    'restyle-count':[0, restyleData],
                    'restyle-count-prev':[0, restyleData]}

    # Move the new event values to the current slots of the dict
    data['counter'][0] += 1
    data['counter'][1] = selectedData
    data['restyle-count'][0] += 1
    data['restyle-count'][1] = restyleData
    data['selected-data'] = selectedData
    data['restyle-data'] = restyleData
    data['toggle-value'] = valueMethod
    data['scatter-value'] = valueScatter

    # First make sure if selectedData is not none, that there are actually
    # selected points in the selected with the selection tool,
    if selectedData is not None:
        if len(selectedData['points']) == 0:
            data['selected-data'] = None

    # Represent each event as a Boolean
    selectEvent1 = True if data['selected-prev'] != data['selected-data'] else False
    restyleEvent1 = True if data['restyle-prev'] != data['restyle-data'] else False
    toggleEvent = True if data['toggle-prev'] != data['toggle-value'] else False
    dropdownEvent = True if data['scatter-prev'] != data['scatter-value'] else False

    # If current data is None and previous-data is not none, then there was no select/restyle events
    selectEvent2 = False if data['selected-prev'] is not None and data['selected-data'] is None else True
    restyleEvent2 = False if data['restyle-prev'] is not None and data['restyle-data'] is None else True

    selectEvent3 = True if data['counter'][1] != data['counter-prev'][1] else False
    restyleEvent3 = True if data['restyle-count'][1] != data['restyle-count-prev'][1] else False

    if selectEvent1 and selectEvent2 and selectEvent3:
        pass
    elif restyleEvent1 and restyleEvent2 and restyleEvent3:
        data['selected-data'] = None
        visible = data['restyle-data'][0]['visible']
        trace_idxs = data['restyle-data'][1]
        vis = list(zip(visible, trace_idxs))
        visible_idxs = [v[1] for v in vis if v[0] != 'legendonly']
        non_visible_idxs = [v[1] for v in vis if v[0] == 'legendonly']

        # Define single click event and double click events on legend clicks (not possible with dash API?)
        l1 = len(non_visible_idxs)
        l2 = len(visible_idxs)
        singleClickEvent = True if (l1 == 1 and l2 == 0) or (l1 == 0 and l2 == 1) else False
        doubleClickEvent = True if (l1 > 0 and l2 == 1) or (l1 == 0 and l2 > 1) else False

        if singleClickEvent:
            if non_visible_idxs:
                for i in non_visible_idxs:
                    val = group_names[i]
                    data['non-visible'].append(data['visible'].pop(data['visible'].index(val)))

            if visible_idxs:
                for i in visible_idxs:
                    val = group_names[i]
                    data['visible'].append(data['non-visible'].pop(data['non-visible'].index(val)))

        elif doubleClickEvent:
            data['visible'] = []
            data['non-visible'] = []

            if non_visible_idxs:
                for i in non_visible_idxs:
                    val = group_names[i]
                    data['non-visible'].append(val)

            if visible_idxs:
                for i in visible_idxs:
                    val = group_names[i]
                    data['visible'].append(val)

    if dropdownEvent:
        data['visible'] = group_names
        data['non-visible'] = []
        data['selected-data'] = None
        data['selected-prev'] = None
        data['restyle-data'] = None
        data['restyle-prev'] = None
    elif toggleEvent:
        data['visible'] = group_names
        data['non-visible'] = []
        data['selected-data'] = None
        data['selected-prev'] = None
        data['restyle-data'] = None
        data['restyle-prev'] = None
    else:
        pass

    data['selected-prev'] = data['selected-data']
    data['restyle-prev'] = data['restyle-data']
    data['toggle-prev'] = data['toggle-value']
    data['scatter-prev'] = data['scatter-value']
    data['counter-prev'] = data['counter']
    data['restyle-count-prev'] = data['restyle-count']
    return data
