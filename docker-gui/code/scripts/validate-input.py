#!/usr/bin/env python3

from rpy2.robjects.packages import importr
import argparse
import csv
import glob
import json
import os
import string
import rpy2.robjects
import rpy2.robjects.packages as rpackages
import sys


VALID_CHARS = "_.%s%s" % (string.ascii_letters, string.digits)


def get_options():
    parser = argparse.ArgumentParser(
        description = 'Validate input data'
    )
    parser.add_argument(
        'data_folder',
        help = "Input data for validation",
    )
    parser.add_argument(
        'input_folder',
        help = "Input for the pipeline after validation",
    )
    parser.add_argument(
        'channels_file_path',
        help = "File to store channel names",
    )
    return parser.parse_args()


def validate_input_data(data_folder, input_folder):
    """ Prepare files from `data_folder` into pipeline friendly inputs to `input_folder`
        Returns -1 if not all files have the same exact channels or
        the total number of cells in the project if the data was successfully transformed.
        It also writes a plain text file `channels_filename` with the channel names for the GUI
    """

    filenames = [os.path.basename(f) for f in glob.glob(os.path.join(data_folder, "*"), recursive=True)]
    extensions = [os.path.splitext(f)[1] for f in filenames]
    if len(set(extensions)) > 1:
        print("All files must be either CSV of FCS")
        return {'ncells':0, 'channels':[], 'message': "error: All files must be either CSV or FCS"}
    file_type = extensions[0]

    # Call the corresponding validation function
    if file_type == ".csv":
        channels, ncells, message = validate_CSV_input(filenames, data_folder, input_folder)
    elif file_type == ".fcs":
        channels, ncells, message = validate_FCS_input(filenames, data_folder, input_folder)
    else:
        print("All files must be either CSV or FCS")
        return {'ncells':0, 'channels':[], 'message': "error: All files must be either CSV or FCS"}

    return {
      'ncells': ncells,
      'channels':channels,
      'message': message
    }

def process_single_CSV(fnames, data_folder, input_folder, sample_column='Sample'):
    with open(os.path.join(data_folder,fnames[0]), 'r', newline='') as infile:
        dialect = csv.Sniffer().sniff(infile.readline())
        infile.seek(0)
        reader = csv.DictReader(infile, dialect=dialect)
        output = {}
        ncells = 0
        channels = reader.fieldnames.copy()
        print(channels)
        print(sample_column)
        if sample_column not in channels:
            print('Single file input must include a Sample column')
            return [],0,"Error: Single file input must include Sample column"
        channels.remove(sample_column)
        for row in reader:
            sample = row[sample_column]
            ncells += 1
            if sample not in output:  
                if sample.endswith('.csv'):
                    fname_new = sample
                else:
                    fname_new = '{}.csv'.format(sample)
                
                fname_id = 'id'+fname_new if fname_new.split('.')[0].isdigit() else fname_new
                outfile = open(os.path.join(input_folder, fname_id), 'w')
                writer = csv.DictWriter(outfile, fieldnames=channels, dialect=csv.excel_tab)
                writer.writeheader()
                output[sample] = outfile, writer
            row.pop(sample_column)
            output[sample][1].writerow(row)
        for outfile, _ in output.values():
            outfile.close()
    return channels, ncells, "done"

def validate_CSV_input(fnames, data_folder, input_folder):
    """ Check/Convert input csv files to be Anduril-friendly """
    channels_new = []
    channels_raw = []
    ncells = 0
    if len(fnames) == 1:
        return(process_single_CSV(fnames, data_folder, input_folder))
    for fname in fnames:
        dialect = ''
        with open(os.path.join(data_folder,fname), 'r', newline='') as infile:
            dialect = csv.Sniffer().sniff(infile.readline())
            infile.seek(0)
            reader = csv.reader(infile, dialect=dialect)
            # get channel names
            channels_raw = next(reader)

            # remove all special chrs from antibody names
            channels_raw = ["".join(list(filter(str.isalnum, channel))) for channel in channels_raw]

            # populate channels_new to compare the channel names between files
            if (not channels_new) or (channels_new == channels_raw):
                channels_new = channels_raw
            else:
                print('Input files contain different channel names')
                return [],0,"error: Input files contain different channel names"

            # change the headers of the csv and replace special chars with '_'

            fname_new = ''.join([e if e in VALID_CHARS else "_" for e in fname])

            with open(os.path.join(input_folder, fname_new),'w') as outfile:
                writer=csv.writer(outfile, dialect=csv.excel_tab)
                writer.writerow(channels_new)
                for row in reader:
                    writer.writerow(row)
                    ncells += 1

    return channels_new, ncells, "done"

def validate_FCS_input(fnames, data_folder, input_folder):
    """ Check/Convert input fcs files to be Anduril-friendly """
    channels_new = []
    ncells = 0
    for fname in fnames:
        exprs, parameters = openFCSFile(os.path.join(data_folder,fname))
        ncells += len(exprs)
        antibodies = parameters.slots['data'][1]
        isotopes = parameters.slots['data'][0]

        # Check that the antibody names are defined in the fcs files and remove special chrs
        if type(antibodies) == rpy2.robjects.vectors.StrVector:
            antibodies = list(antibodies)
            channels_raw = ["".join(list(filter(str.isalnum, antibody))) for antibody in antibodies]
            exprs.colnames = rpy2.robjects.StrVector(channels_raw)
        else:
            isotopes = list(isotopes)
            channels_raw = ["".join(list(filter(str.isalnum, isotope))) for isotope in isotopes]
            exprs.colnames = rpy2.robjects.StrVector(channels_raw)

        # populate channels_new to compare the channel names between files
        if (not channels_new) or (channels_new == channels_raw):
            channels_new = channels_raw
        else:
            print('Input files contain different channel names')
            return [], 0, "error: Input files contain different channel names"

        # write to csv for Anduril
        fname = fname.replace(".fcs", ".csv")
        fname = ''.join([e if e in VALID_CHARS else "_" for e in fname])

        utils_package = importr("utils")
        file_path = os.path.join(input_folder, fname)
        utils_package.write_table(exprs, file=file_path, sep="\t", row_names=False)

    return channels_new, ncells, "done"

def openFCSFile(fname):
    """ Open a fcs file """
    # flowCore seems to be the more robust to FCS versions
    flowCore = importr("flowCore")
    file = flowCore.read_FCS(fname)
    exprs = file.slots['exprs']
    parameters = file.slots['parameters']
    return exprs, parameters

def update_values(new_values, file_path):
  try:
    with open(file_path, 'rt') as fp:
      old_values = json.load(fp)
  except Exception as e:
    old_values = {}

  old_values.update(new_values)
  with open(file_path, 'wt') as fp:
    json.dump(
      old_values,
      fp,
      sort_keys = True,
      indent = 4
    )



if __name__ == "__main__":
  opts = get_options()
  try:
    update_values({"message": "being read"}, opts.channels_file_path)
    update_values(
      validate_input_data(
        opts.data_folder,
        opts.input_folder,
      ),
      opts.channels_file_path
    )
  except Exception as e:
    print(e)
    sys.stdout.flush()
    update_values(
      {'ncells':0, 'channels':[], 'message': "error: " + str(e)},
      opts.channels_file_path
    )

