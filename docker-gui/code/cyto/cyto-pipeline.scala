#!/usr/bin/env anduril
//$OPT -s settings.scala

/**
 * Anduril pipeline for Cyto Single cell analysis software
 * -------------------------------------------------------
 * The pipeline implements the logic behind the app.
 * Parameters from the GUI are read with options(key)
 * where key can be found in config.py, Minimum # of samples: 3
 *
 * Contact julia.casado@helsinki.fi
 */

import anduril.builtin._
import anduril.tools._
import anduril.cytometry._
import org.anduril.runtime._
import setup.settings._
import scala.collection.mutable.Map

object cyto {
    info("Running settings: " + options("settings_name"))
	//--------------------------------------------------------------------------//
	// inputs
	//--------------------------------------------------------------------------//

	// input data
	val inFolder = INPUT(options("in"))
	val inArray = Folder2Array(inFolder, filePattern="(.*)[.]csv").out
	val numFiles = iterArray(inArray).size
	info("Input folder contains "+numFiles+" files")

	// import meta data if user gave it as input
	val metaFile = if(options("clinical_in") != "") {
		INPUT(options("clinical_in")).out
	} else {
		info("no meta data added")
		StringInput("RowName\tColumn1\tColumn2\n").out
	}

	//--------------------------------------------------------------------------//
	// filtering, downsampling and transformation
	//--------------------------------------------------------------------------//

	// Filter out the channels not specified by the user
	val filteredMap = NamedMap[CSV]("filteredMap")
	for ((sampleID, filename) <- iterArray(inArray)){
    	filteredMap(sampleID) = CSVFilter(in=inArray(sampleID), includeColumns=options("all_channels")).out
	}

	// Create An array of the raw data
	val filteredArray = makeArray(filteredMap)
	val dataRawFilteredCSV = CSVListJoin(in=filteredArray, fileCol="sample")

	// Downsampling, if user does not downsample, the data goes through the function without doing anything
	val downsampledArray = CYTOSampler(
                in=filteredArray,
                sampling=options("downsample").toLowerCase.toBoolean,
                method=options("sampling_method"),
                size=options("sample_size").toInt
    ).out

	// Transform the files according to the user input
	val transformedArray = CYTOTransformer(
		in=downsampledArray.force(),
		transformation=options("transformation"),
		normalization=options("normalization")
	)

	//--------------------------------------------------------------------------//
	// Calculate different statistics of the processed and unprocessed data
	//--------------------------------------------------------------------------//

	// drop the sample annotations. Only marker expressions in CSV.
	val data = CSVFilter(in=transformedArray.outCSV, includeColumns="sample", negate=true)
	val dataRaw = CSVFilter(in=CSVListJoin(in=downsampledArray.force(), fileCol="sample"), includeColumns="sample", negate=true)

	//--------------------------------------------------------------------------//
	// calculate sample statistics
	//--------------------------------------------------------------------------//

	// median expressions per sample
	val sampleMedians = CSVSummary(
		in=transformedArray.outCSV,
		clusterCol="sample",
		counts="Count",
		postString="",
		stringMode=false,
		summaryType="median"
	)

	// mean expressions per sample
	val sampleMeans = CSVSummary(
		in=transformedArray.outCSV,
		clusterCol="sample",
		counts="Count",
		postString="",
		stringMode=false,
		summaryType="mean"
	)

	// add sample frequencies to csvs
	val hDataMedianSamples = CSVTransformer(
		csv1=sampleMedians.out,
		columnNames="c('prop', colnames(csv1))",
		transform1="cbind(round(csv1['Count'] / sum(csv1['Count']) * 100, 2), csv1)"
	)

	// add sample frequencies to csvs
	val hDataMeanSamples = CSVTransformer(
		csv1=sampleMeans.out,
		columnNames="c('prop', colnames(csv1))",
		transform1="cbind(round(csv1['Count'] / sum(csv1['Count']) * 100, 2), csv1)"
	)

	// Cell counts per sample
	val cellCounts = CSVFilter(
		in=hDataMeanSamples,
		includeColumns="sample,Count"
	)

	// multi-dimensional-scaling with limma-library
	// TODO: Possibly Do this without REvaluate, check source from limma
	val MDS = REvaluate(script=StringInput("""
		suppressPackageStartupMessages(library(limma))
		samples_median_tbl <- table1[, -which(colnames(table1) %in% c('sample', 'Count', 'prop'))]
		mds_table <- t(samples_median_tbl)
		colnames(mds_table) <- table1$sample
		mds <- plotMDS(mds_table, plot = FALSE)
		table.out <- data.frame(MDS1 = mds$x, MDS2 = mds$y, sample = colnames(mds_table))
		"""),
		table1=hDataMedianSamples
	)

	// non-redundancy scores
	val NRS = NRSScore(
		in=transformedArray.out,
		columnsToRemove="",
		nComp=5
	) // Remove .csv suffixes from "sample" column

	//--------------------------------------------------------------------------//
	// clustering
	//--------------------------------------------------------------------------//

	val dataCluster = CSVFilter(in=transformedArray.outCSV, includeColumns=options("channels_type"))

	val labels = CYTOClusterer(
		in=dataCluster,
		method=options("cluster_method"),
		k_clusters=options("k_clusters").toInt,
		k_neighbors=options("k_nearest").toInt
	).clusters

	// Bind the clusters to the data
	val clusteredData = CSVTransformer(
		csv1=transformedArray.outCSV,
		csv2=labels,
		columnNames="c(colnames(csv1), colnames(csv2))",
		transform1="cbind(csv1, csv2)"
	)
	// TODO: warning clusteredData: Port out is not an array

	//--------------------------------------------------------------------------//
	// cluster statistics
	//--------------------------------------------------------------------------//

	// drop sample column for heatmap csvs
	val clusterExprs = CSVFilter(in=clusteredData, includeColumns="sample", negate=true)

	// median expressions per cluster
	val clusterMedians = CSVSummary(
		in=clusterExprs,
		clusterCol="label",
		counts="Count",
		postString="",
		stringMode=false,
		summaryType="median"
	)

	// mean expressions per cluster
	val clusterMeans = CSVSummary(
		in=clusterExprs,
		clusterCol="label",
		counts="Count",
		postString="",
		stringMode=false,
		summaryType="mean"
	)

	// add cluster frequencies to the CSV
	val hDataMedianClusters = CSVTransformer(
		csv1=clusterMedians.out,
		columnNames="c('prop', colnames(csv1))",
		transform1="cbind(round(csv1['Count'] / sum(csv1['Count']) * 100, 2), csv1)"
	)

	// add cluster frequencies to the CSV
	val hDataMeanClusters = CSVTransformer(
		csv1=clusterMeans.out,
		columnNames="c('prop', colnames(csv1))",
		transform1="cbind(round(csv1['Count'] / sum(csv1['Count']) * 100, 2), csv1)"
	)

	// cluster proportions by sample (column frequency)
	val clusterProportionsCol = CSVDplyr(
		csv1=clusteredData,
		function1="group_by(label, sample)",
		function2="summarise(n=n())",
		function3="mutate(freq=round(n/sum(n)*100, 2))",
		function4="dcast(sample ~ label)",
		function5="replace(., is.na(.), 0)",
		script="library(reshape2)"
	)

	// cluster proportions by sample (row frequency)
	val clusterProportionsRow = CSVDplyr(
		csv1=clusteredData,
		function1="group_by(sample, label)",
		function2="summarise(n=n())",
		function3="mutate(freq=round(n/sum(n)*100, 2))",
		function4="dcast(sample ~ label)",
		function5="replace(., is.na(.), 0)",
		script="library(reshape2)"
	)

	// cluster cell counts per sample
	val clusterSampleCounts = CSVDplyr(
		csv1=clusteredData,
		function1="group_by(label, sample)",
		function2="summarise(n=n())",
		function3="dcast(sample ~ label)",
		function4="replace(., is.na(.), 0)",
		script="library(reshape2)"
	)

	val simpsonIndexes = DiversityIndex(
		in=clusterSampleCounts,
		sampleCol="sample",
		method="simpson"
	)

	//--------------------------------------------------------------------------//
	// dimensionality reduction to 2d
	//--------------------------------------------------------------------------//

	val data2D = CYTOEmbedder(
		in=clusteredData,
		tsne=options("tsne").toBoolean,
		umap=options("umap").toBoolean,
		perplexity=options("perplexity").toInt,
		theta=options("theta").toFloat,
		nNeighbors=options("umap_nearest").toInt,
		minDist=options("umap_min_dist").toFloat,
		sampling=options("downsample_dim").toLowerCase.toBoolean,
		sampleSize=options("sample_size_dim").toInt,
		num_threads=options("max_threads").toInt,
		colsToUse=options("channels_type")
	)

	//--------------------------------------------------------------------------//
	// Join meta data to the files if meta data exists
	//--------------------------------------------------------------------------//

	def joinCSV(file1:CSV, file2:CSV, name:String, keyCols:String) : CSV = {
		val joined = if (iterCSV(file2).size > 0){
			CSVJoin(
				in1=file1,
				in2=file2,
				keyColumnNames=keyCols,
				useKeys=true,
				intersection=false,
				_name=name
			)
		} else {
			CSVTransformer(
				csv1=file1,
				transform1="csv1",
				_name=name
			)
		}
		return joined
	}

	val data2DFull = joinCSV(data2D.out, metaFile, "data2DFull", "sample,sample")
	val dataRawFull = joinCSV(dataRawFilteredCSV, metaFile, "dataRawFull", "sample,sample")
	val dataTransformedFull = joinCSV(transformedArray.outCSV, metaFile, "dataTransformedFull", "sample,sample")
	val NRSFull = joinCSV(NRS, metaFile, "NRSFull", "sample,sample")
	// TODO. output CSV must have raw-untransformed data, just sampled and clustered.
	val resultDataFull = joinCSV(clusteredData, metaFile, "resultDataFull", "sample,sample")
	val simpsonFull = joinCSV(simpsonIndexes, metaFile, "simpsonFull", "sample,sample")
	val mdsFull = joinCSV(MDS.table, metaFile, "mdsFull", "sample,sample")
	val cellCountsFull = joinCSV(cellCounts, metaFile, "cellCountsFull", "sample,sample")
	val clusterCountsFull = joinCSV(clusterSampleCounts, metaFile, "clusterCountsFull", "sample,sample")
	val clusterFreqColFull = joinCSV(clusterProportionsCol, metaFile, "clusterFreqColFull", "sample,sample")
	val clusterFreqRowFull = joinCSV(clusterProportionsRow, metaFile, "clusterFreqRowFull", "sample,sample")

	// -------------------------------------------------------------//
	// pipeline options table for the app
	val optionstbl = REvaluate(script=StringInput("""
		a <- c('2D-embedding Methods','Clustering Method','Transformation','Scaling','Sample Size','2D-embedding sample size')
		dim_1 <- ifelse(param1 == 'True', 'tSNE', '')
		dim_2 <- ifelse(param2 == 'True', 'UMAP', '')

		if (dim_1 != '' && dim_2 != ''){
			dim_text <- c(paste0(dim_1, ", ", dim_2))
		} else if (dim_1 != '' && dim_2 == ''){
			dim_text <- c(paste0(dim_1))
		} else if (dim_1 == '' && dim_2 != ''){
			dim_text <- c(paste0(dim_2))
		}

		sample_1 <- ifelse(param6 == 'False', c('Full Data'), c(param8))
		b <- c(dim_text, param3, param4, param5, paste(sample_1), ifelse(param9 =='False', 'No downsampling', param7))
		table.out <- data.frame(option=a, value=b)
		"""),
		param1=options("tsne"),
		param2=options("umap"),
		param3=options("cluster_method"),
		param4=options("transformation"),
		param5=options("scale"),
		param6=options("downsample"),
		param7=options("sample_size_dim"),
		param8=options("sample_size"),
		param9=options("downsample_dim")
	).table

	//--------------------------------------------------------------------------//
	// feather objects for dash app
	//--------------------------------------------------------------------------//
	val feather = NamedMap[BinaryFile]("feather")
	feather("pipeline_options") = CSV2Feather(in=optionstbl)
	feather("sample_mds") = CSV2Feather(in=mdsFull)
	feather("sample_means") = CSV2Feather(in=hDataMeanSamples)
	feather("sample_medians") = CSV2Feather(in=hDataMedianSamples)
	feather("cell_counts") = CSV2Feather(in=cellCountsFull)
	feather("cluster_medians") = CSV2Feather(in=hDataMedianClusters)
	feather("cluster_means") = CSV2Feather(in=hDataMeanClusters)
	feather("2d_embedding") = CSV2Feather(in=data2DFull)
	feather("cluster_proportions_col") = CSV2Feather(in=clusterFreqColFull)
	feather("cluster_proportions_row") = CSV2Feather(in=clusterFreqRowFull)
	feather("cluster_sample_counts") = CSV2Feather(in=clusterCountsFull)
	feather("simpson_indexes") = CSV2Feather(in=simpsonFull)
	feather("sample_NRS") = CSV2Feather(in=NRSFull)
	feather("raw_data_full") = CSV2Feather(in=dataRawFull)
	feather("trans_data_full") = CSV2Feather(in=dataTransformedFull)

	//--------------------------------------------------------------------------//
	// Python scripts to create dicts ({samplename: pd.Dataframe}) that are written
	// to pickle files. These are done here since calculating these in the dash browser
	// would take forever.
	//--------------------------------------------------------------------------//

	val pickle = NamedMap[BinaryFile]("pickle")

// boxplot dictionary
pickle("boxplots") = PythonEvaluate(
    var1=resultDataFull,
    param0=options("all_channels"),
	script=
"""
import pickle
import pandas as pd
import numpy as np

df = pd.read_csv(var1, delimiter='\t')
cluster_grouped = df.groupby(by='label')
markers=param0
markers = markers.split(",")
meta_cols = df[df.columns[~df.columns.isin(markers)]].columns
meta_cols = meta_cols.drop(['label'])

boxplots = {}
for i, df in cluster_grouped:
    quantiles_full = df.drop(columns=['label']).quantile([0.00, 0.25, 0.25, 0.5, 0.75, 1.00])
    quantiles_full = quantiles_full.sort_values([0.50], axis=1)
    m_order = quantiles_full.columns
    boxplots['cluster {}'.format(i)] = {}
    boxplots['cluster {}'.format(i)]['full'] = {}
    boxplots['cluster {}'.format(i)]['full']['data'] = quantiles_full
    counts = pd.DataFrame(df.pivot_table(index=['sample'], aggfunc='size'))
    counts.reset_index(inplace=True)
    counts.rename(columns={0:'count'}, inplace=True)
    boxplots['cluster {}'.format(i)]['full']['count'] = counts

    if len(meta_cols) > 1:
        for col in meta_cols:
            vals = pd.unique(df[col])
            boxplots['cluster {}'.format(i)][col] = {}
            count_df = pd.DataFrame([])
            for val in vals:
                dff = df[df[col] == val]
                if col != 'sample':
                    gdf = dff.drop(columns=['label']).quantile([0.00, 0.25, 0.25, 0.5, 0.75, 1.00])
                    gdf = gdf[m_order]
                    gdf['group'] = np.repeat(val, gdf.shape[0])
                    boxplots['cluster {}'.format(i)][col][val] = gdf
                    meta = dff.columns[~dff.columns.isin(m_order)]
                    counts = pd.DataFrame(dff[meta].pivot_table(index=['sample', col], aggfunc='size'))
                    counts.reset_index(inplace=True)
                    counts.rename(columns={0:'count'}, inplace=True)
                    count_df = pd.concat([count_df, counts])
                if col != 'sample':
                    boxplots['cluster {}'.format(i)][col]['count'] = count_df

pickle.dump(boxplots, open(out1File,'wb'))
""").out1


// mst data dictionary
pickle("minimum_spanning_trees") = PythonEvaluate(
	var1=resultDataFull,
    param0=options("all_channels"),
	script=
"""
import pickle
import pandas as pd
import numpy as np

full = pd.read_csv(var1, delimiter='\t')
markers=param0
markers = markers.split(",")
meta_data = full[full.columns[~full.columns.isin(markers)]]
meta_vals = meta_data.columns.drop(['label'])

# data for the whole dataset MST
label_counts = full['label'].value_counts()
labels_length = len(label_counts)
label_props = full['label'].value_counts(normalize=True)*100
label_counts.sort_index(inplace=True)
label_props.sort_index(inplace=True)
medians = pd.pivot_table(full, index=['label'], aggfunc=np.median)
medians = medians.assign(cellcounts=pd.Series(label_counts).values, prop=pd.Series(label_props).values)

frames_dict = {}
frames_dict['full data'] = medians

for value in meta_vals:
    grouped = full.groupby([value])
    group_dict = {}
    for name, frame in grouped:

    	# counts and proportions of the labels in each sample
    	label_counts = frame['label'].value_counts()
    	label_props = frame['label'].value_counts(normalize=True)*100

    	# Sort indexes to match cluster labels in pivot table
    	label_counts.sort_index(inplace=True)
    	label_props.sort_index(inplace=True)
    	medians = pd.pivot_table(frame, index=['label'], aggfunc=np.median)
    	medians = medians.assign(cellcounts=pd.Series(label_counts).values, prop=pd.Series(label_props).values)

    	# Fill missing labels with zeroes
    	new_index = list(range(1,labels_length+1))
    	medians = medians.reindex(new_index, fill_value=0)
    	group_dict[name] = medians
    frames_dict[value] = group_dict

pickle.dump(frames_dict, open(out1File,'wb'))
""").out1


// Correlation frames for each sample
pickle("correlations") = PythonEvaluate(
	var1=clusteredData,
	script=
"""
import pickle
import pandas as pd
from scipy.spatial.distance import pdist, squareform
from scipy.cluster.hierarchy import linkage, leaves_list

full = pd.read_csv(var1, delimiter='\t')
corr_dict = {}
for name, df in full.groupby(['sample']):
	df = df.drop(['sample', 'label'], axis=1)
	data = df.corr()
	if not data.isnull().values.any():
		dist = pdist(data, metric='euclidean')
		link = linkage(dist, metric='correlation', method='average')
		data = data.iloc[leaves_list(link), leaves_list(link)]
		corr_dict[name] = data
	else:
		print("ERROR in Correlation matrix: Sample {:} had single value columns.".format(name)) # quick fix, do something instead

pickle.dump(corr_dict, open(out1File,'wb'))
"""
).out1

// Markers dict (different marker groups)
pickle("markers") = PythonEvaluate(
    param0=options("all_channels"),
    param1=options("channels_type"),
    param2=options("channels_state"),
    script=
"""
import pickle

channels_dict = {}
channels_dict['all_channels'] = param0.split(",")
channels_dict['channels_type'] = param1.split(",")
channels_dict['channels_state'] = param2.split(",")

pickle.dump(channels_dict, open(out1File,'wb'))
"""
).out1

	//--------------------------------------------------------------------------//
	// call report website bashscript
	//--------------------------------------------------------------------------//
	val reportWebsite = BashEvaluate(
		array1=makeArray(feather),
		array2=makeArray(pickle),
		script=
		"""
		keys1=($(getarraykeys array1));
		files1=($(getarrayfiles array1));
		for ((i=0; i<${#keys1[@]}; i++))
		do
			cp "${files1[$i]}" @folder1@/${keys1[$i]}.feather
		done;

		keys2=($(getarraykeys array2));
		files2=($(getarrayfiles array2));
		for ((i=0; i<${#keys2[@]}; i++))
		do
			cp "${files2[$i]}" @folder2@/${keys2[$i]}.pickle
		done;
		"""
	)
}
