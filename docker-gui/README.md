
# Userguide TO Be

Start docker with:
```
docker run \
  --rm \
  -p 9999:9999 \
  -p 8050:8050 \
  -v $(pwd)/projects:/projects \
  anduril/cyto
```
Change your working space to match: $(pwd)/projects -> [where your projects folder is]

Or use docker-compose:
`docker-compose up`

working space is set up similarly.

## GUI usage

Shortcuts:

- ctrl-s  save settings
- ctrl-x  eXecute

# Dev Guide

dev cycle:

while true; do ./build.sh; sleep 1; ./run.sh; sleep 1;done



# License

GNU GENERAL PUBLIC LICENSE v.3


