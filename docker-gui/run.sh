#!/bin/bash
set -x
docker run --rm \
   -p 9999:9999\
   -p 8050:8050\
   -v $(pwd)/projects:/projects\
    anduril/cyto
